var pwd = document.getElementById("pwd")
  , confirmPwd = document.getElementById("confirmPwd");

function validatePassword(){
  if(pwd.value != confirmPwd.value) {
    confirmPwd.setCustomValidity("Passwords Don't Match");
  } else {
    confirmPwd.setCustomValidity('');
  }
}

pwd.onchange = validatePassword;
confirmPwd.onkeyup = validatePassword;