<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Job Edit - PROPAPPS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="resources/css/font-awesome.css" rel="stylesheet">
<!-- Custom Style -->
<link href="resources/css/main.css" rel="stylesheet">
<script>
         var liveObjOfState = {
        "Europe": { "France": ["Lille", "Paris"],
 	    "Macedonia": ["Skopje", "Kratovo","Bitola","Koumanovo","Prilep","Tetovo"],
        "England": ["Birmingham", "London"],
        	         },
         "Asia": {
         "China": ["Shanghai", "Pekin"],
         "Japan": ["Osaka", "Tokyo"],
       	"Indonesia": ["Jakarta", "Subraya"]
         }, "America": {
         "Canada": ["Acadia", "Bighorn"],
         "United-States": ["Washington", "Chicago"]
         },
         "Africa": { "Morocco": ["Fez", "Oujda"],
             "Cameroon": ["Yaound�", "Douala"],
             "Gabon": ["Libreville", "Port-Gentil"],
             },
         }
         window.onload = function () {
         var selectCountry = document.getElementById("selectCountry"),
         selectState = document.getElementById("selectState"),
         countrystatecitySelect = document.getElementById("countrystatecitySelect");
         for (var country in liveObjOfState) {
         selectCountry.options[selectCountry.options.length] = new Option(country, country);
         }
         selectCountry.onchange = function () {
         selectState.length = 1; // delete all options bar first
         countrystatecitySelect.length = 1; // delete all options bar first
         if (this.selectedIndex < 1) return; // done
         for (var state in liveObjOfState[this.value]) {
         selectState.options[selectState.options.length] = new Option(state, state);
         }
         }
         selectCountry.onchange(); // reset in case page is reloaded
         selectState.onchange = function () {
         countrystatecitySelect.length = 1; // delete all options bar first
         if (this.selectedIndex < 1) return; // done
         var region = liveObjOfState[selectCountry.value][this.value];
         for (var i = 0; i < region.length; i++) {
         countrystatecitySelect.options[countrystatecitySelect.options.length] = new Option(region[i], region[i]);
         }
         }
         }
      </script>
  <style>
    .full-link {
      width: 97%;
    height: 94%;
    z-index: 1;
    position: absolute;
    }

    .card {
      width: 100%;
    }

    .sidebar {
      height: auto !important;
    }

    .pull-right {
      float: right;
    }
  </style>
</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="margin:0 !important;">
    <div class="header">
      <div class="container">
        <div class="row">
          <div class="col-md-2 col-sm-3 col-xs-12"> 
            <a href="#" class="logo"><img src="resources/images/Logo.png" alt="Logo" /></a>
           	<div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="col-md-10 col-sm-12 col-xs-12"> 
            <!-- Nav start -->
            <div class="navbar navbar-default" role="navigation">
              <div class="navbar-collapse collapse" id="nav-main">
                <ul class="nav navbar-nav">
                  <li><a href="companyHome?id=${company.idCompany}">Home </a></li>
                  <li class="postjob"><a class="btn btn-primary" href="signOutCompany">Logout </a></li>
                  <!-- <li class="postjob"><a href="./webapp/pages/forms/login.html">Login</a></li> -->
                  <!-- <li class="jobseeker"><a href="candidate-listing.html">Job Seeker</a></li> -->
                  <div class="social"> 
                      <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a> 
                       <a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a> 
                       <a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a> 
                    </div>
                </ul>
                <!-- Nav collapes end --> 
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- Nav end --> 
          </div>
        </div>
        <!-- row end --> 
      </div>
      <!-- Header container end --> 
    </div>
    <!-- Main content -->
    <div class="container">
      <div class="row mb-2 margin-top30">
        <div class="col-sm-12">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Edit Job</li>
          </ol>
        </div>
      </div>
        <div class="row">
          <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
            <div class="userccount">
              <div class="formpanel"> 
                
                <!-- Job Information -->
                <h5>Job Information</h5>
                <form action="addApprenticeship" method="post">
                 <input
                 
                  type="text" value="${ company.idCompany }" name="id" hidden="true"
               />
                <div class="row">
                  <div class="col-md-12">
                  
                    <div class="formrow">
                      <p> Sector
                       <select class="form-control" placeholder="Title of qualification" name="occupation">
                       <c:forEach items="${ industry }" var="occupation"  >
	   			<option  >${ occupation }  </option>
	   		
	  			
	  			</c:forEach>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="formrow">
                      <textarea class="form-control" name="shortdesc" placeholder="Profile short description"></textarea>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="formrow">
                      <textarea class="form-control" name="entryreq" placeholder="Entry requirements"></textarea>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="formrow">
                      <textarea class="form-control" name="knowledge" placeholder="Knowledge"></textarea>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="formrow">
                      <textarea class="form-control" name="skills" placeholder="Skills"></textarea>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="formrow">
                      <textarea class="form-control" name="behaviours" placeholder="Behaviours"></textarea>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="formrow">
                      <select class="form-control" name="duration">
                        <option>Duration</option>
                        <option>1 - 3 months</option>
                        <option>3 - 6 months</option>
                        <option>6 - 12 months</option>
                        <option>12 - 18 months</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="formrow">
                    <p> Title
                       <select class="form-control" placeholder="Title of qualification" name="title">
                       <c:forEach items="${ subindustry }" var="qualification"  >
	   			<option  >${ qualification }  </option>
	   		
	  			
	  			</c:forEach>
                      </select>
                    </div>
                  </div>
                   <div class="col-md-12">
                    <div class="formrow">
                    <p> Job
                       <select class="form-control" placeholder="Jib" name="apprenticeship">
                       <c:forEach items="${ apprenticeship }" var="job"  >
	   			<option  >${ job }  </option>
	   		
	  			
	  			</c:forEach>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="formrow">
                      <textarea class="form-control" name="progression" placeholder="Progression (whats next)"></textarea>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="formrow">
                      <select class="form-control" name="school">
                        <option>Choose Schools</option>
                        <option>Kiril Pejcinoviq</option>
                        <option>Mosha Piade</option>
                        <option>Ekonomija</option>
                        <option>Taki Daskalo</option>
                        <option>Mile Janevski - Dzingar</option>
                        <option>Naum Naumovski - Borce</option>
                        <option>Kosta Susinov</option>
                      </select> 
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="formrow">
                      <select class="form-control" name="edprog">
                        <option>Choose Ed. programme</option>
                        <option>1 - 3 years</option>
                        <option>1 - 4 years</option>
                        <option>2 - 3 years</option>
                        <option>4 years</option>
                      </select> 
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="formrow">
                      <select class="form-control" name="area">
                        <option>Industry Area</option>
                        <option>IT</option>
                        <option>Accounts</option>
                        <option>Marketing</option>
                        <option>Manager</option>
                        <option>Banking</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="formrow">
                      <select class="form-control" name="msalary">
                        <option>Monthly Salary</option>
                        <option>$500 - $999</option>
                        <option>$999 - $1499</option>
                        <option>$1500 - $1999</option>
                        <option>$2000 - $3000</option>
                        <option>$3000+</option>
                      </select>
                    </div>
                  </div>
                  
                    <div class="col-md-6">
                    <div class="formrow">
                  
                    
                       <select name="state" id="selectCountry" class="form-control"  >
	   			<option value="" selected="selected" >Continent </option>
	   		
	  			
                      </select>
                      <br>
                      <select name="countrya" id="selectState" class="form-control"  >
	   			<option  value="" selected="selected"> Country </option>
	   		
	  			
                      </select>
                      <br>
                      <select name="location" id="countrystatecitySelect"class="form-control"  >
                      
	   			<option  value="" selected="selected">City</option>
	   		
	  			
                      </select>
                   
                                  </div>
                   
                  </div>
                  
                  <div class="col-md-6">
                    <div class="formrow">
                      <select class="form-control" name="empstatus">
                        <option>Employment Status</option>
                        <option>Permanent</option>
                        <option>Contract</option>
                        <option>Freelance</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="formrow">
                      <select class="form-control" name="emptype">
                        <option>Employment Type</option>
                        <option>Manager</option>
                        <option>Web designer</option>
                        <option>Junior Designer</option>
                        <option>Senior Developer</option>
                        <option>Mid Level Designer</option>
                        <option>Frontend Developer</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="formrow">
                      <select class="form-control" name="positions">
                        <option>Positions</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="formrow">
                      <select class="form-control" name="experience">
                        <option>Experience</option>
                        <option>Fresh</option>
                        <option>1 Year</option>
                        <option>2 Years</option>
                        <option>3 Years</option>
                        <option>4 Years</option>
                        <option>5 Years</option>
                        <option>6 Years</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="formrow">
                      <select class="form-control" name="gender">
                        <option>Gender</option>
                        <option>Male</option>
                        <option>Female</option>
                        <option>Any</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="formrow">
                      <select class="form-control" name="nationality">
                        <option>Nationality</option>
                        <option>United States</option>
                        <option>Australia</option>
                        <option>United Kingdom</option>
                      </select>
                    </div>
                  </div>
                  
                  <div class="col-md-12">
                    <div class="formrow">
                      <textarea class="form-control" name="jobdetail" placeholder="Job Detail"></textarea>
                    </div>
                  </div>
                </div>
                <br>
                <input type="submit" class="btn" value="Post Job">
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    
    <!-- <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved. -->
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- Bootstrap's JavaScript --> 
<script src="resources/js/jquery-2.1.4.min.js"></script> 
<script src="resources/js/bootstrap.min.js"></script> 

<!-- Custom js --> 
<!-- <script src="resources/js/script.js"></script> -->
</body>
</html>
