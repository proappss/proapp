<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login - PROPAPPS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="resources/css/font-awesome.css" rel="stylesheet">
<!-- Custom Style -->
<link href="resources/css/main.css" rel="stylesheet">

<style>
  .wrapper {
    position: relative;
    height: 100vh;
  }

  .content-wrapper.login-page{
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
</style>

</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<!-- Header start -->
<div class="header">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-3 col-xs-12"> <a href="indexLogout" class="logo"><img src="resources/images/Logo.png" alt="" /></a>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="col-md-10 col-sm-12 col-xs-12">
        <div class="navbar navbar-default" role="navigation">
          <div class="navbar-collapse collapse" id="nav-main">
            <ul class="nav navbar-nav">
                <li><a href="indexLogout">Home </a></li>
                <li><a href="aboutLogout">About us</a></li>
                <li><a href="partnersLogout">Partners</a></li>
                <li class="active"><a href="contactUsLogout">Contact</a></li>
                <li class="postjob"><a href="loginLogout">Login</a></li>
                <div class="social">
                  <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                   <a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                   <a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
                </div>
              </ul>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Header end --> <!-- Page Title start -->

<form action="signUp" method="post">

<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->

  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper login-page">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="userccount">
            <!--  <div class="socialLogin">
              <h5>Login Or Register with Social</h5>
              <a href="#." class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a>
              <a href="#." class="gp"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
              </div> -->
              <div id="candidate" class="formpanel">
              	<div class="divRegisterGrid">
	                <div class="formrow">
	                  <input type="text" name="name" class="form-control" placeholder="Name">
	                </div>
	                <div class="formrow">
	                  <input type="text" name="regNo" class="form-control" placeholder="Reg No">
	                </div>
	                <div class="formrow">
	                  <input type="email" name="email" class="form-control" placeholder="Email">
	                </div>

	                <div class="formrow">
	                  <input type="tel" name="tel" class="form-control" placeholder="Phone">
	                </div>

	                <div class="formrow">
	                  <input type="text" name="website" class="form-control" placeholder="Website">
	                </div>

	                <div class="formrow">
	                  <input type="text" name="facebook" class="form-control" placeholder="Facebook">
	                </div>

	                <div class="formrow">
	                  <input type="text" name="linkedin" class="form-control" placeholder="Linkedin">
	                </div>

	                <div class="formrow">
	                  <input type="text" name="type" class="form-control" placeholder="Type">
	                </div>

	                <div class="formrow">
	                  <input type="text" name="nameStreet" class="form-control" placeholder="Name Street">
	                </div>

	                <div class="formrow">
	                  <input type="text" name="zipCode" class="form-control" placeholder="ZIP Code">
	                </div>

	                <div class="formrow">
	                  <input type="text" name="city" class="form-control" placeholder="City">
	                </div>

	                <div class="formrow">
	                  <input type="text" name="country" class="form-control" placeholder="Country">
	                </div>
                </div>
                <div class="row">
				<div class="col-md-12">
				         <div class="formrow">
 							 <textarea name="companyDesc" class="form-control" placeholder="Company About">${ company.companyDesc }</textarea>
                          </div>
				</div>
				</div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="formrow">
                          	  <select class="form-control" name="totalEmployees">
                                <option>Total Employees</option>
                                <option>0-50</option>
                                <option>50-250</option>
                                <option>250-500</option>
                                <option>500+</option>
                              </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="formrow">
						  <select class="form-control" name="yearEstablished">
                                <option>Established In</option>
                                <option>1970</option>
                                <option>1971</option>
                                <option>1972</option>
                                <option>1973</option>
								<option>1974</option>
                                <option>1975</option>
								<option>1976</option>
                                <option>1977</option>
                                <option>1978</option>
                                <option>1979</option>
                                <option>1980</option>
                                <option>1981</option>
                                <option>1982</option>
                                <option>1983</option>
								<option>1984</option>
                                <option>1985</option>
								<option>1986</option>
                                <option>1987</option>
                                <option>1988</option>
                                <option>1989</option>
                                <option>1990</option>
                                <option>1991</option>
                                <option>1992</option>
                                <option>1993</option>
								<option>1994</option>
                                <option>1995</option>
								<option>1996</option>
                                <option>1997</option>
                                <option>1998</option>
                                <option>1999</option>
                                <option>2000</option>
                                <option>2001</option>
                                <option>2002</option>
                                <option>2003</option>
								<option>2004</option>
                                <option>2005</option>
								<option>2006</option>
                                <option>2007</option>
                                <option>2008</option>
                                <option>2009</option>
                                <option>2010</option>
                                <option>2011</option>
                                <option>2012</option>
                                <option>2013</option>
								<option>2014</option>
                                <option>2015</option>
								<option>2016</option>
                                <option>2017</option>
                                <option>2018</option>
                                <option>2019</option>
								<option>2020</option>
                                <option>2021</option>
                                <option>2022</option>
                              </select>
                        </div>
                      </div>
                </div>

                <div class="formrow">
                  <input type="text" name="login" class="form-control" placeholder="Login">
                </div>

                <div class="formrow">
                  <input type="password" name="pwd" class="form-control" placeholder="Password (5 characters minimum and 20 maximum)" required pattern='{5,20}' id="pwd">
                </div>
                <div class="formrow">
                  <input type="password" name="confirmPwd" class="form-control" placeholder="Confirm Password" id="confirmPwd">
                </div>
                <input type="submit" class="btn" value="Register">
              </div>

              <div class="newuser">
                <i class="fa fa-user" aria-hidden="true"></i> Already a Member? <a href="loginLogout">Login Here</a>
              </div>

          </div>
        </div>

      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">

    <!-- <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved. -->
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>

</form>

<!-- Bootstrap's JavaScript -->
<script src="resources/js/jquery-2.1.4.min.js"></script>
<script src="resources/js/bootstrap.min.js"></script>

<!-- Custom js -->
<script src="resources/js/script.js"></script>

<!-- Check Password js -->
<script src="resources/js/checkPassword.js"></script>
</body>
</html>
