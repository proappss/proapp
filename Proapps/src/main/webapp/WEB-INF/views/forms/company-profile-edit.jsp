<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Company Edit Profile - PROPAPPS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="resources/css/font-awesome.css" rel="stylesheet">
<!-- Custom Style -->
<link href="resources/css/main.css" rel="stylesheet">
  <style>
    .full-link {
      width: 97%;
    height: 94%;
    z-index: 1;
    position: absolute;
    }

    .card {
      width: 100%;
    }

    .sidebar {
      height: auto !important;
    }

    .pull-right {
      float: right;
    }

  </style>
</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->

  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="margin:0 !important;">
    <div class="header">
      <div class="container">
        <div class="row">
          <div class="col-md-2 col-sm-3 col-xs-12">
            <a href="#" class="logo"><img src="resources/images/Logo.png" alt="Logo" /></a>
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="col-md-10 col-sm-12 col-xs-12">
            <!-- Nav start -->
            <div class="navbar navbar-default" role="navigation">
              <div class="navbar-collapse collapse" id="nav-main">
                <ul class="nav navbar-nav">
                  <li><a href="companyHome?id=${ company.idCompany }">Home </a></li>
                  <li class="postjob"><a class="btn btn-primary" href="signOutCompany">Logout </a></li>
                  <!-- <li class="postjob"><a href="./webapp/pages/forms/login.html">Login</a></li> -->
                  <!-- <li class="jobseeker"><a href="candidate-listing.html">Job Seeker</a></li> -->
                  <div class="social">
                      <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                       <a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                       <a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
                    </div>
                </ul>
                <!-- Nav collapes end -->
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- Nav end -->
          </div>
        </div>
        <!-- row end -->
      </div>
      <!-- Header container end -->
    </div>
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <div class="container">
      <div class="row mb-2 margin-top30">
        <div class="col-sm-12">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Edit Company</li>
          </ol>
        </div>
      </div>
	          <div class="row">
            <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
            <div class="userccount">
              <div class="formpanel">

                <!-- Company Basic Information -->
                <h5>Company Information</h5>
				 <div class="row">
                    <div class="col-md-6">
                        <div class="formrow">
                          <input type="text" name="companyname" class="form-control" placeholder="Company Name" value="${ company.name }">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="formrow">
                          <input type="text" name="industry" class="form-control" placeholder="Industry" value="${ company.regNo }">
                        </div>
                      </div>
                </div>
				<div class="row">
                    <div class="col-md-6">
                        <div class="formrow">
                          <input type="text" name="companyaddress" class="form-control" placeholder="Address" value="${ company.addressCompany.nameStreet }">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="formrow">
                          <input type="text" name="companycity" class="form-control" placeholder="City" value="${ company.addressCompany.city }">
                        </div>
                      </div>
                </div>
				<div class="row">
                    <div class="col-md-6">
                        <div class="formrow">
                          <input type="email" name="title" class="form-control" placeholder="E-mail HR staff" value="${ company.email }">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="formrow">
                          <input type="tel" name="number" class="form-control" placeholder="Phone HR staff" value="${ company.tel }">
                        </div>
                      </div>
                </div>
				<div class="row">
				<div class="col-md-6">
                          <div class="formrow">
                            <input type="text" name="fb" class="form-control" placeholder="Facebook" value="${ company.facebook }">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="formrow">
                            <input type="text" name="twitter" class="form-control" placeholder="Twitter" value="${ company.linkedin }">
                          </div>
                        </div>
				</div>
				<div class="row">
                    <div class="col-md-6">
                        <div class="formrow">
                          	  <select class="form-control" name="NoEmploy" value="${ company.totalEmployees }">
                                <option>${ company.totalEmployees }</option>
                                <option>0-50</option>
                                <option>50-250</option>
                                <option>250-500</option>
                                <option>500+</option>
                              </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="formrow">
						  <select class="form-control" name="established" value="${ company.yearEstablished }">
                                <option>${ company.yearEstablished }</option>
                                <option>1970</option>
                                <option>1971</option>
                                <option>1972</option>
                                <option>1973</option>
								<option>1974</option>
                                <option>1975</option>
								<option>1976</option>
                                <option>1977</option>
                                <option>1978</option>
                                <option>1979</option>
                                <option>1980</option>
                                <option>1981</option>
                                <option>1982</option>
                                <option>1983</option>
								<option>1984</option>
                                <option>1985</option>
								<option>1986</option>
                                <option>1987</option>
                                <option>1988</option>
                                <option>1989</option>
                                <option>1990</option>
                                <option>1991</option>
                                <option>1992</option>
                                <option>1993</option>
								<option>1994</option>
                                <option>1995</option>
								<option>1996</option>
                                <option>1997</option>
                                <option>1998</option>
                                <option>1999</option>
                                <option>2000</option>
                                <option>2001</option>
                                <option>2002</option>
                                <option>2003</option>
								<option>2004</option>
                                <option>2005</option>
								<option>2006</option>
                                <option>2007</option>
                                <option>2008</option>
                                <option>2009</option>
                                <option>2010</option>
                                <option>2011</option>
                                <option>2012</option>
                                <option>2013</option>
								<option>2014</option>
                                <option>2015</option>
								<option>2016</option>
                                <option>2017</option>
                                <option>2018</option>
                                <option>2019</option>
								<option>2020</option>
                                <option>2021</option>
                                <option>2022</option>
                              </select>
                        </div>
                      </div>
                </div>
				<div class="row">
				<div class="col-md-12">
				         <div class="formrow">
 							 <textarea name="CompanyDescr" class="form-control" placeholder="Company About">${ company.companyDesc }</textarea>
                          </div>
				</div>
				</div>
			</div>
        </div>
      </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
            <div class="userccount">
              <div class="formpanel">


                <br>
                <div class="loctext"><a class="btn" href="deleteCompany?id=${ company.idCompany }">DELETE COMPANY</a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">

    <!-- <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved. -->
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- Bootstrap's JavaScript -->
<script src="resources/js/jquery-2.1.4.min.js"></script>
<script src="resources/js/bootstrap.min.js"></script>

<!-- Custom js -->
<!-- <script src="resources/js/script.js"></script> -->
</body>
</html>
