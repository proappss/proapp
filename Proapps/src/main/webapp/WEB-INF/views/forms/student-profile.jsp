<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Apprenticeship Listings - PROPAPPS</title>
      <script>
         var liveObjOfState = {
        "Europe": { "France": ["Lille", "Paris"],
 	    "Macedonia": ["Skopje", "Kratovo","Bitola","Koumanovo","Prilep","Tetovo"],
        "England": ["Birmingham", "London"],
        	         },
         "Asia": {
         "China": ["Shanghai", "Pekin"],
         "Japan": ["Osaka", "Tokyo"],
       	"Indonesia": ["Jakarta", "Subraya"]
         }, "America": {
         "Canada": ["Acadia", "Bighorn"],
         "United-States": ["Washington", "Chicago"]
         },
         "Africa": { "Morocco": ["Fez", "Oujda"],
             "Cameroon": ["Yaound�", "Douala"],
             "Gabon": ["Libreville", "Port-Gentil"],
             },
         }
         window.onload = function () {
         var selectCountry = document.getElementById("selectCountry"),
         selectState = document.getElementById("selectState"),
         countrystatecitySelect = document.getElementById("countrystatecitySelect");
         for (var country in liveObjOfState) {
         selectCountry.options[selectCountry.options.length] = new Option(country, country);
         }
         selectCountry.onchange = function () {
         selectState.length = 1; // delete all options bar first
         countrystatecitySelect.length = 1; // delete all options bar first
         if (this.selectedIndex < 1) return; // done
         for (var state in liveObjOfState[this.value]) {
         selectState.options[selectState.options.length] = new Option(state, state);
         }
         }
         selectCountry.onchange(); // reset in case page is reloaded
         selectState.onchange = function () {
         countrystatecitySelect.length = 1; // delete all options bar first
         if (this.selectedIndex < 1) return; // done
         var region = liveObjOfState[selectCountry.value][this.value];
         for (var i = 0; i < region.length; i++) {
         countrystatecitySelect.options[countrystatecitySelect.options.length] = new Option(region[i], region[i]);
         }
         }
         }
      </script>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<!-- Font Awesome -->
<link href="resources/css/font-awesome.css" rel="stylesheet">

<!-- Custom Style -->
<link href="resources/css/main.css" rel="stylesheet">
  <style>
    .full-link {
      width: 97%;
    height: 94%;
    z-index: 1;
    position: absolute;
    }

    .card {
      width: 100%;
    }

    .sidebar {
      height: auto !important;
    }

    .pull-right {
      float: right;
    }

    .header {
      background-color: transparent;
    }
  </style>
</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->

  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="margin:0 !important;">
    <div class="header">
      <div class="container">
        <div class="row">
          <div class="col-md-2 col-sm-3 col-xs-12">
              <a href="#" class="logo"><img src="resources/images/Logo.png" alt="Logo" /></a>
             
              <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="col-md-10 col-sm-12 col-xs-12">
            <!-- Nav start -->
            <div class="navbar navbar-default" role="navigation">
              <div class="navbar-collapse collapse" id="nav-main">
                <ul class="nav navbar-nav">
                  <li class="active"><a href="studentHome?id=${ student.idStudent }">Home </a></li>
                  <li><a href="studentProfile?id=${ student.idStudent }">Profile </a></li>
                  <li class="postjob"><a class="btn btn-primary" href="signOutStudent">Logout </a></li>
                  <!-- <li class="jobseeker"><a href="candidate-listing.html">Job Seeker</a></li> -->
                </ul>
                <!-- Nav collapes end -->
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- Nav end -->
          </div>
        </div>
        <!-- row end -->
      </div>
      <!-- Header container end -->
    </div>
    <!-- Main content -->
    <div class="container">
      <div class="row margin-top30">
        <div class="col-md-5">
            <div class="postimg"><img src="resources/images/about-us-img1.jpg" alt="your alt text"></div>
             
          </div>
      <div class="col-md-7">
        <h2>About students and apprenticeships</h2>
        <p style="text-align: justify;">Gain a qualification which is recognized, strengthen your skills, gain experience and enhance your employability. Engaging in an apprenticeship will provide you with opportunity to have a genuine job! Your job will be based on skills development programme with an accompanying assessment</p>
        <p style="margin-top:10px;text-align: justify">The programme will make sure that the apprentices will shape what you learn and help you progress towards set career goals.</p>
        <p style="margin-top:10px;text-align: justify">A specialist in the field of your interest will guide you reach these career goals. This guidance is set on one-to-one basis.</p>
        <p style="margin-top:10px;text-align: justify">If you see yourself as an apprentice, then you:</p>
        <ul class="orderlist">
          <li style="width: 100%;">Have a clear focus on which career you might want to pursue</li>
          <li style="width: 100%;">Committed to work and study whereas work is your main focus</li>
          <li style="width: 100%;">Are ready to take up the competing load of work and school in the same time</li>
          <li style="width: 100%;">Feel at ease with assignments and assessments at work and school</li>
        </ul>
       <p></p>
        <a href="comingSoon" style="margin-top:20px;" name="" id="" class="btn btn-primary" btn-lg="" btn-block"="">Become an apprentice</a>
      </div>

    </div>
      <div class="row mb-2 margin-top30">
        <div class="col-sm-12">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="href="studentHome?id=${ student.idStudent }>Home</a></li>
            <li class="breadcrumb-item active">Apprenticeship List</li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8 col-sm-8" style="margin: 0 auto;">
          <!-- Search List -->
          <ul class="searchList">
         <c:forEach items="${ listeApprenticeship }" var="apprenticeship">
          <!-- Job start -->
          <li>
          <div class="row">
            <a class="clickable" href="apprenticeshipDescription?idCompany=${company.idCompany}&idApprenticeship=${apprenticeship.idApp}"></a>
            <div class="col-md-8 col-sm-8">
              <div class="jobimg"><img src="resources/images/employers/emplogo8.jpg" alt="Job Name"></div>
              <div class="jobinfo">
                <h3><a href="#.">${ apprenticeship.title }</a></h3>
                <div class="companyName"><a href="#.">${ company.name }</a></div>
                <c:if test="${ apprenticeship.empstatus =='Freelance'}">
                  <div class="location"><label class="freelance">Freelance</label>   - <span>${ apprenticeship.location }</span></div>
                </c:if>
                <c:if test="${ apprenticeship.empstatus =='Permanent'}">
                  <div class="location"><label class="fulltime">Full Time</label>   - <span>${ apprenticeship.location }</span></div>
                </c:if>
                <c:if test="${ apprenticeship.empstatus =='Contract'}">
                  <div class="location"><label class="partTime">Part Time</label>   - <span>${ apprenticeship.location }</span></div>
                </c:if>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="col-md-4 col-sm-4">
              <div class="listbtn"><a href="studentApprenticeshipView?idStudent=${student.idStudent}&idApprenticeship=${apprenticeship.idApp}">Apply Now</a></div>
            </div>

          </div>
          <p> ${ apprenticeship.jobdetail }</p>
        </li>
          <!-- Job end -->

</c:forEach>


          
          </ul>

          <!-- Pagination Start -->
          <div class="pagiWrap">
            <div class="row">
              <div class="col-md-4 col-sm-4">
                <div class="showreslt">Showing 1-10</div>
              </div>
              <div class="col-md-8 col-sm-8 text-right">
                <ul class="pagination">
                  <li class="active"><a href="#.">1</a></li>
                  <li><a href="#.">2</a></li>
                  <li><a href="#.">3</a></li>
                  <li><a href="#.">4</a></li>
                  <li><a href="#.">5</a></li>
                  <li><a href="#.">6</a></li>
                  <li><a href="#.">7</a></li>
                  <li><a href="#.">8</a></li>
                </ul>
              </div>
            </div>
          </div>
          <!-- Pagination end -->
        </div>
        <div class="widget">
              <h4 class="widget-title">Filter your apprenticeship</h4>
        	<form action="studentFilter?id=${ student.idStudent }" method="post">
        	   <input type="hidden" name="idStudent" class="form-control" placeholder="Title" value="${student.idStudent}">
        <div class="col-md-4 col-sm-4">
          <!-- Side Bar start -->
          <div class="sidebar">
            <!-- Jobs By Title -->
            <div class="widget">
            <div class="row">
            
                  <div class="col-md-12">
                  
                    <div class="formrow">
                  
                      <p> Sector
                       <select class="form-control" placeholder="Title of qualification" name="occupation">
                       <c:forEach items="${ industry }" var="occupation"  >
	   			<option  >${ occupation }  </option>
	   		
	  			
	  			</c:forEach>
                      </select>
                   
                                  </div>
                   
                                  </div>
                   
                                  </div>
               <br>    
                 <div class="row">
                  <div class="col-md-12">
                  
                    <div class="formrow">
                  
                      <p> Localisation
                       <select name="state" id="selectCountry" class="form-control"  >
	   			<option value="" selected="selected" >Continent </option>
	   		
	  			
                      </select>
                      <br>
                      <select name="countrya" id="selectState" class="form-control"  >
	   			<option  value="" selected="selected"> Country </option>
	   		
	  			
                      </select>
                      <br>
                      <select name="region" id="countrystatecitySelect"class="form-control"  >
                      
	   			<option  value="" selected="selected">City</option>
	   		
	  			
                      </select>
                   
                                  </div>
                   
                                  </div>
                   
                                  </div>
               <br>  
              </div>
              <!-- title end -->
              <a href="#.">View More</a> </div>

          
            <!-- Salary end -->

            <!-- button -->
            <div class="searchnt">
               <button class="btn" type="submit"><i class="fa fa-search" aria-hidden="true"></i> Search job</button>
            </div>
            </form>
            <!-- button end-->
          </div>
          <!-- Side Bar end -->
        </div>
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">

    <!-- <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved. -->
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- Bootstrap's JavaScript -->
<script src="assets/js/jquery-2.1.4.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<!-- Custom js -->
<!-- <script src="assets/js/script.js"></script> -->
</body>
</html>
    