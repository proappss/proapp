<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Company Edit Profile - PROPAPPS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="resources/css/font-awesome.css" rel="stylesheet">
<!-- Custom Style -->
<link href="resources/css/main.css" rel="stylesheet">
  <style>
    .full-link {
      width: 97%;
    height: 94%;
    z-index: 1;
    position: absolute;
    }

    .card {
      width: 100%;
    }

    .sidebar {
      height: auto !important;
    }

    .pull-right {
      float: right;
    }

  </style>
</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->

  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="margin:0 !important;">
    <div class="header">
      <div class="container">
        <div class="row">
          <div class="col-md-2 col-sm-3 col-xs-12">
            <a href="#" class="logo"><img src="resources/images/Logo.png" alt="Logo" /></a>
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="col-md-10 col-sm-12 col-xs-12">
            <!-- Nav start -->
            <div class="navbar navbar-default" role="navigation">
              <div class="navbar-collapse collapse" id="nav-main">
                <ul class="nav navbar-nav">
                  <li><a href="studentHome?id=${ student.idStudent }">Home </a></li>
                   <li><a href="studentProfile?id=${ student.idStudent }">Profile </a></li>

                  <!-- <li class="postjob"><a href="./webapp/pages/forms/login.html">Login</a></li> -->
                  <!-- <li class="jobseeker"><a href="candidate-listing.html">Job Seeker</a></li> -->
                  <div class="social">
                      <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                       <a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                       <a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
                    </div>
                </ul>
                <!-- Nav collapes end -->
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- Nav end -->
          </div>
        </div>
        <!-- row end -->
      </div>
      <!-- Header container end -->
    </div>
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <div class="container">
      <div class="row mb-2 margin-top30">
        <div class="col-sm-12">
          <ol class="breadcrumb float-sm-right">
             <li><a href="studentHome?id=${ student.idStudent }">Home </a></li>
            <li class="breadcrumb-item active">Edit Company</li>
          </ol>
        </div>
      </div>
	          <div class="row">
            <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
            <div class="userccount">
              <div class="formpanel">

                <!-- Company Basic Information -->
                <h5>Student Information</h5>
				 <div class="row">
                    <div class="col-md-6">
                        <div class="formrow">
                          <input type="text" name="companyname" class="form-control" placeholder="Student Name" value="${ student.name }">
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="formrow">
                          <input type="text" name="companyname" class="form-control" placeholder="Student Surname" value="${ student.surname }">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="formrow">
                          <input type="text" name="industry" class="form-control" placeholder="Birth date" value="${ student.dateBirth }">
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="formrow">
                          <input type="text" name="companyname" class="form-control" placeholder="Company Name" value="${ student.placeBirth}">
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="formrow">
                          <input type="tel" name="companyname" class="form-control" placeholder="Company Name" value="${ student.phone }">
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="formrow">
                          <input type="email" name="companyname" class="form-control" placeholder="Company Name" value="${ student.email }">
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="formrow">
                          <input type="text" name="companyname" class="form-control" placeholder="Company Name" value="${ student.gender }">
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="formrow">
                          <input type="text" name="companyname" class="form-control" placeholder="Company Name" value="${ student.registrationDate }">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="formrow">
                          <input type="text" name="companyname" class="form-control" placeholder="Company Name" value="${ student.lastCertification }">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="formrow">
                          <input type="text" name="companyname" class="form-control" placeholder="Company Name" value="${ student.currentSchool }">
                        </div>
                      </div>
                </div>

				<div class="row">
                    <div class="col-md-6">
                        <div class="formrow">
                          <input type="text" name="companyaddress" class="form-control" placeholder="Address" value="${ company.addressStudent.nameStreet }">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="formrow">
                          <input type="text" name="companycity" class="form-control" placeholder="City" value="${ company.addressStudent.city }">
                        </div>
                      </div>
                </div>


        </div>
      </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
            <div class="userccount">
              <div class="formpanel">




                </div>
                <br>
                <div class="loctext"><a class="btn" href="deleteStudent?id=${ student.idStudent}">DELETE STUDENT</a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">

    <!-- <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved. -->
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- Bootstrap's JavaScript -->
<script src="resources/js/jquery-2.1.4.min.js"></script>
<script src="resources/js/bootstrap.min.js"></script>

<!-- Custom js -->
<!-- <script src="resources/js/script.js"></script> -->
</body>
</html>
