<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Job Listings - PROPAPPS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="resources/css/font-awesome.css" rel="stylesheet">
<!-- Custom Style -->
<link href="resources/css/main.css" rel="stylesheet">
  <style>
    .full-link {
      width: 97%;
    height: 94%;
    z-index: 1;
    position: absolute;
    }

    .card {
      width: 100%;
    }

    .sidebar {
      height: auto !important;
    }

    .pull-right {
      float: right;
    }
  </style>
</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="margin:0 !important;">
    <div class="header">
      <div class="container">
        <div class="row">
          <div class="col-md-2 col-sm-3 col-xs-12"> 
            <a href="#" class="logo"><img src="resources/images/Logo.png" alt="Logo" /></a>
                        <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="col-md-10 col-sm-12 col-xs-12"> 
            <!-- Nav start -->
            <div class="navbar navbar-default" role="navigation">
              <div class="navbar-collapse collapse" id="nav-main">
                <ul class="nav navbar-nav">
                  <li class="active"><a href="index.html">Home </a></li>
                  <li><a href="candidate-edit-cv.html">Profile </a></li>
                  <!-- <li class="jobseeker"><a href="candidate-listing.html">Job Seeker</a></li> -->
                  <div class="social"> 
                      <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a> 
                       <a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a> 
                       <a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a> 
                    </div>
                </ul>
                <!-- Nav collapes end --> 
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- Nav end --> 
          </div>
        </div>
        <!-- row end --> 
      </div>
      <!-- Header container end --> 
    </div>
    <!-- Main content -->
    <div class="container"> 
      <div class="row mb-2 margin-top30">
        <div class="col-sm-12">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Job List</li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8 col-sm-8" style="margin: 0 auto;"> 
          <!-- Search List -->
          <ul class="searchList">
            <!-- job start -->
            <li>
              <div class="row">
                <div class="col-md-8 col-sm-8">
                  <div class="jobimg"><img src="companies/comp1.png" alt="Job Name"></div>
                  <div class="jobinfo">
                    <h3><a href="./job-description.html">Technical Database Engineer</a></h3>
                    <div class="companyName"><a href="#.">Datebase Management Company</a></div>
                    <div class="location"><label class="fulltime">Full Time</label>   - <span>New York</span></div>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="col-md-4 col-sm-4">
                  <div class="listbtn"><a href="#.">Apply Now</a></div>
                </div>
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis.</p>
            </li>
            <!-- job end --> 
            
            <!-- job start -->
            <li>
              <div class="row">
                <div class="col-md-8 col-sm-8">
                  <div class="jobimg"><img src="companies/comp2.png" alt="Job Name"></div>
                  <div class="jobinfo">
                    <h3><a href="./job-description.html">Technical Database Engineer</a></h3>
                    <div class="companyName"><a href="#.">Datebase Management Company</a></div>
                    <div class="location"><label class="freelance">Free Lancer</label>   - <span>New York</span></div>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="col-md-4 col-sm-4">
                  <div class="listbtn"><a href="#.">Apply Now</a></div>
                </div>
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis.</p>
            </li>
            <!-- job end --> 
            
            <!-- job start -->
            <li>
              <div class="row">
                <div class="col-md-8 col-sm-8">
                  <div class="jobimg"><img src="companies/comp3.png" alt="Job Name"></div>
                  <div class="jobinfo">
                    <h3><a href="./job-description.html">Technical Database Engineer</a></h3>
                    <div class="companyName"><a href="#.">Datebase Management Company</a></div>
                    <div class="location"><label class="partTime">Part Time</label>   - <span>New York</span></div>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="col-md-4 col-sm-4">
                  <div class="listbtn"><a href="#.">Apply Now</a></div>
                </div>
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis.</p>
            </li>
            <!-- job end --> 
            
            <!-- job start -->
            <li>
              <div class="row">
                <div class="col-md-8 col-sm-8">
                  <div class="jobimg"><img src="companies/comp4.png" alt="Job Name"></div>
                  <div class="jobinfo">
                    <h3><a href="./job-description.html">Technical Database Engineer</a></h3>
                    <div class="companyName"><a href="#.">Datebase Management Company</a></div>
                    <div class="location"><label class="fulltime">Full Time</label>   - <span>New York</span></div>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="col-md-4 col-sm-4">
                  <div class="listbtn"><a href="#.">Apply Now</a></div>
                </div>
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis.</p>
            </li>
            <!-- job end --> 
            
            <!-- job start -->
            <li>
              <div class="row">
                <div class="col-md-8 col-sm-8">
                  <div class="jobimg"><img src="companies/comp2.png" alt="Job Name"></div>
                  <div class="jobinfo">
                    <h3><a href="./job-description.html">Technical Database Engineer</a></h3>
                    <div class="companyName"><a href="#.">Datebase Management Company</a></div>
                    <div class="location"><label class="freelance">Free Lancer</label>   - <span>New York</span></div>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="col-md-4 col-sm-4">
                  <div class="listbtn"><a href="#.">Apply Now</a></div>
                </div>
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis.</p>
            </li>
            <!-- job end --> 
            
            <!-- job start -->
            <li>
              <div class="row">
                <div class="col-md-8 col-sm-8">
                  <div class="jobimg"><img src="companies/comp1.png" alt="Job Name"></div>
                  <div class="jobinfo">
                    <h3><a href="./job-description.html">Technical Database Engineer</a></h3>
                    <div class="companyName"><a href="#.">Datebase Management Company</a></div>
                    <div class="location"><label class="partTime">Part Time</label>   - <span>New York</span></div>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="col-md-4 col-sm-4">
                  <div class="listbtn"><a href="#.">Apply Now</a></div>
                </div>
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis.</p>
            </li>
            <!-- job end --> 
            
            <!-- job start -->
            <li>
              <div class="row">
                <div class="col-md-8 col-sm-8">
                  <div class="jobimg"><img src="companies/comp3.png" alt="Job Name"></div>
                  <div class="jobinfo">
                    <h3><a href="./job-description.html">Technical Database Engineer</a></h3>
                    <div class="companyName"><a href="#.">Datebase Management Company</a></div>
                    <div class="location"><label class="fulltime">Full Time</label>   - <span>New York</span></div>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="col-md-4 col-sm-4">
                  <div class="listbtn"><a href="#.">Apply Now</a></div>
                </div>
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis.</p>
            </li>
            <!-- job end --> 
            
            <!-- job start -->
            <li>
              <div class="row">
                <div class="col-md-8 col-sm-8">
                  <div class="jobimg"><img src="companies/comp4.png" alt="Job Name"></div>
                  <div class="jobinfo">
                    <h3><a href="./job-description.html">Technical Database Engineer</a></h3>
                    <div class="companyName"><a href="#.">Datebase Management Company</a></div>
                    <div class="location"><label class="freelance">Free Lancer</label>   - <span>New York</span></div>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="col-md-4 col-sm-4">
                  <div class="listbtn"><a href="#.">Apply Now</a></div>
                </div>
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis.</p>
            </li>
            <!-- job end --> 
            
            <!-- job start -->
            <li>
              <div class="row">
                <div class="col-md-8 col-sm-8">
                  <div class="jobimg"><img src="companies/comp2.png" alt="Job Name"></div>
                  <div class="jobinfo">
                    <h3><a href="./job-description.html">Technical Database Engineer</a></h3>
                    <div class="companyName"><a href="#.">Datebase Management Company</a></div>
                    <div class="location"><label class="fulltime">Full Time</label>   - <span>New York</span></div>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="col-md-4 col-sm-4">
                  <div class="listbtn"><a href="#.">Apply Now</a></div>
                </div>
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis.</p>
            </li>
            <!-- job end --> 
            
            <!-- job start -->
            <li>
              <div class="row">
                <div class="col-md-8 col-sm-8">
                  <div class="jobimg"><img src="companies/comp3.png" alt="Job Name"></div>
                  <div class="jobinfo">
                    <h3><a href="./job-description.html">Technical Database Engineer</a></h3>
                    <div class="companyName"><a href="#.">Datebase Management Company</a></div>
                    <div class="location"><label class="partTime">Part Time</label>   - <span>New York</span></div>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="col-md-4 col-sm-4">
                  <div class="listbtn"><a href="#.">Apply Now</a></div>
                </div>
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis.</p>
            </li>
            <!-- job end -->
            
          </ul>
          
          <!-- Pagination Start -->
          <div class="pagiWrap">
            <div class="row">
              <div class="col-md-4 col-sm-4">
                <div class="showreslt">Showing 1-10</div>
              </div>
              <div class="col-md-8 col-sm-8 text-right">
                <ul class="pagination">
                  <li class="active"><a href="#.">1</a></li>
                  <li><a href="#.">2</a></li>
                  <li><a href="#.">3</a></li>
                  <li><a href="#.">4</a></li>
                  <li><a href="#.">5</a></li>
                  <li><a href="#.">6</a></li>
                  <li><a href="#.">7</a></li>
                  <li><a href="#.">8</a></li>
                </ul>
              </div>
            </div>
          </div>
          <!-- Pagination end --> 
        </div>
        <div class="col-md-4 col-sm-4"> 
          <!-- Side Bar start -->
          <div class="sidebar"> 
            <!-- Jobs By Title -->
            <div class="widget">
              <h4 class="widget-title">Jobs By Title</h4>
              <ul class="optionlist">
                <li>
                  <input type="checkbox" name="checkname" id="webdesigner">
                  <label for="webdesigner"></label>
                  Web Designer <span>12</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="3dgraphic">
                  <label for="3dgraphic"></label>
                  3D Graphic Designer <span>33</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="graphic">
                  <label for="graphic"></label>
                  Graphic Designer <span>33</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="electronicTech">
                  <label for="electronicTech"></label>
                  Electronics Technician <span>33</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="webgraphic">
                  <label for="webgraphic"></label>
                  Web / Graphic Designer <span>33</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="brandmanager">
                  <label for="brandmanager"></label>
                  Brand Manager <span>33</span> </li>
              </ul>
              <!-- title end --> 
              <a href="#.">View More</a> </div>
            
            <!-- Jobs By City -->
            <div class="widget">
              <h4 class="widget-title">Jobs By City</h4>
              <ul class="optionlist">
                <li>
                  <input type="checkbox" name="checkname" id="newyork">
                  <label for="newyork"></label>
                  New York <span>12</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="losangles">
                  <label for="losangles"></label>
                  Los Angeles <span>33</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="chicago">
                  <label for="chicago"></label>
                  Chicago <span>33</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="houston">
                  <label for="houston"></label>
                  Houston <span>12</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="sandiego">
                  <label for="sandiego"></label>
                  San Diego <span>555</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="sanjose">
                  <label for="sanjose"></label>
                  San Jose <span>44</span> </li>
              </ul>
              <a href="#.">View More</a> </div>
            <!-- Jobs By City end--> 
            
            <!-- Jobs By Experience -->
            <div class="widget">
              <h4 class="widget-title">Jobs By Experience</h4>
              <ul class="optionlist">
                <li>
                  <input type="checkbox" name="checkname" id="permanent">
                  <label for="permanent"></label>
                  Full Time/Permanent <span>12</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="contract">
                  <label for="contract"></label>
                  Contract <span>33</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="parttime">
                  <label for="parttime"></label>
                  Part Time <span>33</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="internship">
                  <label for="internship"></label>
                  Internship <span>33</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="freelance">
                  <label for="freelance"></label>
                  Freelance <span>33</span> </li>
              </ul>
              <a href="#.">View More</a> </div>
            <!-- Jobs By Experience end --> 
            
            <!-- Jobs By Industry -->
            <div class="widget">
              <h4 class="widget-title">Jobs By Industry</h4>
              <ul class="optionlist">
                <li>
                  <input type="checkbox" name="checkname" id="infotech">
                  <label for="infotech"></label>
                  Information Technology <span>22</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="advertise">
                  <label for="advertise"></label>
                  Advertising/PR <span>45</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="services">
                  <label for="services"></label>
                  Services <span>44</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="health">
                  <label for="health"></label>
                  Health &amp; Fitness <span>88</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="mediacomm">
                  <label for="mediacomm"></label>
                  Media/Communications <span>22</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="fashion">
                  <label for="fashion"></label>
                  Fashion <span>11</span> </li>
              </ul>
              <a href="#.">View More</a> </div>
            <!-- Jobs By Industry end --> 
            
            <!-- Top Companies -->
            <div class="widget">
              <h4 class="widget-title">Top Companies</h4>
              <ul class="optionlist">
                <li>
                  <input type="checkbox" name="checkname" id="Envato">
                  <label for="Envato"></label>
                  Envato <span>12</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="Themefores">
                  <label for="Themefores"></label>
                  Themefores <span>33</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="GraphicRiver">
                  <label for="GraphicRiver"></label>
                  Graphic River <span>33</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="Codecanyon">
                  <label for="Codecanyon"></label>
                  Codecanyon <span>33</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="AudioJungle">
                  <label for="AudioJungle"></label>
                  Audio Jungle <span>33</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="Videohive">
                  <label for="Videohive"></label>
                  Videohive <span>33</span> </li>
              </ul>
              <a href="#.">View More</a> </div>
            <!-- Top Companies end --> 
            
            <!-- Salary -->
            <div class="widget">
              <h4 class="widget-title">Salary Range</h4>
              <ul class="optionlist">
                <li>
                  <input type="checkbox" name="checkname" id="price1">
                  <label for="price1"></label>
                  0 to $100 <span>12</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="price2">
                  <label for="price2"></label>
                  $100 to $199 <span>41</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="price3">
                  <label for="price3"></label>
                  $199 to $499 <span>33</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="price4">
                  <label for="price4"></label>
                  $499 to $999 <span>66</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="price5">
                  <label for="price5"></label>
                  $999 to $4999 <span>159</span> </li>
                <li>
                  <input type="checkbox" name="checkname" id="price6">
                  <label for="price6"></label>
                  Above $4999 <span>865</span> </li>
              </ul>
              <a href="#.">View More</a> </div>
            <!-- Salary end --> 
            
            <!-- button -->
            <div class="searchnt">
              <button class="btn"><i class="fa fa-search" aria-hidden="true"></i> Search Jobs</button>
            </div>
            <!-- button end--> 
          </div>
          <!-- Side Bar end --> 
        </div>
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    
    <!-- <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved. -->
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- Bootstrap's JavaScript --> 
<script src="resources/js/jquery-2.1.4.min.js"></script> 
<script src="resources/js/bootstrap.min.js"></script> 

<!-- Custom js --> 
<!-- <script src="resources/js/script.js"></script> -->
</body>
</html>
