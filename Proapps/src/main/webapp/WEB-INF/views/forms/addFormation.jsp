<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Formation Edit - PROPAPPS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="resources/css/font-awesome.css" rel="stylesheet">
<!-- Custom Style -->
<link href="resources/css/main.css" rel="stylesheet">

  <style>
    .full-link {
      width: 97%;
    height: 94%;
    z-index: 1;
    position: absolute;
    }

    .card {
      width: 100%;
    }

    .sidebar {
      height: auto !important;
    }

    .pull-right {
      float: right;
    }
  </style>
</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="margin:0 !important;">
    <div class="header">
      <div class="container">
        <div class="row">
          <div class="col-md-2 col-sm-3 col-xs-12"> 
            <a href="#" class="logo"><img src="resources/images/Logo.png" alt="Logo" /></a>
                       <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="col-md-10 col-sm-12 col-xs-12"> 
            <!-- Nav start -->
            <div class="navbar navbar-default" role="navigation">
              <div class="navbar-collapse collapse" id="nav-main">
                <ul class="nav navbar-nav">
                  <li><a href="schoolHome?id=${school.idSchool}">Home </a></li>
                  <li><a href="schoolProfile?id=${ school.idSchool}">Profile </a></li>
                  <!-- <li class="postjob"><a href="./webapp/pages/forms/login.html">Login</a></li> -->
                  <!-- <li class="jobseeker"><a href="candidate-listing.html">Job Seeker</a></li> -->
                  <div class="social"> 
                      <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a> 
                       <a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a> 
                       <a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a> 
                    </div>
                </ul>
                <!-- Nav collapes end --> 
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- Nav end --> 
          </div>
        </div>
        <!-- row end --> 
      </div>
      <!-- Header container end --> 
    </div>
    <!-- Main content -->
    <div class="container">
      <div class="row mb-2 margin-top30">
        <div class="col-sm-12">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Edit Formation</li>
          </ol>
        </div>
      </div>
        <form action="addFormationForm" method="post">

<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper login-page">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <div class="container"> 
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="userccount">
            <!--  <div class="socialLogin">
              <h5>Login Or Register with Social</h5>
              <a href="#." class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> 
              <a href="#." class="gp"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
              </div> -->
              <div id="candidate" class="formpanel">
              		
              		<div class="formrow">
	                  <input type="hidden" name="idSchool" class="form-control" placeholder="Title" value="${school.idSchool}">
	                </div>
	                <div class="formrow">
	                  <input type="text" name="title" class="form-control" placeholder="Title">
	                </div>
	                <div class="formrow">
	                  <input type="text" name="knwoledge" class="form-control" placeholder="knwoledge">
	                </div>
	                
	                <div class="formrow">
	                  <input type="text" name="duration" class="form-control" placeholder="duration">
	                </div>
	                
	                <div class="formrow">
	                  <input type="text" name="numberHours" class="form-control" placeholder="numberHours">
	                </div>
	                
	                <div class="formrow">
	                  <input type="text" name="location" class="form-control" placeholder="location">
	                </div>
	                
	                <div class="formrow">
	                  <input type="number" name="position" class="form-control" placeholder="position">
	                </div>
	                
	                <div class="formrow">
	                  <input type="text" name="type" class="form-control" placeholder="Type">
	                </div>
                
                <div class="row">
				<div class="col-md-12">
				         <div class="formrow">
 							 <textarea name="formationDesc" class="form-control" placeholder="Description Formation"></textarea>
                          </div>
				</div>
				</div>
				<div class="row">
				<div class="col-md-12">
				         <div class="formrow">
 							 <textarea name="formationDetail" class="form-control" placeholder="Detail Formation"></textarea>
                          </div>
				</div>
				</div>
                
                <input type="submit" class="btn" value="Add Formation">
              </div>
           
          </div>
        </div>
       
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    
    <!-- <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved. -->
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>

</form>
      </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    
    <!-- <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved. -->
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- Bootstrap's JavaScript --> 
<script src="resources/js/jquery-2.1.4.min.js"></script> 
<script src="resources/js/bootstrap.min.js"></script> 

<!-- Custom js --> 
<!-- <script src="resources/js/script.js"></script> -->
</body>
</html>
