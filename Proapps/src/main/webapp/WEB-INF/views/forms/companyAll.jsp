<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Company Profile - PROPAPPS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">



<!-- Bootstrap -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<!-- Font Awesome -->
<link href="resources/css/font-awesome.css" rel="stylesheet">

<!-- Custom Style -->
<link href="resources/css/main.css" rel="stylesheet">

  <style>
    .full-link {
      width: 97%;
    height: 94%;
    z-index: 1;
    position: absolute;
    }

    ul>li {
      list-style-type: none;
    }

    .details {
      padding: 0px;
    }

    .responsibilities {
      margin-top: 50px;
      border-top: 2px solid black;
      padding-top: 100px;
    }

    .job-header {
    background: #fff;
    border: 1px solid #e4e4e4;
    margin-bottom: 30px;
}

.job-header .jobinfo {
    padding: 25px;
    border-bottom: 1px solid #e4e4e4;
}

.content-wrapper {
  margin: 0 !important;
}

.cadsocial a {
    color: #666;
    font-size: 24px;
    display: inline-block;
    margin-right: 7px;
}
.userPic {
    border: none !important;
}

.clickable {
  position: absolute;
  width: 100%;
  height: 100%;
  z-index: 1;
  top: 0;
  left: 0;
}
.apprenticeship-desc li a {
  border: none;
}

.header {
  background-color: transparent;
}
  </style>
</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->

  <!-- Main Sidebar Container -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="margin: 0 !important;">
    <!-- Header start -->
    <div class="">
      <div class="container">
        <div class="row">
          <div class="col-md-2 col-sm-3 col-xs-12">
            <a href="#" class="logo"><img src="resources/images/Logo.png" alt="Logo" /></a>
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="col-md-10 col-sm-12 col-xs-12">
            <!-- Nav start -->
            <div class="navbar navbar-default" role="navigation">
              <div class="navbar-collapse collapse" id="nav-main">
                <ul class="nav navbar-nav apprenticeship-desc">
                  <li><a href="companyHome?id=${ company.idCompany }">Home </a></li>
                  <li class="postjob"><a class="btn btn-primary" href="signOutCompany">Logout </a></li>
                </ul>
                <!-- Nav collapes end -->
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- Nav end -->
          </div>
        </div>
        <!-- row end -->
      </div>
      <!-- Header container end -->
    </div>
<!-- Header end -->

    <!-- Main content -->
    <div class="container">
      <div class="row mb-2 margin-top30">
        <div class="col-sm-12">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="companyHome?id=${ company.idCompany }">Home</a></li>
            <li class="breadcrumb-item active">App Company Apprenticeships</li>
          </ol>
        </div>
      </div>
        <!-- Job Header start -->
        <div class="job-header">
          <div class="jobinfo">
            <div class="row">
              <div class="col-md-8 col-sm-8">
                <!-- Candidate Info -->
                <div class="candidateinfo">
                  <div class="userPic" style="background: none;"><img src="/assets/images/employers/emplogo16.jpg" alt=""></div>
                  <div class="title">Datebase Management Company</div>
                  <div class="desi">Development Company</div>
                  <div class="loctext"><i class="fa fa-history" aria-hidden="true"></i> Member Since, Aug 14, 2016</div>
                  <div class="loctext"><i class="fa fa-map-marker" aria-hidden="true"></i> 123 Boscobel Street, NW8 8PS</div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-md-4 col-sm-4">
                <!-- Candidate Contact -->
                <div class="candidateinfo">
                  <div class="loctext"><i class="fa fa-phone" aria-hidden="true"></i> (+1) 123.456.7890</div>
                  <div class="loctext"><i class="fa fa-envelope" aria-hidden="true"></i> info@company.com</div>
                  <div class="loctext"><i class="fa fa-globe" aria-hidden="true"></i> www.company.com</div>
                  <div class="cadsocial">
                      <a href="http://www.twitter.com" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                      <a href="http://www.facebook.com" target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                      <a href="https://www.linkedin.com" target="_blank"><i class="fa fa-instagram-square"></i></a> </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Job Detail start -->
        <div class="row">
          <h3>Open Apprenticeships</h3>
          <div class="col-lg-9">
            <div class="relatedJobs">
              <ul class="searchList">
                <c:forEach items="${ listeApprenticeship }" var="apprenticeship">
          <!-- Job start -->
          <li>
          <div class="row">
            <a class="clickable" href="apprenticeshipDescription?idCompany=${company.idCompany}&idApprenticeship=${apprenticeship.idApp}"></a>
            <div class="col-md-8 col-sm-8">
              <div class="jobimg"><img src="resources/images/employers/emplogo8.jpg" alt="Job Name"></div>
              <div class="jobinfo">
                <h3><a href="#.">${ apprenticeship.title }</a></h3>
                <div class="companyName"><a href="#.">${ company.name }</a></div>
                <c:if test="${ apprenticeship.empstatus =='Freelance'}">
                  <div class="location"><label class="freelance">Freelance</label>   - <span>${ apprenticeship.location }</span></div>
                </c:if>
                <c:if test="${ apprenticeship.empstatus =='Permanent'}">
                  <div class="location"><label class="fulltime">Full Time</label>   - <span>${ apprenticeship.location }</span></div>
                </c:if>
                <c:if test="${ apprenticeship.empstatus =='Contract'}">
                  <div class="location"><label class="partTime">Part Time</label>   - <span>${ apprenticeship.location }</span></div>
                </c:if>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="col-md-4 col-sm-4">
              <div class="listbtn"><a href="apprenticeshipDescription?idCompany=${company.idCompany}&idApprenticeship=${apprenticeship.idApp}">Apply Now</a></div>
            </div>

          </div>
          <p> ${ apprenticeship.jobdetail }</p>
        </li>
          <!-- Job end -->

</c:forEach>
  </ul>
            </div>
            
            <div class="pagiWrap" style="margin-bottom:30px;">
              <div class="row">
                <div class="col-md-4 col-sm-6">
                  <div class="showreslt">Showing 1-10</div>
                </div>
                <div class="col-md-8 col-sm-6 text-right">
                  <ul class="pagination">
                    <li class="active"><a href="#.">1</a></li>
                    <li><a href="#.">2</a></li>
                    <li><a href="#.">3</a></li>
                    <li><a href="#.">4</a></li>
                    <li><a href="#.">5</a></li>
                    <li><a href="#.">6</a></li>
                    <li><a href="#.">7</a></li>
                    <li><a href="#.">8</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-12">
			<form action="companyFilter?id=${ company.idCompany }" method="post">
  				
				<div class="formrow">
	                  <input type="hidden" name="idCompany" class="form-control" placeholder="Title" value="${company.idCompany}">
	                </div>
            <!-- Side Bar start -->
            <div class="sidebar">
              <!-- Jobs By Title -->
              <div class="widget">
                <h4 class="widget-title">Apprenticeships By Sector </h4>
                <ul class="optionlist">
                  <li>
                    <input type="checkbox" name="webdesigner" id="webdesigner">
                    <label for="webdesigner"></label>
                    Web Designer <span>12</span> </li>
                  <li>
                    <input type="checkbox" name="3dgraphic" id="3dgraphic">
                    <label for="3dgraphic"></label>
                    3D Graphic Designer <span>33</span> </li>
                  <li>
                    <input type="checkbox" name="graphic" id="graphic">
                    <label for="graphic"></label>
                    Graphic Designer <span>33</span> </li>
                  <li>
                    <input type="checkbox" name="electronicTech" id="electronicTech">
                    <label for="electronicTech"></label>
                    Electronics Technician <span>33</span> </li>
                  <li>
                    <input type="checkbox" name="webgraphic" id="webgraphic">
                    <label for="webgraphic"></label>
                    Web / Graphic Designer <span>33</span> </li>
                  <li>
                    <input type="checkbox" name="brandmanager" id="brandmanager">
                    <label for="brandmanager"></label>
                    Brand Manager <span>33</span> </li>
                </ul>
               </div>

              <!-- Jobs By City -->
              <div class="widget">
                <h4 class="widget-title">Apprenticeships By City</h4>
                <ul class="optionlist">
                  <li>
                    <input type="checkbox" name="newyork" id="newyork">
                    <label for="newyork"></label>
                    New York <span>12</span> </li>
                  <li>
                    <input type="checkbox" name="losangles" id="losangles">
                    <label for="losangles"></label>
                    Los Angeles <span>33</span> </li>
                  <li>
                    <input type="checkbox" name="chicago" id="chicago">
                    <label for="chicago"></label>
                    Chicago <span>33</span> </li>
                  <li>
                    <input type="checkbox" name="houston" id="houston">
                    <label for="houston"></label>
                    Houston <span>12</span> </li>
                  <li>
                    <input type="checkbox" name="sandiego" id="sandiego">
                    <label for="sandiego"></label>
                    San Diego <span>555</span> </li>
                  <li>
                    <input type="checkbox" name="sanjose" id="sanjose">
                    <label for="sanjose"></label>
                    San Jose <span>44</span> </li>
                </ul>
                  
               </div>
              <div class="searchnt">
                <button class="btn" type="submit"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
                </form>
              </div>
            </div>
            <!-- Side Bar end -->
          </div>
        </div>
      </div>
  </div>


 <!--Footer-->
<div class="row">
  <div class="footerWrap">
    <div class="container">
      <div class="row">
        <!--About Us-->
        <div class="col-md-3 col-sm-12">
          <div class="ft-logo"><img src="/assets/images/Logo.png" alt="Logo"></div>
          <p>Expand your knowledge and develop your skills for brighter tomorrow. </p>
          <!-- Social Icons -->
          <div class="social">
            <p>Follow us on Social Media: </p>
            <a href="https://facebook.com/VETapprenticeships" target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a>
          </div>
          <!-- Social Icons end -->
        </div>
        <!--About us End-->

        <!--Quick Links-->
        <div class="col-md-2 col-sm-6">
          <h5>Quick Links</h5>
          <!--Quick Links menu Start-->
          <ul class="quicklinks">
            <li><a href="#.">Companies</a></li>
            <li><a href="#.">Schools</a></li>
            <li><a href="#.">Students</a></li>
            <li><a href="#.">Company Listings</a></li>
            <li><a href="#.">Success Stories</a></li>
          </ul>
        </div>
        <!--Quick Links menu end-->

        <!--Jobs By Industry-->
        <div class="col-md-3 col-sm-6">
          <h5>Jobs By Industry</h5>
          <!--Industry menu Start-->
          <ul class="quicklinks">
            <li><a href="#.">Information Technology Jobs</a></li>
            <li><a href="#.">Recruitment/Employment Firms Jobs</a></li>
            <li><a href="#.">Education/Training Jobs</a></li>
            <li><a href="#.">Manufacturing Jobs</a></li>
            <li><a href="#.">Call Center Jobs</a></li>
          </ul>
          <!--Industry menu End-->
          <div class="clear"></div>
        </div>

        <!--Latest Articles-->
        <div class="col-md-4 col-sm-12">
          <div class="ft-logo" style="width:80%;"><img src="/assets/images/erasmusplus-logo-all-en-300dpi.jpg" /></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--Footer end-->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- Bootstrap's JavaScript -->
<script src="../assets/js/jquery-2.1.4.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>

<!-- Custom js -->
<!-- <script src="assets/js/script.js"></script> -->
</body>
</html>
    