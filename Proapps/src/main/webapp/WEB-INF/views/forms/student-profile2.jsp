<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Student Edit Profile - PROPAPPS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<!-- Font Awesome -->
<link href="resources/css/font-awesome.css" rel="stylesheet">

<!-- Custom Style -->
<link href="resources/css/main.css" rel="stylesheet">
  <style>
    .full-link {
      width: 97%;
    height: 94%;
    z-index: 1;
    position: absolute;
    }

    .card {
      width: 100%;
    }

    .sidebar {
      height: auto !important;
    }

    .pull-right {
      float: right;
    }

    .header {
      background-color: transparent;
    }
  </style>
</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->

  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="margin:0 !important;">
    <div class="header">
      <div class="container">
        <div class="row">
          <div class="col-md-2 col-sm-3 col-xs-12">
             <div class="ft-logo"><img src="resources/images/Logo.png" alt="Logo"></div>
              </a>
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="col-md-10 col-sm-12 col-xs-12">
            <!-- Nav start -->
            <div class="navbar navbar-default" role="navigation">
              <div class="navbar-collapse collapse" id="nav-main">
                <ul class="nav navbar-nav">
                  <li><a href="studentHome?id=${ student.idStudent }">Home </a></li>
                 <li><a href="studentProfile?id=${ student.idStudent }">Profile </a></li>
                  <li class="postjob"><a class="btn btn-primary" href="signOutSchool">Logout </a></li>
                  <!-- <li class="postjob"><a href="./webapp/pages/forms/login.html">Login</a></li> -->
                  <!-- <li class="jobseeker"><a href="candidate-listing.html">Job Seeker</a></li> -->
                </ul>
                <!-- Nav collapes end -->
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- Nav end -->
          </div>
        </div>
        <!-- row end -->
      </div>
      <!-- Header container end -->
    </div>
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <div class="container">
      <div class="row mb-2 margin-top30">
        <div class="col-sm-12">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="href="studentHome?id=${ student.idStudent }>Home</a></li>
            <li class="breadcrumb-item active"><a href="studentEditProfile?id=${ student.idStudent }">Edit student </a></li>
          </ol>
        </div>
      </div>
        <div class="row">
          <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
            <div class="userccount">
              <div class="formpanel">

                <!-- Personal Information -->
                <h5>Personal Information</h5>
                <div class="row">
                  <div class="col-md-6">
                    <div class="formrow">
                      <input type="text" name="name" class="form-control" placeholder="User Name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="formrow">
                      <input type="email" name="email" class="form-control" placeholder="Email">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="formrow">
                      <input type="tel" name="phone" class="form-control" placeholder="Phone">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="formrow">
                      <input type="text" name="facebook" class="form-control" placeholder="Facebook">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="formrow">
                      <input type="text" name="address" class="form-control" placeholder="Address">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="formrow">
                      <input type="text" name="twitter" class="form-control" placeholder="Twitter">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="formrow">

                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="formrow">

                    </div>
                  </div>
                  <div class="col-md-6">
                        <div class="formrow">
                      <input type="text" name="LinkedIn" class="form-control" placeholder="LinkedIn">
                    </div>
                  </div>
                  <div class="col-md-3">

                    </div>
                  </div>
					         <div class="row">
                  <div class="col-md-12">
                    <div class="formrow">
                      <textarea name="aboutme" class="form-control" placeholder="About Me"></textarea>
                    </div>
                  </div>
				  </div>

                <hr>

                <!-- Skills -->
                <h5>Skills</h5>
                <div class="row">
                  <div class="col-md-6">
                    <div class="formrow">
                      <input type="text" name="skill" class="form-control" placeholder="Skill Name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="formrow">
                      <input type="text" name="skilllevel" class="form-control" placeholder="Skill Level in %">
                    </div>
                  </div>
                </div>
                <a href="#.">Add Other</a>
                <hr>

				<!-- Behaviours -->
                <h5>Behaviours</h5>
                <div class="row">
                  <div class="col-md-6">
                    <div class="formrow">
                      <input type="text" name="behaviour" class="form-control" placeholder="Behaviour Name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="formrow">
                      <input type="text" name="behaviourlevel" class="form-control" placeholder="Behaviour Level in %">
                    </div>
                  </div>
                </div>
                <a href="#.">Add Other</a>
                <hr>

                <!-- Education -->
                <h5>Education</h5>
                <div class="row">
                  <div class="col-md-6">
                    <div class="formrow">
                      <input type="text" name="Schoolname" class="form-control" placeholder="School Name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="formrow">
                      <input type="text" name="degreename" class="form-control" placeholder="Degree Name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="formrow">
					<input type="text" name="degreedate" class="form-control" placeholder="Degree Compelete Date">

                    </div>
                  </div>
                </div>
                <a href="#.">Add Other</a>
                <hr>

                <!-- Experience -->
                <h5>Experience</h5>
                <div class="row">
                  <div class="col-md-6">
                    <div class="formrow">
                      <input type="text" name="company" class="form-control" placeholder="Company">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="formrow">
                      <input type="text" name="webcom" class="form-control" placeholder="Company Website">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="formrow">
                      <select class="form-control" name="join-frm">
                        <option>Join From</option>
                        <option>2011</option>
                        <option>2012</option>
                        <option>2013</option>
                        <option>2014</option>
                        <option>2015</option>
                        <option>2016</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="formrow">
                      <select class="form-control" name="endon">
                        <option>End on</option>
                        <option>2011</option>
                        <option>2012</option>
                        <option>2013</option>
                        <option>2014</option>
                        <option>2015</option>
                        <option>Present</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="formrow">
                      <input type="text" name="location" class="form-control" placeholder="Location">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="formrow">
                      <textarea class="form-control" name="about-company" placeholder="About Company"></textarea>
                    </div>
                  </div>
                </div>
                <a href="#.">Add Other</a>
                <hr>

                <!-- Portfolio -->
                <h5>Portfolio</h5>
                <div class="row">
                  <div class="col-md-6">
                    <div class="formrow">
                      <input type="text" name="projname" class="form-control" placeholder="Project Name">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="formrow">
                      <select class="form-control" name="startfrm">
                        <option>Start From</option>
                        <option>2011</option>
                        <option>2012</option>
                        <option>2013</option>
                        <option>2014</option>
                        <option>2015</option>
                        <option>2016</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="formrow">
                      <select class="form-control" name="endon2">
                        <option>End on</option>
                        <option>2011</option>
                        <option>2012</option>
                        <option>2013</option>
                        <option>2014</option>
                        <option>2015</option>
                        <option>Present</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="formrow">
                      <input type="text" name="projdesc" class="form-control" placeholder="Project Short Description">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="formrow">

                    </div>
                  </div>
                </div>
                <a href="#.">Add Other</a>
                <hr>


                <br>
                <a href="./student-profile.html" class="btn btn-primary">Save(on Working)</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">

    <!-- <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved. -->
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- Bootstrap's JavaScript -->
<script src="../assets/js/jquery-2.1.4.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>

<!-- Custom js -->
<!-- <script src="assets/js/script.js"></script> -->
</body>
</html>
