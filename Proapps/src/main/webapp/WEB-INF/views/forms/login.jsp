<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login - PROPAPPS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="resources/css/font-awesome.css" rel="stylesheet">
<!-- Custom Style -->
<link href="resources/css/main.css" rel="stylesheet">

<style>
  .wrapper {
    position: relative;
    height: 100vh;
  }

  .content-wrapper.login-page{
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
</style>

</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->

  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper login-page">
    <!-- Content Header (Page header) -->

	<form action="signInCompany" method="post">

    <!-- Main content -->
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="userccount">
            <div class="socialLogin">
              <h5>Login Or Register with Social</h5>
              <a href="#." class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a>
              <a href="#." class="gp"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
              </div>
            <h5 class="text-center">User Login</h5>
            <!-- login form -->
            <div class="formpanel">
              <div class="formrow">
                <input type="text" name = "login" class="form-control" placeholder="Login">
              </div>
              <div class="formrow">
                <input type="password" name = "pwd" class="form-control" placeholder="Password (5 characters minimum and 20 maximum)" required pattern='{5,20}'>
              </div>


              <input type="submit" style="margin-bottom:10px;" class="btn" value="Login as Company">


            </div>
            <!-- login form  end-->

            <!-- sign up form -->
            <div class="newuser"><i class="fa fa-user" aria-hidden="true"></i> New Company? <a href="registerLogout">Sign Up Here</a></div>
            <!-- sign up form end-->
            <!-- sign up form -->
            <div class="newuser"><i class="fa fa-user" aria-hidden="true"></i> New School? <a href="registerSchool">Sign Up Here</a></div>
            <!-- sign up form end-->
          </div>
        </div>

      </div>
    </div>

    </form>


    <form action="signInStudent" method="post">

  <br>
  <br>
  <br>
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="userccount">
            <div class="socialLogin">

            <h5 class="text-center">Student Login</h5>
            <!-- login form -->
            <div class="formpanel">
              <div class="formrow">
                <input type="text" name = "login" class="form-control" placeholder="Login">
              </div>
              <div class="formrow">
                <input type="password" name = "pwd" class="form-control" placeholder="Password (5 characters minimum and 20 maximum)" required pattern='{5,20}'>
              </div>


              <input type="submit" style="margin-bottom:10px;" class="btn" value="Login as student">



            </div>
            <!-- login form  end-->

            <!-- sign up form -->

            <!-- sign up form end-->
            <!-- sign up form -->
            <div class="newuser"><i class="fa fa-user"></i>Student ?<a href="registerLogoutStudent"> <p> Sign Up Here</p></a>
            </div>
            <!-- sign up form end-->

          </div>
        </div>

      </div>
    </div>

    </form>
     <br>
  <br>
  <br>
    <form action="signInSchool" method="post">

    <!-- Main content -->
    <div class="container"> 
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="userccount">
            <h5 class="text-center">School Login</h5>
            <!-- login form -->
            <div class="formpanel">
              <div class="formrow">
                <input type="text" name = "login" class="form-control" placeholder="Login">
              </div>
              <div class="formrow">
                <input type="password" name = "pwd" class="form-control" placeholder="Password (5 characters minimum and 20 maximum)" required pattern='{5,20}'>
              </div>


              <input type="submit" style="margin-bottom:10px;" class="btn" value="Login as School">


            </div>
            <!-- login form  end-->

            <!-- sign up form -->
            <div class="newuser"><i class="fa fa-user" aria-hidden="true"></i> New School? <a href="registerSchool">Sign Up Here</a></div>
            <!-- sign up form end-->
          </div>
        </div>

      </div>
    </div>

    </form>

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">

    <!-- <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved. -->
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- Bootstrap's JavaScript -->
<script src="resources/js/jquery-2.1.4.min.js"></script>
<script src="resources/js/bootstrap.min.js"></script>

<!-- Custom js -->
<script src="resources/js/script.js"></script>
</body>
</html>
