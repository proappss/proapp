<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>School Profile - PROPAPPS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="resources/css/font-awesome.css" rel="stylesheet">
<!-- Custom Style -->
<link href="resources/css/main.css" rel="stylesheet">

  <style>
    .full-link {
      width: 97%;
    height: 94%;
    z-index: 1;
    position: absolute;
    }

    ul>li {
      list-style-type: none;
    }

    .details {
      padding: 0px;
    }

    .responsibilities {
      margin-top: 50px;
      border-top: 2px solid black;
      padding-top: 100px;
    }

    .job-header {
    background: #fff;
    border: 1px solid #e4e4e4;
    margin-bottom: 30px;
}

.job-header .jobinfo {
    padding: 25px;
    border-bottom: 1px solid #e4e4e4;
}

.content-wrapper {
  margin: 0 !important;
}

.cadsocial a {
    color: #666;
    font-size: 24px;
    display: inline-block;
    margin-right: 7px;
}
.userPic {
    border: none !important;
}

.clickable {
  position: absolute;
  width: 100%;
  height: 100%;
  z-index: 1;
  top: 0;
  left: 0;
}

.job-header .contentbox .userPortfolio li {
  height: 200px;
}
  </style>
</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->

  <!-- Main Sidebar Container -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="margin: 0 !important;">
    <!-- Header start -->
<div class="header">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-3 col-xs-12">
        <a href="#" class="logo"><img src="resources/images/Logo.png" alt="Logo" /></a>
         <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="col-md-10 col-sm-12 col-xs-12">
        <!-- Nav start -->
        <div class="navbar navbar-default" role="navigation">
          <div class="navbar-collapse collapse" id="nav-main">
            <ul class="nav navbar-nav">

              <li><a href="schoolProfile?id=${ school.idSchool}">Profile </a></li>
              <li><a href="signOutCompany">Sign Out </a></li>
              <!-- <li class="postjob"><a href="./webapp/pages/forms/login.html">Login</a></li> -->
              <!-- <li class="jobseeker"><a href="candidate-listing.html">Job Seeker</a></li> -->
              <!-- <div class="social">
                  <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                   <a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                   <a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a> -->
                </div>
            </ul>
            <!-- Nav collapes end -->
          </div>
          <div class="clearfix"></div>
        </div>
        <!-- Nav end -->
      </div>
    </div>
    <!-- row end -->
  </div>
  <!-- Header container end -->
</div>
<!-- Header end -->

    <!-- Main content -->
    <div class="container" style="margin-top:20px;">
        <!-- Job Header start -->
        <div class="job-header">
          <div class="jobinfo">
            <div class="row">
              <div class="col-md-8 col-sm-8">
                <!-- Candidate Info -->
                <div class="candidateinfo">
                  <div class="userPic"><img src="../companies/comp1.png" alt=""></div>
                  <div class="title">${ school.name}</div>
                  <div class="desi">${ school.type}</div>
                  <div class="loctext"><i class="fa fa-history" aria-hidden="true"></i> Member Since, ${ school.dateJoin}</div>
                  <div class="loctext"><i class="fa fa-map-marker" aria-hidden="true"></i> ${ school.addressSchool.nameStreet }</div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-md-4 col-sm-4">
                <!-- Candidate Contact -->
                <div class="candidateinfo">
                  <div class="loctext"><i class="fa fa-phone" aria-hidden="true"></i> ${ school.tel }</div>
                  <div class="loctext"><i class="fa fa-envelope" aria-hidden="true"></i> ${ school.email }</div>
                  <div class="loctext"><i class="fa fa-globe" aria-hidden="true"></i> ${ school.website }</div>
                  <div class="cadsocial">
                      <a href="http://www.twitter.com" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                      <a href="http://www.facebook.com" target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                      <a href="https://www.linkedin.com" target="_blank"><i class="fa fa-instagram-square"></i></a> </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Buttons -->
          <!-- <div class="jobButtons" style="height: 80px;">  -->
              <!-- <a href="#." class="btn apply"><i class="fa fa-paper-plane" aria-hidden="true"></i> Join Us</a>  -->
              <!-- <a href="#." class="btn"><i class="fa fa-envelope" aria-hidden="true"></i> Send Message</a>  -->
              <!-- <a href="school-profile-edit.html" class="btn"> Edit</a>  -->

              <!-- <a href="#." class="btn"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save This Company</a> -->
        <!-- </div> -->
        </div>

        <!-- Job Detail start -->
        <div class="row">
          <div class="col-md-8">
            <!-- About Employee start -->
            <div class="job-header">
              <div class="contentbox">
                <h3>About School</h3>
                <p> ${ school.schoolDesc }</p>
              </div>
            </div>
            <div class="job-header">
                <div class="contentbox">
                  <h3>Educational programs</h3>
                  <ul class="experienceList">
                  <c:forEach items="${ listFormationSchool }" var="formation">
                    <li>
                      <div class="row">
                        <div class="col-md-2"><img src="../webapp/pages/forms/companies/comp1.png" alt="your alt text"></div>
                        <div class="col-md-10">
                          <h4>${formation.title} </h4>
                          <div class="row">
                            <!-- <div class="col-md-4">High School</div> -->
                            <div class="col-md-8">knowledge: ${formation.knwoledge} </div>
                            <!-- <div class="col-md-4">University</div> -->
                            <div class="col-md-8">Description: ${formation.formationDesc} </div>
                          </div>
                          <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque massa vel lorem fermentum fringilla. Pellentesque id est et neque blandit ornare</p> -->
                        </div>
                      </div>
                    </li>
                    </c:forEach>
                  </ul>
                </div>
              </div>
              <div class="job-header">
                <div class="contentbox">
                  <h3>Apprenticeships being offered by companies</h3>
                <!--    <ul class="row userPortfolio">
                    <li class="col-md-3 col-sm-3">
                      <div class="imgbox"><img src="../webapp/pages/forms/companies/comp1.png" alt="your alt text">
                        <div class="itemHover">
                          <div class="zoombox"><a href="" title="images/portfolio/portfolio-img1" class="item-zoom fancybox-effects-a"><i class="fa fa-search" aria-hidden="true"></i></a> <a href="" rel="nofollow" class="item-zoom" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i></a> </div>
                          <div class="infoItem">
                            <div class="itemtitle">
                              <h5>Title Here</h5>
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>-->
                </div>
              </div>
          </div>
          <div class="col-md-4">
            <!-- Company Detail start -->
            <div class="job-header">
              <div class="jobdetail">
                <h3>School Details</h3>
                <ul class="jbdetail">
                  <li class="row">
                    <div class="col-md-6 col-xs-6">Total Employees</div>
                    <div class="col-md-6 col-xs-6"><span>10-50</span></div>
                  </li>
                  <li class="row">
                    <div class="col-md-6 col-xs-6">Established In</div>
                    <div class="col-md-6 col-xs-6"><span>2008</span></div>
                  </li>
                  <li class="row">
                    <div class="col-md-6 col-xs-6">Current jobs</div>
                    <div class="col-md-6 col-xs-6"><span>10</span></div>
                  </li>
                </ul>
              </div>
            </div>

            <!-- Google Map start -->
            <div class="job-header">
              <div class="jobdetail">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d193572.19492844533!2d-74.11808565615137!3d40.70556503857166!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1481975053066" allowfullscreen=""></iframe>
              </div>
            </div>

            <!-- Contact Company start -->
            <div class="job-header">
              <div class="jobdetail">
                <h3>Contact School</h3>
                <div class="formpanel">
                  <div class="formrow">
                    <input type="text" class="form-control" placeholder="Your Name">
                  </div>
                  <div class="formrow">
                    <input type="email" class="form-control" placeholder="Your Email">
                  </div>
                  <div class="formrow">
                    <input type="tel" class="form-control" placeholder="Phone">
                  </div>
                  <div class="formrow">
                    <input type="text" class="form-control" placeholder="Subject">
                  </div>
                  <div class="formrow">
                    <textarea class="form-control" placeholder="Message"></textarea>
                  </div>
                  <input type="submit" class="btn" value="Submit">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>

  <footer class="main-footer">
    <!-- <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div> -->
    <!-- <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved. -->
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- Bootstrap's JavaScript -->
<script src="resources/js/jquery-2.1.4.min.js"></script>
<script src="resources/js/bootstrap.min.js"></script>

<!-- Custom js -->
<!-- <script src="assets/js/script.js"></script> -->
</body>
</html>
