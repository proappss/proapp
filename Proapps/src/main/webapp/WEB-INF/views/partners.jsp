<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Partners - PROAPPS</title>
<!-- Fav Icon -->
<link rel="shortcut icon" href="/resources/images/favicon.ico">

<!-- Owl carousel -->
<link href="resources/css/owl.carousel.css" rel="stylesheet">

<!-- Bootstrap -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<!-- Font Awesome -->
<link href="resources/css/font-awesome.css" rel="stylesheet">

<!-- Custom Style -->
<link href="resources/css/main.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="js/html5shiv.min.js"></script>
  <script src="js/respond.min.js"></script>
<![endif]-->

</head>
<body>
<!-- Header start -->
<div class="header">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-3 col-xs-12"> <a href="indexLogout" class="logo"><img src="resources/images/Logo.png" alt="Logo" /></a>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="col-md-10 col-sm-12 col-xs-12">
        <div class="navbar navbar-default" role="navigation">
          <div class="navbar-collapse collapse" id="nav-main">
            <ul class="nav navbar-nav">
	              	<li><a href="indexLogout">Home </a></li>
					<li><a href="aboutLogout">About us</a></li>
					<li class="active"><a href="partnersLogout">Partners</a></li>
					<li><a href="contactUsLogout">Contact</a></li>
					<li class="postjob"><a href="loginLogout">Login</a></li>
                <div class="social"> 
                  	<a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a> 
                   	<a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a> 
                   	<a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a> 
                </div>
              </ul>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Header end --> 

<!-- --------------------------------------------------------------------------------------------------- -->
<div class="about-wraper"> 
  <!-- About -->
  <div class="container">
    <div class="row">
      <h2 style="margin-bottom: 100px;">About PARTNERS</h2>
      <div class="col-md-3">
          <div class="postimg"><img src="resources/images/ECLEE.jpg" alt="your alt text" /></div>
      </div>
      <div class="col-md-6">
		    <h4>PATOU INTERNATIONAL, FRANCE</h4>
		    <a href="https://www.eclee.com">https://www.eclee.com</a>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad, illum optio nostrum cupiditate repudiandae aspernatur vero asperiores sed reiciendis enim atque aliquam accusantium maxime assumenda sequi? Corporis ea aut quasi!</p>
      </div>
      <!-- <div class="col-md-2">
        <p>Advertising</p>
        <img src="assets/images/about-us-img1.jpg" style="width:100%;" alt="your alt text" />
      </div> -->
</div>
    <div class="row" style="margin-top:20px;">
      <div class="col-md-3">
          <div class="postimg"><img src="resources/images/univPartner.png" alt="your alt text" /></div>
      </div>
      <div class="col-md-6">
        <h4>UNIVERZA V LJUBLJANI, SLOVENIA</h4>
          <a href="http://www.ef.uni-lj.si">http://www.ef.uni-lj.si</a><br>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad, illum optio nostrum cupiditate repudiandae aspernatur vero asperiores sed reiciendis enim atque aliquam accusantium maxime assumenda sequi? Corporis ea aut quasi!</p>
      </div>
      <!-- <div class="col-md-2">
        <p>Advertising</p>
        <img src="assets/images/about-us-img1.jpg" style="width:100%;" alt="your alt text" />
      </div> -->
    </div>
    <div class="row" style="margin-top:20px;">
      <div class="col-md-3">
          <div class="postimg"><img src="resources/images/OEMVP.png" alt="your alt text" /></div>
      </div>
      <div class="col-md-6">
        <h4>ECONOMIC CHAMBER OF NORTH-WEST MACEDONIA, MACEDONIA</h4>
        <a href="http://oemvp.org">http://oemvp.org</a> <br>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad, illum optio nostrum cupiditate repudiandae aspernatur vero asperiores sed reiciendis enim atque aliquam accusantium maxime assumenda sequi? Corporis ea aut quasi!</p>
      </div>
      <!-- <div class="col-md-2">
        <p>Advertising</p>
        <img src="assets/images/about-us-img1.jpg" style="width:100%;" alt="your alt text" />
      </div> -->
    </div>
    <div class="row"  style="margin-top:20px;">
      <div class="col-md-3">
          <div class="postimg"><img src="resources/images/about-us-img1.jpg" alt="your alt text" /></div>
      </div>
      <div class="col-md-6">
      <h4>IDEA O.K. TRAINING AND CONSULTING </h4>
          <a href="https://www.ideaok.mk">https://www.ideaok.mk</a> <br>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad, illum optio nostrum cupiditate repudiandae aspernatur vero asperiores sed reiciendis enim atque aliquam accusantium maxime assumenda sequi? Corporis ea aut quasi!</p>
      </div>
      <!-- <div class="col-md-2">
        <p>Advertising</p>
        <img src="assets/images/about-us-img1.jpg" style="width:100%;" alt="your alt text" />
      </div> -->
    </div>
  </div>

</div>
<!--Footer-->
<div class="footerWrap">
  <div class="container">
    <div class="row"> 
      <!--About Us-->
      <div class="col-md-3 col-sm-12">
        <div class="ft-logo"><img src="resources/images/Logo.png" alt="Logo"></div>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et consequat elit. Proin molestie eros sed urna auctor lobortis. </p>
        
        <!-- Social Icons -->
        <div class="social"> <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a></div>
        <!-- Social Icons end --> 
      </div>
      <!--About us End--> 
      
      <!--Quick Links-->
      <div class="col-md-2 col-sm-6">
        <h5>Quick Links</h5>
        <!--Quick Links menu Start-->
        <ul class="quicklinks">
          <li><a href="#.">Career Services</a></li>
          <li><a href="#.">CV Writing</a></li>
          <li><a href="#.">Career Resources</a></li>
          <li><a href="#.">Company Listings</a></li>
          <li><a href="#.">Success Stories</a></li>
        </ul>
      </div>
      <!--Quick Links menu end--> 
      
      <!--Jobs By Industry-->
      <div class="col-md-3 col-sm-6">
        <h5>Jobs By Industry</h5>
        <!--Industry menu Start-->
        <ul class="quicklinks">
          <li><a href="#.">Information Technology Jobs</a></li>
          <li><a href="#.">Recruitment/Employment Firms Jobs</a></li>
          <li><a href="#.">Education/Training Jobs</a></li>
          <li><a href="#.">Manufacturing Jobs</a></li>
          <li><a href="#.">Call Center Jobs</a></li>
        </ul>
        <!--Industry menu End-->
        <div class="clear"></div>
      </div>
      
      <!--Latest Articles-->
      <div class="col-md-4 col-sm-12">
        <div class="ft-logo" style="width:80%;"><img src="resources/images/erasmusplus-logo-all-en-300dpi.jpg" /></div>
      </div>
    </div>
  </div>
</div>
<!--Footer end--> 


<!--Copyright-->
<div class="copyright">
    <div class="container">
      <div class="bttxt">Copyright &copy; 2020. All Rights Reserved. Design by: PROAPPS</a></div>
    </div>
  </div>

<!-- Bootstrap's JavaScript --> 
<script src="resources/js/jquery-2.1.4.min.js"></script> 
<script src="resources/js/bootstrap.min.js"></script> 

<!-- Owl carousel --> 
<script src="resources/js/owl.carousel.js"></script> 

<!-- Custom js --> 
<script src="resources/js/script.js"></script>
</body>
</html>
