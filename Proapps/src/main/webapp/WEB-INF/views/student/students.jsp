<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Student - PROAPPS</title>
<!-- Fav Icon -->
<link rel="shortcut icon" href="/resources/images/favicon.ico">

<!-- Owl carousel -->
<!-- <link href="resources/css/owl.carousel.css" rel="stylesheet"> -->

<!-- Bootstrap -->
<link href="../resources/css/bootstrap.min.css" rel="stylesheet">

<!-- Font Awesome -->
<link href="../resources/css/font-awesome.css" rel="stylesheet">

<!-- Custom Style -->
<link href="../resources/css/main.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="js/html5shiv.min.js"></script>
  <script src="js/respond.min.js"></script>
<![endif]-->

</head>
<body>
<!-- Header start -->
<div class="header">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-3 col-xs-12"> <a href="../index.html" class="logo"><img src="resources/images/logo.png" alt="Logo" /></a>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="col-md-10 col-sm-12 col-xs-12">
        <div class="navbar navbar-default" role="navigation">
          <div class="navbar-collapse collapse" id="nav-main">
            <ul class="nav navbar-nav">
              <li><a href="studentHome?id=${ student.idStudent }">Home </a></li>
              <li><a href="../about.html">About us</a></li>
              <li><a href="../partners.html">Partners</a></li>
              <li><a href="../contact-us.html">Contact</a></li>
              <li class="postjob"><a href="../webapp/pages/forms/login.html">Login</a></li>
              <div class="social"> 
                <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a> 
                 <a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a> 
                 <a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a> 
              </div>
              </ul>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Header end --> 

<div class="about-wraper"> 
  <!-- About -->
  <div class="container">
    <div class="row">
        <div class="col-md-5">
            <div class="postimg"><img src="../resources/images/about-us-img1.jpg" alt="your alt text" /></div>
          </div>
      <div class="col-md-7">
        <h2>About Students Section</h2>
        <p>Gain a qualification which is recognized, strengthen your skills, gain experience and enhance your employability.<br>
		<br>Engaging in an apprenticeship will provide you with opportunity to have a genuine job! Your job will be based on skills development programme with an accompanying assessment.<br>
		<br>The programme will make sure that the apprentices will shape what you learn and help you progress towards set career goals.<br>
		<br>A specialist in the field of your interest will guide you reach these career goals. This guidance is set on one-to-one basis.<br>
		<br>If you see yourself as an apprentice, then you:<br></p>
		<div class="col-md-12"> 
		  <ul>
			  <li>* Have a clear focus on which career you might want to pursue </li>
			  <li>* Committed to work and study whereas work is your main focus </li>
			  <li>* Are ready to take up the competing load of work and school in the same time</li>
			  <li>* Feel at ease with assignments and assessments at work and school</li>
			</ul><br><br>
		  </div>
      </div>
      
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12"> 
        <!-- Search List -->
        <ul class="searchList">
          <!-- Candidate -->
          <li>
            <div class="row">
              <div class="col-md-5 col-sm-5">
                <div class="jobimg"><img src="../webapp/pages/forms/candidate-img/candidate.jpg" alt="Candidate Name"></div>
                <div class="jobinfo">
                  <h3><a href="#.">Jhon Doe</a></h3>
                  <div class="cateinfo">Java Developer</div>
                  <div class="location"> New York City, New York</div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="col-md-4 col-sm-4">
                <!-- <div class="minsalary">$5000 <span>/ month</span></div> -->
              </div>
              <div class="col-md-3 col-sm-3">
                <div class="listbtn"><a href="student-cv.html">View Profile</a></div>
              </div>
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu nulla eget nisl dapibus finibus. Maecenas quis sem vel neque rhoncus dignissim. Ut et eros rhoncus, gravida tellus auctor,</p>
          </li>
          <!-- Candidate -->
          <li>
            <div class="row">
              <div class="col-md-5 col-sm-5">
                <div class="jobimg"><img src="../webapp/pages/forms/candidate-img/candidate.jpg" alt="Candidate Name"></div>
                <div class="jobinfo">
                  <h3><a href="#.">Jhon Doe</a></h3>
                  <div class="cateinfo">Java Developer</div>
                  <div class="location"> New York City, New York</div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="col-md-4 col-sm-4">
                <!-- <div class="minsalary">$5000 <span>/ month</span></div> -->
              </div>
              <div class="col-md-3 col-sm-3">
                <div class="listbtn"><a href="student-cv.html">View Profile</a></div>
              </div>
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu nulla eget nisl dapibus finibus. Maecenas quis sem vel neque rhoncus dignissim. Ut et eros rhoncus, gravida tellus auctor,</p>
          </li>
          <!-- Candidate -->
          <li>
            <div class="row">
              <div class="col-md-5 col-sm-5">
                <div class="jobimg"><img src="../webapp/pages/forms/candidate-img/candidate.jpg" alt="Candidate Name"></div>
                <div class="jobinfo">
                  <h3><a href="#.">Jhon Doe</a></h3>
                  <div class="cateinfo">Java Developer</div>
                  <div class="location"> New York City, New York</div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="col-md-4 col-sm-4">
                <!-- <div class="minsalary">$5000 <span>/ month</span></div> -->
              </div>
              <div class="col-md-3 col-sm-3">
                <div class="listbtn"><a href="student-cv.html">View Profile</a></div>
              </div>
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu nulla eget nisl dapibus finibus. Maecenas quis sem vel neque rhoncus dignissim. Ut et eros rhoncus, gravida tellus auctor,</p>
          </li>
          <!-- Candidate -->
          <li>
            <div class="row">
              <div class="col-md-5 col-sm-5">
                <div class="jobimg"><img src="../webapp/pages/forms/candidate-img/candidate.jpg" alt="Candidate Name"></div>
                <div class="jobinfo">
                  <h3><a href="#.">Jhon Doe</a></h3>
                  <div class="cateinfo">Java Developer</div>
                  <div class="location"> New York City, New York</div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="col-md-4 col-sm-4">
                <!-- <div class="minsalary">$5000 <span>/ month</span></div> -->
              </div>
              <div class="col-md-3 col-sm-3">
                <div class="listbtn"><a href="student-cv.html">View Profile</a></div>
              </div>
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu nulla eget nisl dapibus finibus. Maecenas quis sem vel neque rhoncus dignissim. Ut et eros rhoncus, gravida tellus auctor,</p>
          </li>
          <!-- Candidate -->
          <li>
            <div class="row">
              <div class="col-md-5 col-sm-5">
                <div class="jobimg"><img src="../webapp/pages/forms/candidate-img/candidate.jpg" alt="Candidate Name"></div>
                <div class="jobinfo">
                  <h3><a href="#.">Jhon Doe</a></h3>
                  <div class="cateinfo">Java Developer</div>
                  <div class="location"> New York City, New York</div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="col-md-4 col-sm-4">
                <!-- <div class="minsalary">$5000 <span>/ month</span></div> -->
              </div>
              <div class="col-md-3 col-sm-3">
                <div class="listbtn"><a href="student-cv.html">View Profile</a></div>
              </div>
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu nulla eget nisl dapibus finibus. Maecenas quis sem vel neque rhoncus dignissim. Ut et eros rhoncus, gravida tellus auctor,</p>
          </li>
          <!-- Candidate -->
          <li>
            <div class="row">
              <div class="col-md-5 col-sm-5">
                <div class="jobimg"><img src="../webapp/pages/forms/candidate-img/candidate.jpg" alt="Candidate Name"></div>
                <div class="jobinfo">
                  <h3><a href="#.">Jhon Doe</a></h3>
                  <div class="cateinfo">Java Developer</div>
                  <div class="location"> New York City, New York</div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="col-md-4 col-sm-4">
                <!-- <div class="minsalary">$5000 <span>/ month</span></div> -->
              </div>
              <div class="col-md-3 col-sm-3">
                <div class="listbtn"><a href="student-cv.html">View Profile</a></div>
              </div>
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu nulla eget nisl dapibus finibus. Maecenas quis sem vel neque rhoncus dignissim. Ut et eros rhoncus, gravida tellus auctor,</p>
          </li>
          <!-- Candidate -->
          <li>
            <div class="row">
              <div class="col-md-5 col-sm-5">
                <div class="jobimg"><img src="../webapp/pages/forms/candidate-img/candidate.jpg" alt="Candidate Name"></div>
                <div class="jobinfo">
                  <h3><a href="#.">Jhon Doe</a></h3>
                  <div class="cateinfo">Java Developer</div>
                  <div class="location"> New York City, New York</div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="col-md-4 col-sm-4">
                <!-- <div class="minsalary">$5000 <span>/ month</span></div> -->
              </div>
              <div class="col-md-3 col-sm-3">
                <div class="listbtn"><a href="student-cv.html">View Profile</a></div>
              </div>
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu nulla eget nisl dapibus finibus. Maecenas quis sem vel neque rhoncus dignissim. Ut et eros rhoncus, gravida tellus auctor,</p>
          </li>
          <!-- Candidate -->
          <li>
            <div class="row">
              <div class="col-md-5 col-sm-5">
                <div class="jobimg"><img src="../webapp/pages/forms/candidate-img/candidate.jpg" alt="Candidate Name"></div>
                <div class="jobinfo">
                  <h3><a href="#.">Jhon Doe</a></h3>
                  <div class="cateinfo">Java Developer</div>
                  <div class="location"> New York City, New York</div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="col-md-4 col-sm-4">
                <!-- <div class="minsalary">$5000 <span>/ month</span></div> -->
              </div>
              <div class="col-md-3 col-sm-3">
                <div class="listbtn"><a href="student-cv.html">View Profile</a></div>
              </div>
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu nulla eget nisl dapibus finibus. Maecenas quis sem vel neque rhoncus dignissim. Ut et eros rhoncus, gravida tellus auctor,</p>
          </li>
          <!-- Candidate -->
          <li>
            <div class="row">
              <div class="col-md-5 col-sm-5">
                <div class="jobimg"><img src="../webapp/pages/forms/candidate-img/candidate.jpg" alt="Candidate Name"></div>
                <div class="jobinfo">
                  <h3><a href="#.">Jhon Doe</a></h3>
                  <div class="cateinfo">Java Developer</div>
                  <div class="location"> New York City, New York</div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="col-md-4 col-sm-4">
                <!-- <div class="minsalary">$5000 <span>/ month</span></div> -->
              </div>
              <div class="col-md-3 col-sm-3">
                <div class="listbtn"><a href="student-cv.html">View Profile</a></div>
              </div>
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu nulla eget nisl dapibus finibus. Maecenas quis sem vel neque rhoncus dignissim. Ut et eros rhoncus, gravida tellus auctor,</p>
          </li>
          <!-- Candidate -->
          <li>
            <div class="row">
              <div class="col-md-5 col-sm-5">
                <div class="jobimg"><img src="../webapp/pages/forms/candidate-img/candidate.jpg" alt="Candidate Name"></div>
                <div class="jobinfo">
                  <h3><a href="#.">Jhon Doe</a></h3>
                  <div class="cateinfo">Java Developer</div>
                  <div class="location"> New York City, New York</div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="col-md-4 col-sm-4">
                <!-- <div class="minsalary">$5000 <span>/ month</span></div> -->
              </div>
              <div class="col-md-3 col-sm-3">
                <div class="listbtn"><a href="student-cv.html">View Profile</a></div>
              </div>
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu nulla eget nisl dapibus finibus. Maecenas quis sem vel neque rhoncus dignissim. Ut et eros rhoncus, gravida tellus auctor,</p>
          </li>
        </ul>
        
        <!-- Pagination Start -->
        <div class="pagiWrap">
          <div class="row">
            <div class="col-md-4 col-sm-6">
              <div class="showreslt">Showing 1-10</div>
            </div>
            <div class="col-md-8 col-sm-6 text-right">
              <ul class="pagination">
                <li class="active"><a href="#.">1</a></li>
                <li><a href="#.">2</a></li>
                <li><a href="#.">3</a></li>
                <li><a href="#.">4</a></li>
                <li><a href="#.">5</a></li>
                <li><a href="#.">6</a></li>
                <li><a href="#.">7</a></li>
                <li><a href="#.">8</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

<!--Footer-->
<div class="footerWrap">
    <div class="container">
      <div class="row"> 
        <!--About Us-->
        <div class="col-md-3 col-sm-12">
          <div class="ft-logo"><img src="../resources/images/logo.png" alt="Logo"></div>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et consequat elit. Proin molestie eros sed urna auctor lobortis. </p>
          
          <!-- Social Icons -->
          <div class="social"> <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a> <a href="#." target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a> <a href="#." target="_blank"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a> <a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a> <a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a> </div>
          <!-- Social Icons end --> 
        </div>
        <!--About us End--> 
        
        <!--Quick Links-->
        <div class="col-md-2 col-sm-6">
          <h5>Quick Links</h5>
          <!--Quick Links menu Start-->
          <ul class="quicklinks">
            <li><a href="#.">Career Services</a></li>
            <li><a href="#.">CV Writing</a></li>
            <li><a href="#.">Career Resources</a></li>
            <li><a href="#.">Company Listings</a></li>
            <li><a href="#.">Success Stories</a></li>
          </ul>
        </div>
        <!--Quick Links menu end--> 
        
        <!--Jobs By Industry-->
        <div class="col-md-3 col-sm-6">
          <h5>Jobs By Industry</h5>
          <!--Industry menu Start-->
          <ul class="quicklinks">
            <li><a href="#.">Information Technology Jobs</a></li>
            <li><a href="#.">Recruitment/Employment Firms Jobs</a></li>
            <li><a href="#.">Education/Training Jobs</a></li>
            <li><a href="#.">Manufacturing Jobs</a></li>
            <li><a href="#.">Call Center Jobs</a></li>
          </ul>
          <!--Industry menu End-->
          <div class="clear"></div>
        </div>
        
        <!--Latest Articles-->
        <div class="col-md-4 col-sm-12">
          <h5>Latest Articles</h5>
          <ul class="posts-list">
            <!--Article 1-->
            <li>
              <article class="post post-list">
                <div class="entry-content media">
                  <div class="media-left"> <a href="#." title="" class="entry-image"> <img width="80" height="80" src="resources/images/news-1.jpg" alt="Your alt text here"> </a> </div>
                  <div class="media-body">
                    <h4 class="entry-title"> <a href="#.">Sed fermentum at lectus nec porta.</a> </h4>
                    <div class="entry-content-inner">
                      <div class="entry-meta"> <span class="entry-date">Jan 28, 2016</span> </div>
                    </div>
                  </div>
                </div>
              </article>
            </li>
            <!--Article end 1--> 
            
            <!--Article 2-->
            <li>
              <article class="post post-list">
                <div class="entry-content media">
                  <div class="media-left"> <a href="#." title="" class="entry-image"> <img width="80" height="80" src="resources/images/news-2.jpg" alt="Your alt text here"> </a> </div>
                  <div class="media-body">
                    <h4 class="entry-title"> <a href="#.">Sed fermentum at lectus nec porta.</a> </h4>
                    <div class="entry-content-inner">
                      <div class="entry-meta"> <span class="entry-date">Jan 28, 2016</span> </div>
                    </div>
                  </div>
                </div>
              </article>
            </li>
            <!--Article end 2--> 
          </ul>
        </div>
      </div>
    </div>
  </div>
<!--Footer end--> 

<!--Copyright-->
<div class="copyright">
    <div class="container">
      <div class="bttxt">Copyright &copy; 2020. All Rights Reserved. Design by: PROAPPS</a></div>
    </div>
  </div>

<!-- Bootstrap's JavaScript --> 
<script src="../resources/js/jquery-2.1.4.min.js"></script> 
<script src="../resources/js/bootstrap.min.js"></script> 

<!-- Owl carousel --> 
<!-- <script src="../resources/js/owl.carousel.js"></script>  -->

<!-- Custom js --> 
<script src="resources/js/script.js"></script>
</body>
</html>