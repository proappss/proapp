<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Candidate Profile - PROPAPPS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link href="../resources/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="../resources/css/font-awesome.css" rel="stylesheet">
<!-- Custom Style -->
<link href="../resources/css/main.css" rel="stylesheet">
  <style>
    .full-link {
      width: 97%;
    height: 94%;
    z-index: 1;
    position: absolute;
    }

    .card {
      width: 100%;
    }

    .sidebar {
      height: auto !important;
    }

    .pull-right {
      float: right;
    }
  </style>
</head>
<body>
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->

  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="margin:0 !important;">
    <div class="header">
      <div class="container">
        <div class="row">
          <div class="col-md-2 col-sm-3 col-xs-12">
            <a href="index.html" class="logo" style="font-size: 22px;">
            <!-- <img src="../resources/images/logo.png" alt="" /> -->
              TEMPORARY LOGO
            </a>
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="col-md-10 col-sm-12 col-xs-12">
            <!-- Nav start -->
            <div class="navbar navbar-default" role="navigation">
              <div class="navbar-collapse collapse" id="nav-main">
                <ul class="nav navbar-nav">
                  <li><a href="../index.html">Home </a></li>
                  <li><a href="../about.html">About us</a></li>
                  <li><a href="../partners.html">Partners</a></li>
                  <li><a href="../contact-us.html">Contact</a></li>
                  <li class="postjob"><a href="../webapp/pages/forms/login.html">Login</a></li>
                  <div class="social">
                    <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                     <a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                     <a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
                  </div>
                    </ul>
                <!-- Nav collapes end -->
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- Nav end -->
          </div>
        </div>
        <!-- row end -->
      </div>
      <!-- Header container end -->
    </div>
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <div class="container">
      <div class="row mb-2 margin-top30">
        <div class="col-sm-12">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Candidate CV</li>
          </ol>
        </div>
      </div>
        <!-- Job Header start -->
        <div class="job-header">
          <div class="jobinfo">
            <div class="row">
              <div class="col-md-8 col-sm-8">
                <!-- Candidate Info -->
                <div class="candidateinfo">
                  <div class="userPic"><img src="../webapp/pages/forms/candidate-img/candidate.jpg" alt=""></div>
                  <div class="title">Elliot Scott</div>
                  <div class="desi">Senior Data Analytist</div>
                  <div class="loctext"><i class="fa fa-history" aria-hidden="true"></i> Member Since, Aug 14, 2016</div>
                  <div class="loctext"><i class="fa fa-map-marker" aria-hidden="true"></i> 123 Boscobel Street, NW8 8PS</div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-md-4 col-sm-4">
                <!-- Candidate Contact -->
                <div class="candidateinfo">
                  <div class="loctext"><i class="fa fa-phone" aria-hidden="true"></i> (+1) 123.456.7890</div>
                  <div class="loctext"><i class="fa fa-envelope" aria-hidden="true"></i> info@mywebsite.com</div>
                  <div class="loctext"><i class="fa fa-globe" aria-hidden="true"></i> www.mywebsite.com</div>
                  <div class="cadsocial"> <a href="http://www.twitter.com" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a> <a href="http://www.plus.google.com" target="_blank"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a> <a href="http://www.facebook.com" target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a> <a href="https://www.pinterest.com" target="_blank"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a> <a href="https://www.linkedin.com" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a> </div>
                </div>
              </div>
            </div>
          </div>

          <!-- Buttons -->

        </div>

        <!-- Job Detail start -->
        <div class="row">
          <div class="col-md-8">
            <!-- About Employee start -->
            <div class="job-header">
              <div class="contentbox">
                <h3>About me</h3>
                <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque massa vel lorem fermentum fringilla. Pellentesque id est et neque blandit ornare malesuada a mauris. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sagittis, quam a fringilla congue, turpis turpis molestie ligula, ut laoreet elit arcu sed nulla. Sed at quam commodo, egestas turpis eget, fringilla dui. Etiam sit amet nulla metus. Etiam iaculis lobortis ultricies. <strong>Maecenas maximus magna a metus consectetur, vel fermentum nisl ultrices</strong>. Quisque eget ante id dui posuere ullamcorper ut molestie eros. Aliquam ultrices lacinia risus, at lacinia ante vehicula id. Nulla in lectus dignissim, egestas purus sit amet, mattis libero. Maecenas ullamcorper rutrum porta. </p>
                <ul>
                  <li>In non augue eget purus placerat aliquet sit amet lobortis lacus.</li>
                  <li>Curabitur interdum nisl quis placerat ornare.</li>
                  <li>Curabitur ornare enim ac aliquam efficitur.</li>
                  <li>Etiam volutpat leo et orci luctus, blandit accumsan arcu placerat.</li>
                  <li>Proin egestas dui et pulvinar pellentesque.</li>
                  <li>In ultricies nulla eget lacus tempor lobortis.</li>
                </ul>
              </div>
            </div>

            <!-- Education start -->
            <div class="job-header">
              <div class="contentbox">
                <h3>Education</h3>
                <ul class="educationList">
                  <li>
                    <div class="date">31<br>
                      May<br>
                      2012</div>
                    <h4>Kiril Pejcinoviq </h4>
                    <!-- <p>Educational Programe: General</p> -->
                    <p>4 years</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque massa vel lorem fermentum fringilla. Pellentesque id est et neque blandit ornare malesuada a mauris.</p>
                    <div class="clearfix"></div>
                  </li>
                  <li>
                    <div class="date">31<br>
                      May<br>
                      2015</div>
                    <h4>South East European University </h4>
                    <p>Educational Programe: Bachelor </p>
                    <p>3 years</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque massa vel lorem fermentum fringilla. Pellentesque id est et neque blandit ornare malesuada a mauris.</p>
                    <div class="clearfix"></div>
                  </li>
                  <li>
                    <div class="date">31<br>
                      May<br>
                      2017</div>
                    <h4>South East European University </h4>
                    <p>Educational Programe: Master </p>
                    <p>2 years</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque massa vel lorem fermentum fringilla. Pellentesque id est et neque blandit ornare malesuada a mauris.</p>
                    <div class="clearfix"></div>
                  </li>
                </ul>
              </div>
            </div>

            <!-- Experience start -->
            <div class="job-header">
              <div class="contentbox">
                <h3>Professional experience</h3>
                <ul class="experienceList">
                  <li>
                    <div class="row">
                      <div class="col-md-2"><img src="../webapp/pages/forms/companies/comp1.png" alt="your alt text"></div>
                      <div class="col-md-10">
                        <h4>Company Name</h4>
                        <div class="row">
                          <div class="col-md-6">www.companywebsite.com</div>
                          <div class="col-md-6">From 2014 - Present</div>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque massa vel lorem fermentum fringilla. Pellentesque id est et neque blandit ornare</p>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="row">
                      <div class="col-md-2"><img src="../webapp/pages/forms/companies/comp2.png" alt="your alt text"></div>
                      <div class="col-md-10">
                        <h4>Company Name</h4>
                        <div class="row">
                          <div class="col-md-6">www.companywebsite.com</div>
                          <div class="col-md-6">From 2014 - Present</div>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque massa vel lorem fermentum fringilla. Pellentesque id est et neque blandit ornare</p>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="row">
                      <div class="col-md-2"><img src="../webapp/pages/forms/companies/comp3.png" alt="your alt text"></div>
                      <div class="col-md-10">
                        <h4>Company Name</h4>
                        <div class="row">
                          <div class="col-md-6">www.companywebsite.com</div>
                          <div class="col-md-6">From 2014 - Present</div>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque massa vel lorem fermentum fringilla. Pellentesque id est et neque blandit ornare</p>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>

            <!-- Portfolio start -->
            <div class="job-header">
              <div class="contentbox">
                <h3>Portfolio</h3>
                <ul class="row userPortfolio">
                  <li class="col-md-6 col-sm-6">
                    <div class="imgbox"><img src="../webapp/pages/forms/companies/comp1.png" alt="your alt text">
                      <div class="itemHover">
                        <div class="zoombox"><a href="" title="images/portfolio/portfolio-img1" class="item-zoom fancybox-effects-a"><i class="fa fa-search" aria-hidden="true"></i></a> <a href="" rel="nofollow" class="item-zoom" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i></a> </div>
                        <div class="infoItem">
                          <div class="itemtitle">
                            <h5>Title Here</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="col-md-6 col-sm-6">
                    <div class="imgbox"><img src="../webapp/pages/forms/companies/comp2.png" alt="your alt text">
                      <div class="itemHover">
                        <div class="zoombox"><a href="" title="images/portfolio/portfolio-img2" class="item-zoom fancybox-effects-a"><i class="fa fa-search" aria-hidden="true"></i></a> <a href="" rel="nofollow" class="item-zoom" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i></a> </div>
                        <div class="infoItem">
                          <div class="itemtitle">
                            <h5>Title Here</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="col-md-6 col-sm-6">
                    <div class="imgbox"><img src="../webapp/pages/forms/companies/comp3.png" alt="your alt text">
                      <div class="itemHover">
                        <div class="zoombox"><a href="" title="images/portfolio/portfolio-img3" class="item-zoom fancybox-effects-a"><i class="fa fa-search" aria-hidden="true"></i></a> <a href="" rel="nofollow" class="item-zoom" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i></a> </div>
                        <div class="infoItem">
                          <div class="itemtitle">
                            <h5>Title Here</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="col-md-6 col-sm-6">
                    <div class="imgbox"><img src="../webapp/pages/forms/companies/comp4.png" alt="your alt text">
                      <div class="itemHover">
                        <div class="zoombox"><a href="" title="images/portfolio/portfolio-img4" class="item-zoom fancybox-effects-a"><i class="fa fa-search" aria-hidden="true"></i></a> <a href="" rel="nofollow" class="item-zoom" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i></a> </div>
                        <div class="infoItem">
                          <div class="itemtitle">
                            <h5>Title Here</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="col-md-6 col-sm-6">
                    <div class="imgbox"><img src="../webapp/pages/forms/companies/comp1.png" alt="your alt text">
                      <div class="itemHover">
                        <div class="zoombox"><a href="" title="images/portfolio/portfolio-img5" class="item-zoom fancybox-effects-a"><i class="fa fa-search" aria-hidden="true"></i></a> <a href="" rel="nofollow" class="item-zoom" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i></a> </div>
                        <div class="infoItem">
                          <div class="itemtitle">
                            <h5>Title Here</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="col-md-6 col-sm-6">
                    <div class="imgbox"><img src="../webapp/pages/forms/companies/comp2.png" alt="your alt text">
                      <div class="itemHover">
                        <div class="zoombox"><a href="" title="images/portfolio/portfolio-img6" class="item-zoom fancybox-effects-a"><i class="fa fa-search" aria-hidden="true"></i></a> <a href="" rel="nofollow" class="item-zoom" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i></a> </div>
                        <div class="infoItem">
                          <div class="itemtitle">
                            <h5>Title Here</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <!-- Candidate Detail start -->
            <div class="job-header">
              <div class="jobdetail">
                <h3>Candidate Detail</h3>
                <ul class="jbdetail">
                  <li class="row">
                    <div class="col-md-6 col-xs-6">Experience</div>
                    <div class="col-md-6 col-xs-6"><span>5 Years</span></div>
                  </li>
                  <li class="row">
                    <div class="col-md-6 col-xs-6">Age</div>
                    <div class="col-md-6 col-xs-6"><span>28 Years</span></div>
                  </li>
                  <li class="row">
                    <div class="col-md-6 col-xs-6">Current Salary($)</div>
                    <div class="col-md-6 col-xs-6"><span class="permanent">10K - 12K</span></div>
                  </li>
                  <li class="row">
                    <div class="col-md-6 col-xs-6">Expected Salary($)</div>
                    <div class="col-md-6 col-xs-6"><span class="freelance">14K - 18K</span></div>
                  </li>
                  <li class="row">
                    <div class="col-md-6 col-xs-6">Education Levels</div>
                    <div class="col-md-6 col-xs-6"><span>Masters</span></div>
                  </li>
                </ul>
              </div>
            </div>

            <!-- Google Map start -->
            <div class="job-header">
              <div class="jobdetail">
                <h3>Skills</h3>
                <div class="skillswrap">
                  <!-- Skill -->
                  <h5>Photoshop</h5>
                  <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 90%"> <span>90%</span> </div>
                  </div>
                  <!-- Skill -->
                  <h5>HTML5</h5>
                  <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 85%"> <span>85%</span> </div>
                  </div>
                  <!-- Skill -->
                  <h5>Jquery</h5>
                  <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 90%"> <span>90%</span> </div>
                  </div>
                  <!-- Skill -->
                  <h5>Wordpress</h5>
                  <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 60%"> <span>60%</span> </div>
                  </div>
                  <!-- Skill -->
                  <h5>PHP</h5>
                  <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:35%"> <span>35%</span> </div>
                  </div>
                  <!-- Skill -->
                  <h5>Javascript</h5>
                  <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 95%"> <span>95%</span> </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Contact Company start -->
            <div class="job-header">
              <div class="jobdetail">
                <h3>Contact Elliot Scott</h3>
                <div class="formpanel">
                  <div class="formrow">
                    <input type="text" class="form-control" placeholder="Your Name">
                  </div>
                  <div class="formrow">
                    <input type="email" class="form-control" placeholder="Your Email">
                  </div>
                  <div class="formrow">
                    <input type="tel" class="form-control" placeholder="Phone">
                  </div>
                  <div class="formrow">
                    <input type="text" class="form-control" placeholder="Subject">
                  </div>
                  <div class="formrow">
                    <textarea class="form-control" placeholder="Message"></textarea>
                  </div>
                  <input type="submit" class="btn" value="Submit">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">

    <!-- <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved. -->
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<!-- Bootstrap's JavaScript -->
<script src="../resources/js/jquery-2.1.4.min.js"></script>
<script src="../resources/js/bootstrap.min.js"></script>
<!-- Custom js -->
<!-- <script src="resources/js/script.js"></script> -->
</body>
</html>
