<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Info Apprenticeship</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Bootstrap -->
	<link href="resources/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- Font Awesome -->
	<link href="resources/css/font-awesome.css" rel="stylesheet">
	
	<!-- Custom Style -->
	<link href="resources/css/main.css" rel="stylesheet">
  <style>
    .full-link {
      width: 97%;
    height: 94%;
    z-index: 1;
    position: absolute;
    }

    .card {
      width: 100%;
    }

    .sidebar {
      height: auto !important;
    }

    .pull-right {
      float: right;
    }

    .header {
      background-color: transparent;
    }
  </style>
</head>
<body>
	<!-- Content Wrapper. Contains page content -->
  


	<form class="" action="mail" method="post">
      <input type="hidden" name="idS" value="${ student.idStudent}">
      <input type="hidden" name="idA" value="${ apprenticeship.idApp}">
      <button type="submit" name="button" class="btn">SEND MAIL</button>
    </form>
    <div class="wrapper">
  <!-- Navbar -->

  <!-- Main Sidebar Container -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="margin: 0 !important;">
    <div class="header">
      <div class="container">
        <div class="row">
          <div class="col-md-2 col-sm-3 col-xs-12"> 
            <a href="#" class="logo"><img src="resources/images/Logo.png" alt="Logo" /></a>
              <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="col-md-10 col-sm-12 col-xs-12"> 
            <!-- Nav start -->
            <div class="navbar navbar-default" role="navigation">
              <div class="navbar-collapse collapse" id="nav-main">
                <ul class="nav navbar-nav">
                  <li><a href="companyHome?id=${ company.idCompany }">Home </a></li>
                  <li><a href="./company-profile.html">Profile</a></li>
                  <!-- <li class="postjob"><a href="./webapp/pages/forms/login.html">Login</a></li> -->
                  <!-- <li class="jobseeker"><a href="candidate-listing.html">Job Seeker</a></li> -->
                  <div class="social"> 
                      <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a> 
                       <a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a> 
                       <a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a> 
                    </div>
                </ul>
                <!-- Nav collapes end --> 
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- Nav end --> 
          </div>
        </div>
        <!-- row end --> 
      </div>
      <!-- Header container end --> 
    </div>
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <div class="container"> 
      <div class="row mb-2 margin-top30">
        <div class="col-sm-12">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Apprenticeship Description</li>
          </ol>
        </div>
      </div>
    <!-- Job Header start -->
    <div class="job-header">
      <div class="jobinfo">
        <div class="row">
          <div class="col-md-8">
            <h2>${ apprenticeship.title } - ${company.name}</h2>
            <div class="ptext">Date Posted: 2016/05/19</div>
            <div class="ptext">Industry: ${ apprenticeship.area }</div>
            <div class="ptext">Duration: ${ apprenticeship.title }</div>
                      </div>
          <div class="col-md-4">
            <div class="companyinfo">
              <div class="companylogo"><img src="resources/images/employers/emplogo11.png" alt="your alt text"></div>
              <div class="title"><a href="#.">Forum International</a></div>
              <div class="ptext">${ apprenticeship.company.addressCompany.country } , ${ apprenticeship.company.addressCompany.city }</div>
              <div class="loctext"><a class="btn btn-primary" href="deleteApprenticeship?id=${ apprenticeship.idApp }">DELETE APPRENTICESHIP</a></div>
              
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
      <!--div class="jobButtons"> 
        <!--a href="#." class="btn apply"><i class="fa fa-paper-plane" aria-hidden="true"></i> Apply Now</a--> 
        <!--a href="#." class="btn"><i class="fa fa-envelope" aria-hidden="true"></i> Email to Friend</a--> 
        <!--a href="./job-edit-post.html" class="btn btn-primary pull-right">Edit Apprenticeship</a--> 
        <!-- <a href="#." class="btn report"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Report Abuse</a--> 
      </div-->
    </div>
    
    <!-- Job Detail start -->
    <div class="row">
      <div class="col-md-8"> 
        <!-- Job Description start -->
        <div class="job-header">
          <div class="contentbox">
            <h3>Apprenticeship Description</h3>
            <p>${ apprenticeship.shortDesc }</p>
            <br>
           <h3>Knowledge</h3>
           <p>${ apprenticeship.knwoledge }</p>
           <h3>Skills</h3>
           <p>${ apprenticeship.skills }</p>
           <h3>Behaviours</h3>
            <p>${ apprenticeship.behaviour }</p>
          </div>
        </div>
        <!--
        <div class="job-header">
          <div class="contentbox">
            <h3>Companies offering this standard</h3>
            <ul class="experienceList">
              <li>
                <div class="row">
                  <div class="col-md-2"><img src="companies/comp1.png" alt="your alt text"></div>
                  <div class="col-md-10">
                    <h4>Company Name</h4>
                    <div class="row">
                      <div class="col-md-6">www.companywebsite.com</div>
                      <div class="col-md-6">From 2014 - Present</div>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque massa vel lorem fermentum fringilla. Pellentesque id est et neque blandit ornare</p>
                  </div>
                </div>
              </li>
              <li>
                <div class="row">
                  <div class="col-md-2"><img src="companies/comp2.png" alt="your alt text"></div>
                  <div class="col-md-10">
                    <h4>Company Name</h4>
                    <div class="row">
                      <div class="col-md-6">www.companywebsite.com</div>
                      <div class="col-md-6">From 2014 - Present</div>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque massa vel lorem fermentum fringilla. Pellentesque id est et neque blandit ornare</p>
                  </div>
                </div>
              </li>
              <li>
                <div class="row">
                  <div class="col-md-2"><img src="companies/comp3.png" alt="your alt text"></div>
                  <div class="col-md-10">
                    <h4>Company Name</h4>
                    <div class="row">
                      <div class="col-md-6">www.companywebsite.com</div>
                      <div class="col-md-6">From 2014 - Present</div>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque massa vel lorem fermentum fringilla. Pellentesque id est et neque blandit ornare</p>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
        -->
        <!--div class="job-header">
          <div class="contentbox">
            <h3>Schools that offer education related to this standard</h3>
            <ul class="educationList">
              <li>
                <div class="date">31<br>
                  May<br>
                  2012</div>
                <h4>Kiril Pejcinoviq </h4>
                <!-- <p>Educational Programe: General</p> >
                <p>4 years</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque massa vel lorem fermentum fringilla. Pellentesque id est et neque blandit ornare malesuada a mauris.</p>
                <div class="clearfix"></div>
              </li>
              <li>
                <div class="date">31<br>
                  May<br>
                  2015</div>
                <h4>South East European University </h4>
                <p>Educational Programe: Bachelor </p>
                <p>3 years</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque massa vel lorem fermentum fringilla. Pellentesque id est et neque blandit ornare malesuada a mauris.</p>
                <div class="clearfix"></div>
              </li>
              <li>
                <div class="date">31<br>
                  May<br>
                  2017</div>
                <h4>South East European University </h4>
                <p>Educational Programe: Master </p>
                <p>2 years</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque massa vel lorem fermentum fringilla. Pellentesque id est et neque blandit ornare malesuada a mauris.</p>
                <div class="clearfix"></div>
              </li>
            </ul>
          </div>
        </div-->
        <!-- Job Description end --> 
        
        <!-- related jobs start -->
        
      </div>
      <!-- related jobs end -->
      
      <div class="col-md-4"> 
        <!-- Job Detail start -->
        <div class="job-header">
          <div class="jobdetail">
            <h3>Apprenticeship Detail</h3>
            <ul class="jbdetail">
              <li class="row">
                <div class="col-md-6 col-xs-6">Job Id</div>
                <div class="col-md-6 col-xs-6"><span>${ apprenticeship.idApp }</span></div>
              </li>
              <li class="row">
                <div class="col-md-6 col-xs-6">location</div>
                <div class="col-md-6 col-xs-6"><span>${ apprenticeship.company.addressCompany.country } , ${ apprenticeship.company.addressCompany.city }</span></div>
              </li>
              <li class="row">
                <div class="col-md-6 col-xs-6">Company</div>
                <div class="col-md-6 col-xs-6"><a href="#.">${ apprenticeship.company.name }</a></div>
              </li>
              <li class="row">
                <div class="col-md-6 col-xs-6">Employment Status</div>
                <div class="col-md-6 col-xs-6">
                <c:if test="${ apprenticeship.empstatus =='Freelance'}">
                	<span class="freelance">Freelance</span>
				</c:if>
				<c:if test="${ apprenticeship.empstatus =='Permanent'}">
                	<span class="permanent">Permanent</span>  
				</c:if>
				<c:if test="${ apprenticeship.empstatus =='Contract'}">
                	<span class="contract">Contract</span> 
				</c:if> 
                </div>
              </li>
              <li class="row">
                <div class="col-md-6 col-xs-6">Employment Type</div>
                <div class="col-md-6 col-xs-6"><span>${ apprenticeship.emptype }</span></div>
              </li>
              <li class="row">
                <div class="col-md-6 col-xs-6">Positions</div>
                <div class="col-md-6 col-xs-6"><span>${ apprenticeship.position }</span></div>
              </li>
              <li class="row">
                <div class="col-md-6 col-xs-6">Experience</div>
                <div class="col-md-6 col-xs-6"><span>${ apprenticeship.experience }</span></div>
              </li>
              <li class="row">
                <div class="col-md-6 col-xs-6">Gender</div>
                <div class="col-md-6 col-xs-6"><span>${ apprenticeship.gender }</span></div>
              </li>
              <li class="row">
                <div class="col-md-6 col-xs-6">Degree</div>
                <div class="col-md-6 col-xs-6"><span>Masters</span></div>
              </li>
              <li class="row">
                <div class="col-md-6 col-xs-6">Title of qualification </div>
                <div class="col-md-6 col-xs-6"><span>????</span></div>
              </li>
            </ul>
          </div>
        </div>
        
        <!-- Contact Company start -->
      
        
        <!-- Google Map start -->
        <div class="job-header">
          <div class="jobdetail">
            <h3>Google Map</h3>
            <div class="gmap">
              <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d13276.272465119835!2d72.97962462269287!3d33.70718629417979!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1480401216309" allowfullscreen=""></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>

  <footer class="main-footer">
    <!-- <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div> -->
    <!-- <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved. -->
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- Bootstrap's JavaScript --> 
<script src="resources/js/jquery-2.1.4.min.js"></script> 
<script src="resources/js/bootstrap.min.js"></script> 

<!-- Custom js --> 
<!-- <script src="resources/js/script.js"></script> -->
</body>
</html>
    

	<!-- Bootstrap's JavaScript --> 
	<script src="assets/js/jquery-2.1.4.min.js"></script> 
	<script src="assets/js/bootstrap.min.js"></script> 
</body>
</html>