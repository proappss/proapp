<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Homepage - PROPAPPS</title>
<!-- Fav Icon -->
<!-- <link rel="shortcut icon" href="/resources/images/favicon.ico"> -->

<!-- Slider -->
<link href="resources/js/revolution-slider/css/settings.css" rel="stylesheet">

<!-- Owl carousel -->
<link href="resources/css/owl.carousel.css" rel="stylesheet">

<!-- Bootstrap -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<!-- Font Awesome -->
<link href="resources/css/font-awesome.css" rel="stylesheet">

<!-- Custom Style -->
<link href="resources/css/main.css" rel="stylesheet">
<style>
.modal {
  display: none;
    position: fixed;
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    background: rgb(0,0,0);
    background: rgba(0,0,0,.8);
    overflow: auto;line-height: 1.4em;
    font-family: "Roboto", sans-serif;
    font-weight: normal;
}
/* Aditional Styles */

/* Modal Content */
.modal-content {
  line-height: 1.4em;
  background: #ECEFF1;
  position: relative;
    width: 80%;
    max-width: 920px;
    margin: 100px auto;
    animation-name: modalbox;
    animation-duration: .4s;
    animation-timing-function: cubic-bezier(0,0,.3,1.6);
    font-family: "Roboto", sans-serif;
    font-weight: normal;
}

/* Add Animation */
@-webkit-keyframes animatetop {
  from {top:-300px; opacity:0}
  to {top:0; opacity:1}
}

@keyframes animatetop {
  from {top:-300px; opacity:0}
  to {top:0; opacity:1}
}

/* The Close Button */
.close {
  color: white;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #0000cc;
  text-decoration: none;
  cursor: pointer;
}

.modal-header {
  padding: 20px 40px;
    background: #546E7A;
    color: #ffffff;
      font-family: "Roboto", sans-serif;
}
#titre{font-family: "Roboto", sans-serif;}
.modal-body {padding: 2px 16px;}

.modal-footer {
  padding: 2px 16px;
  background-color: #0000cc;
  color: white;
}
  .click-block {
    position: absolute;
    z-index: 10;
    top:0px;
    left:0px;
    width:100%;
    height: 100%;
  }
</style>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="js/html5shiv.min.js"></script>
  <script src="js/respond.min.js"></script>
<![endif]-->
</head>
<body>
<script>
alert("Script");
console.log((${test}));
console.log((${test}));
if((${test} = true)){
		alert("GENIAL");
	}
	</script>
<div id="myModal" class="modal"onload="callFunction();">

  <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header">
      <img src="resources/images/Logo.png"
     alt="The head and torso of a dinosaur skeleton;
          it has a large head with long sharp teeth"
     width="40"
     height="40">
      <span class="close">&times;</span>
      <h2 id="titre">Notification</h2>
    </div>
    <div class="modal-body">
      <p>Congratulation !</p>
      <pre>Your account have been succefully created, you can now connect            - The Proapps Team</pre>
    </div>

  </div>

</div>
<script th:inline="javascript">
/*<![CDATA[*/
alert("ok");
    const test = ${test} 'default';
    alert(test);
/*]]>*/
</script>
<script>




// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
document.onreadystatechange =  function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>
<!-- Header start -->
<div class="header">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-3 col-xs-12"> <a href="indexLogout" class="logo"><img src="resources/images/Logo.png" alt="Logo" /></a>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="col-md-10 col-sm-12 col-xs-12"> 
        <!-- Nav start -->
        <div class="navbar navbar-default" role="navigation">
          <div class="navbar-collapse collapse" id="nav-main">
            <ul class="nav navbar-nav">
              <li><a href="indexLogout">Home </a></li>
              <li><a href="aboutLogout">About us</a></li>
              <li><a href="partnersLogout">Partners</a></li>
              <li><a href="contactUsLogout">Contact</a></li>
              <li class="postjob"><a href="loginLogout">Login</a></li>
              <!-- <li class="jobseeker"><a href="candidate-listing.html">Job Seeker</a></li> -->
              <div class="social"> 
                  <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a> 
                   <a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a> 
                   <a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a> 
                </div>
            </ul>
            <!-- Nav collapes end --> 
          </div>
          <div class="clearfix"></div>
        </div>
        <!-- Nav end --> 
      </div>
    </div>
    <!-- row end --> 
  </div>
  <!-- Header container end --> 
</div>
<!-- Header end --> 

<!-- Revolution slider start -->
<div class="tp-banner-container">
  <div class="tp-banner" >
    <ul>
      <!--Slide-->
      <li data-slotamount="7" data-transition="3dcurtain-vertical" data-masterspeed="1000" data-saveperformance="on"> <img alt="Your alt text" src="resources/images/slider/dummy.png" data-lazyload="resources/images/slider/proappsBanner.jpeg">
        <div class="caption lfl large-title tp-resizeme slidertext1" data-x="left" data-y="100" data-speed="600" data-start="1600">Slider 1 - News 1<br />
          In your Area</div>
        <div class="caption lfb large-title tp-resizeme sliderpara" data-x="left" data-y="200" data-speed="600" data-start="2800">Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br />
          Lorem Ipsum has been the industry's standard dummy text ever since.</div>
        <div class="caption lfl large-title tp-resizeme slidertext5" data-x="left" data-y="280" data-speed="600" data-start="3500"><a href="#.">Contact Us</a></div>
      </li>
      <!--Slide end--> 
      <!--Slide-->
      <li data-slotamount="7" data-transition="3dcurtain-vertical" data-masterspeed="1000" data-saveperformance="on"> <img alt="Your alt text" src="resources/images/slider/dummy.png" data-lazyload="resources/images/slider/proappsBanner2.jpeg">
        <div class="caption lfl large-title tp-resizeme slidertext1" data-x="left" data-y="100" data-speed="600" data-start="1600">Slider 2 - News 2<br />
          Around The World</div>
        <div class="caption lfb large-title tp-resizeme sliderpara" data-x="left" data-y="200" data-speed="600" data-start="2800">Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br />
          Lorem Ipsum has been the industry's standard dummy text ever since.</div>
        <div class="caption lfl large-title tp-resizeme slidertext5" data-x="left" data-y="280" data-speed="600" data-start="3500"><a href="#.">Contact Us</a></div>
      </li>
      <!--Slide end-->
       <!--Slide-->
       <li data-slotamount="7" data-transition="3dcurtain-vertical" data-masterspeed="1000" data-saveperformance="on"> <img alt="Your alt text" src="resources/images/slider/dummy.png" data-lazyload="resources/images/slider/proappsBanner3.jpeg">
        <div class="caption lfl large-title tp-resizeme slidertext1" data-x="left" data-y="100" data-speed="600" data-start="1600">Slider 3 - News 3<br />
          Around The World</div>
        <div class="caption lfb large-title tp-resizeme sliderpara" data-x="left" data-y="200" data-speed="600" data-start="2800">Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br />
          Lorem Ipsum has been the industry's standard dummy text ever since.</div>
        <div class="caption lfl large-title tp-resizeme slidertext5" data-x="left" data-y="280" data-speed="600" data-start="3500"><a href="#.">Contact Us</a></div>
      </li>
      <!--Slide end-->
      
    </ul>
  </div>
</div>
<!-- Revolution slider end --> 
<div class="textrow" style="padding-bottom: 100px;">
    <div class="container">
        <div class="row">
        <div class="col-md-5">
          <div class="postimg"><img src="resources/images/erasmusplus-logo-all-en-300dpi.jpg" alt="your alt text"></div>
        </div>
        <div class="col-md-7">
          <h2>What is ProAPPs platform</h2>
          <p style="text-align: justify;">The ProAPPs platform aims to shape and support the educational institutions and companies in direction of preparing
            the next skilling and re-skilling programmes of the future industry leaders. It provides opportunities to link
            business and education in a network wherein connected educational institutions and businesses can work together
            towards developing leaders that can make a difference in our societies.<br>
            <br>
            ProAPPs will foster new bonds and stronger connections among industry players and educational institutions, VET
            schools and universities. Such connections entail diverse set activities such as new curriculums, innovations in
            education and training, apprenticeships prorgammes and, overall, magnify the positive impact of the future employees
            and society as a whole.<br>
            <br>
            The mission of ProAPPs is to foster networking and engagement of companies and educational institutions in order to
            foster higher uptake of VET and apprenticeships. <br>
            </p>
            <br>
            <br>
        </div>
        <div class="col-lg-12 mb-2 section greybg" style="padding: 50px 35px;margin:50px 0px;">
          <h2>ProAPPS Synopsis</h2>
          The ProAPPs platform offer space for collaboration, exchange of information as well as new on apprenticehips offered by companies.<br>
          <br>
          <p style="text-align: justify;">A space for your company to get involved in strengthening the corporate
          training and development programs, current and future, by providing
          apprenticeships and apply the state of play and best practices for VET education
          and training.</p>
          <br>
          <p>Long lasting partnerships between VET schools and companies – connections
          wherein companies and educational institutions address the needs of the
          educational community and the industry, while focused on future skilled
          employees.</p>
          <br>
          <p style="text-align: justify;">A network of peers devoted to cultivating and advancing VET education and
          apprenticeships. The network will enhance the visibility of the apprentices among
          their peer but more importantly among educators and professionals.</p>
          <br>
          <p style="text-align: justify;">Apprenticeships capitalize on the collaboration of educators and businesses as
          they define skills and needs of the industry by ensuring apprentices are well
          prepared to the up-to-date industry skills needs.</p>
          <br>
        </div>
      </div>
      <div class="row">
        <div class="four-plan">
          <div class="container">
            <div class="titleTop">
              <h3>Choose Your Field of Interest <span></span></h3>
            </div>
            <div class="col-lg-12">
              <div class="col-md-4 col-sm-6 col-xs-12">
                <a class="click-block" href="listCompany"></a>
                <ul class="boxes">
                  <li class="icon">
                    <div class="iconcircle"><i class="fa fa-user" aria-hidden="true"></i></div>
                  </li>
                  <li class="plan-name">COMPANIES @ProAPPs</li>
                  <li style="margin-top:30px;">
                    <p> Companies can gain and share insights that will strengthen their involvement in education and training
                      as well provide news pertaining to their industries. Their cutting edge training programs and other
                      opportunities such as apprenticeships can be placed on the ProAPPs platform as they see fit.</p>
                  </li>
                </ul>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12">
                <a class="click-block" href="comingSoon"></a>
                <ul class="boxes">
                  <li class="icon"><i class="fa fa-paper-plane" aria-hidden="true"></i></li>
                  <li class="plan-name">SCHOOLS @ProAPPs</li>
                  <li style="margin-top:30px;">
                    <p>VET schools offer practical learning programmes wchih relate to specific industry sector and particular
                      jobs. A wide range of qualifications, and at various levels, can be gained by the students. Moreover,
                      the courses in VET schools are designed to help students and apprentices to get the skills needed to
                      start a job more easily, make their transition from school to work more effective and moreover enable
                      them to continue their education at higher level. To see what kind of qualifications can lead you to
                      your dream career or future academic endeavors visit the school section.</p>
                  </li>
                </ul>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12">
                <a class="click-block" href="comingSoon"></a>
                <ul class="boxes">
                  <li class="icon"><i class="fa fa-paper-plane" aria-hidden="true"></i></li>
                  <li class="plan-name">APPRENTICES @ProAPPs</li>
                  <li style="margin-top:30px;">
                    <p> If you are looking for an apprenticeship leading you to your future job, academic career or simply
                      building up knowledge and skills visit the apprenticeship section. Note that apprentices are focused on
                      their workplace while gaining job-specific skills and as addition they are supported by educational
                      institutions – VET schools.</p>
                    <p> Depending on the qualification you want to gain, the path and length is different. However, it starts
                      with building your apprenticeship CV.</p>
                  </li>
                </ul>
              </div>
      
            </div>
          </div>
        </div>
      </div>
      <!-- Top Employers start -->
<div class="row">
<div class="section greybg">
  <div class="container"> 
    <!-- title start -->
      <h3><span>Companies</span></h3>
    <!-- title end -->
    
    <ul class="employerList">
      <!--employer-->
      <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Company Name"><a href="comingSoon"><img src="resources/images/employers/emplogo1.jpg" alt="Company Name" /></a></li>
      <!--employer-->
      <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Company Name"><a href="comingSoon"><img src="resources/images/employers/emplogo2.jpg" alt="Company Name" /></a></li>
      <!--employer-->
      <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Company Name"><a href="comingSoon"><img src="resources/images/employers/emplogo3.jpg" alt="Company Name" /></a></li>
      <!--employer-->
      <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Company Name"><a href="comingSoon"><img src="resources/images/employers/emplogo4.jpg" alt="Company Name" /></a></li>
      <!--employer-->
      <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Company Name"><a href="comingSoon"><img src="resources/images/employers/emplogo4.jpg" alt="Company Name" /></a></li>
      <!--employer-->
      <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Company Name"><a href="comingSoon"><img src="resources/images/employers/emplogo5.jpg" alt="Company Name" /></a></li>
      <!--employer-->
      <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Company Name"><a href="comingSoon"><img src="resources/images/employers/emplogo6.jpg" alt="Company Name" /></a></li>
      <!--employer-->
      <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Company Name"><a href="comingSoon"><img src="resources/images/employers/emplogo7.jpg" alt="Company Name" /></a></li>
      <!--employer-->
      <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Company Name"><a href="comingSoon"><img src="resources/images/employers/emplogo8.jpg" alt="Company Name" /></a></li>
      <!--employer-->
      <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Company Name"><a href="comingSoon"><img src="resources/images/employers/emplogo9.jpg" alt="Company Name" /></a></li>
      <!--employer-->
      <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Company Name"><a href="comingSoon"><img src="resources/images/employers/emplogo10.jpg" alt="Company Name" /></a></li>
      <!--employer-->
      <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Company Name"><a href="comingSoon"><img src="resources/images/employers/emplogo11.jpg" alt="Company Name" /></a></li>
      <!--employer-->
      <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Company Name"><a href="comingSoon"><img src="resources/images/employers/emplogo12.jpg" alt="Company Name" /></a></li>
      <!--employer-->
      <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Company Name"><a href="comingSoon"><img src="resources/images/employers/emplogo13.jpg" alt="Company Name" /></a></li>
      <!--employer-->
      <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Company Name"><a href="comingSoon"><img src="resources/images/employers/emplogo14.jpg" alt="Company Name" /></a></li>
      <!--employer-->
      <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Company Name"><a href="comingSoon"><img src="resources/images/employers/emplogo15.jpg" alt="Company Name" /></a></li>
      
    </ul>
  </div>
</div>
</div>
<!-- Top Employers ends --> 
    </div>
  </div>






<!--Footer-->
<div class="footerWrap">
  <div class="container">
    <div class="row"> 
      <!--About Us-->
      <div class="col-md-3 col-sm-12">
        <div class="ft-logo"><img src="resources/images/Logo.png" alt="Logo"></div>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et consequat elit. Proin molestie eros sed urna auctor lobortis. </p>
        
        <!-- Social Icons -->
        <div class="social"> <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a></div>
        <!-- Social Icons end --> 
      </div>
      <!--About us End--> 
      
      <!--Quick Links-->
      <div class="col-md-2 col-sm-6">
        <h5>Quick Links</h5>
        <!--Quick Links menu Start-->
        <ul class="quicklinks">
          <li><a href="#.">Career Services</a></li>
          <li><a href="#.">CV Writing</a></li>
          <li><a href="#.">Career Resources</a></li>
          <li><a href="#.">Company Listings</a></li>
          <li><a href="#.">Success Stories</a></li>
        </ul>
      </div>
      <!--Quick Links menu end--> 
      
      <!--Jobs By Industry-->
      <div class="col-md-3 col-sm-6">
        <h5>Jobs By Industry</h5>
        <!--Industry menu Start-->
        <ul class="quicklinks">
          <li><a href="#.">Information Technology Jobs</a></li>
          <li><a href="#.">Recruitment/Employment Firms Jobs</a></li>
          <li><a href="#.">Education/Training Jobs</a></li>
          <li><a href="#.">Manufacturing Jobs</a></li>
          <li><a href="#.">Call Center Jobs</a></li>
        </ul>
        <!--Industry menu End-->
        <div class="clear"></div>
      </div>
      
      <!--Latest Articles-->
      <div class="col-md-4 col-sm-12">
        <div class="ft-logo" style="width:80%;"><img src="resources/images/erasmusplus-logo-all-en-300dpi.jpg" /></div>
      </div>
    </div>
  </div>
</div>
<!--Footer end--> 


<!--Copyright-->
<div class="copyright">
  <div class="container">
    <div class="bttxt">Copyright &copy; 2020. All Rights Reserved. Design by: PROAPPS</a></div>
  </div>
</div>

<!-- Bootstrap's JavaScript --> 
<script src="resources/js/jquery-2.1.4.min.js"></script> 
<script src="resources/js/bootstrap.min.js"></script> 

<!-- Revolution Slider --> 
<script type="text/javascript" src="resources/js/revolution-slider/js/jquery.themepunch.tools.min.js"></script> 
<script type="text/javascript" src="resources/js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script> 
<!-- map with geo locations --> 

<!-- Owl carousel --> 
<script src="resources/js/owl.carousel.js"></script> 

<!-- Custom js --> 
<script src="resources/js/script.js"></script>
</body>
</html>
