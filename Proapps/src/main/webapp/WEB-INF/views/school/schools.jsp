<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>School Profile - PROPAPPS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link href="../resources/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="../resources/css/font-awesome.css" rel="stylesheet">
<!-- Custom Style -->
<link href="../resources/css/main.css" rel="stylesheet">

  <style>
    .full-link {
      width: 97%;
    height: 94%;
    z-index: 1;
    position: absolute;
    }

    ul>li {
      list-style-type: none;
    }

    .details {
      padding: 0px;
    }

    .responsibilities {
      margin-top: 50px;
      border-top: 2px solid black;
      padding-top: 100px;
    }

    .job-header {
    background: #fff;
    border: 1px solid #e4e4e4;
    margin-bottom: 30px;
}

.job-header .jobinfo {
    padding: 25px;
    border-bottom: 1px solid #e4e4e4;
}

.content-wrapper {
  margin: 0 !important;
}

.cadsocial a {
    color: #666;
    font-size: 24px;
    display: inline-block;
    margin-right: 7px;
}
.userPic {
    border: none !important;
}

.clickable {
  position: absolute;
  width: 100%;
  height: 100%;
  z-index: 1;
  top: 0;
  left: 0;
}

.job-header .contentbox .userPortfolio li {
  height: 200px;
}
  </style>
</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->

  <!-- Main Sidebar Container -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="margin: 0 !important;">
    <!-- Header start -->
<div class="header">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-3 col-xs-12">
        <a href="index.html" class="logo" style="font-size: 22px;">
          <!-- <img src="resources/images/logo.png" alt="" /> -->
            TEMPORARY LOGO
          </a>        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="col-md-10 col-sm-12 col-xs-12"> 
        <!-- Nav start -->
        <div class="navbar navbar-default" role="navigation">
          <div class="navbar-collapse collapse" id="nav-main">
            <ul class="nav navbar-nav">
              <li><a href="../index.html">Home </a></li>
              <li><a href="../about.html">About us</a></li>
              <li><a href="../partners.html">Partners</a></li>
              <li><a href="../contact-us.html">Contact</a></li>
              <li class="postjob"><a href="../webapp/pages/forms/login.html">Login</a></li>
              <div class="social"> 
                  <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a> 
                   <a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a> 
                   <a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a> 
                </div>
            </ul>
            <!-- Nav collapes end --> 
          </div>
          <div class="clearfix"></div>
        </div>
        <!-- Nav end --> 
      </div>
    </div>
    <!-- row end --> 
  </div>
  <!-- Header container end --> 
</div>
<!-- Header end --> 

    <!-- Main content -->
    <div class="container" style="margin-top:20px;"> 
        <!-- Job Header start -->
        <div class="job-header">
          <div class="contentbox">
            <h3>Section for General information about the vocational schools </h3>
            <p> Vocational education and training (VET) provides young people with the foundation for moving from schools to employment just as well as moving from compulsory education (high school) to further academic development (university).<br><br>
			Additionally, it is the place for social cohesion, developing young people into active citizens, gain employability skills and entrepreneurship spirit.<br><br>
			In VET schools young people gain education and training as well as re-skilling. Moreover, re-skilling, upgrading skills and gaining new skills are crucial not only for young people but for adults as well, employed and unemployed.<br><br>
			VET schools co-operate with civil society organization and companies and build strong links with various industries. As such, the VET schools provide a fully developed and up to date VET provisions to young people. Based on this, VET students can are optimal replacement and /or complement the work based leaning (relevant practical as well as soft skill).<br><br>
			The path to your dream career starts in a VET school. See the opportunities bellow.<br><br></p>
          </div>
        </div>
      
        <!-- Job Detail start -->
        <div class="row">
          <div class="col-md-12 col-sm-12" style="margin: 0 auto;"> 
            <!-- Search List -->
            <ul class="searchList">
              <!-- job start -->
              <li>
                <div class="row">
                  <div class="col-md-8 col-sm-8">
                    <a class="clickable" href="./job-description.html"></a>
                    <div class="jobimg"><img src="../webapp/pages/forms/companies/comp1.png" alt="Job Name"></div>
                    <div class="jobinfo">
                      <h3><a href="./job-description.html">School 1</a></h3>
                      <div class="companyName"><a href="#.">Datebase Management Company</a></div>
                      <div class="location"><label class="fulltime">Full Time</label>   - <span>New York</span></div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="col-md-4 col-sm-4">
                    <div class="listbtn"><a href="school.html">View School</a></div>
                  </div>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis.</p>
              </li>
              <!-- job end --> 
              
              <!-- job start -->
              <li>
                <div class="row">
                  <div class="col-md-8 col-sm-8">
                    <div class="jobimg"><img src="../webapp/pages/forms/companies/comp2.png" alt="Job Name"></div>
                    <div class="jobinfo">
                      <h3><a href="./job-description.html">School 2</a></h3>
                      <div class="companyName"><a href="#.">Datebase Management Company</a></div>
                      <div class="location"><label class="freelance">Free Lancer</label>   - <span>New York</span></div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="col-md-4 col-sm-4">
                    <div class="listbtn"><a href="school.html">View School</a></div>
                  </div>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis.</p>
              </li>
              <!-- job end --> 
              
              <!-- job start -->
              <li>
                <div class="row">
                  <div class="col-md-8 col-sm-8">
                    <div class="jobimg"><img src="../webapp/pages/forms/companies/comp3.png" alt="Job Name"></div>
                    <div class="jobinfo">
                      <h3><a href="./job-description.html">School 3</a></h3>
                      <div class="companyName"><a href="#.">Datebase Management Company</a></div>
                      <div class="location"><label class="partTime">Part Time</label>   - <span>New York</span></div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="col-md-4 col-sm-4">
                    <div class="listbtn"><a href="school.html">View School</a></div>
                  </div>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis.</p>
              </li>
              <!-- job end --> 
              
              <!-- job start -->
              <li>
                <div class="row">
                  <div class="col-md-8 col-sm-8">
                    <div class="jobimg"><img src="../webapp/pages/forms/companies/comp4.png" alt="Job Name"></div>
                    <div class="jobinfo">
                      <h3><a href="./job-description.html">School 4</a></h3>
                      <div class="companyName"><a href="#.">Datebase Management Company</a></div>
                      <div class="location"><label class="fulltime">Full Time</label>   - <span>New York</span></div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="col-md-4 col-sm-4">
                    <div class="listbtn"><a href="school.html">View School</a></div>
                  </div>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis.</p>
              </li>
              <!-- job end --> 
              
              <!-- job start -->
              <li>
                <div class="row">
                  <div class="col-md-8 col-sm-8">
                    <div class="jobimg"><img src="../webapp/pages/forms/companies/comp2.png" alt="Job Name"></div>
                    <div class="jobinfo">
                      <h3><a href="./job-description.html">School 5</a></h3>
                      <div class="companyName"><a href="#.">Datebase Management Company</a></div>
                      <div class="location"><label class="freelance">Free Lancer</label>   - <span>New York</span></div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="col-md-4 col-sm-4">
                    <div class="listbtn"><a href="school.html">View School</a></div>
                  </div>
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis.</p>
              </li>
              <!-- job end -->               
            </ul>
          </div>
         
        </div>
      </div>
  </div>

  <footer class="main-footer">
    <!-- <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div> -->
    <!-- <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved. -->
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- Bootstrap's JavaScript --> 
<script src="../resources/js/jquery-2.1.4.min.js"></script> 
<script src="../resources/js/bootstrap.min.js"></script> 

<!-- Custom js --> 
<!-- <script src="resources/js/script.js"></script> -->
</body>
</html>
