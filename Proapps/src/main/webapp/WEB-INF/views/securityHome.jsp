<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Security</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap -->
  <link href="resources/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="resources/css/font-awesome.css" rel="stylesheet">
  <!-- Custom Style -->
  <link href="resources/css/main.css" rel="stylesheet">
    <style>
      .wrapper {
        position: relative;
        height: 100vh;
      }

      .content-wrapper.login-page{
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
      }
    </style>
  </head>
  <body>


    <div class="container">

      <div class="row">

        <div class="col-md-6 col-md-offset-3">
          <div class="title">
            <img src="resources/images/Logo.png" alt="" />
            <h2>WELCOME TO PROAPPS PROD</h2>
          </div>
          <div class="userccount">
            <!-- login form -->
            <div class="formpanel">
            	<form action="signSecurityHome" method="post">
	              <div class="formrow">
	                <input type="password" class="form-control" placeholder="Password" name="pwd">
	              </div>
	              <button type="submit" name="button" class="btn">enter</button>
              </form>

            </div>

            <!-- sign up form end-->

          </div>
        </div>

      </div>
    </div>

  <!-- Bootstrap's JavaScript -->
  <script src="assets/js/jquery-2.1.4.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>

  <!-- Custom js -->
  <script src="assets/js/script.js"></script>


  </body>
</html>
