<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login - PROPAPPS</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Bootstrap -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="resources/css/font-awesome.css" rel="stylesheet">
<!-- Custom Style -->
<link href="resources/css/main.css" rel="stylesheet">

<style>
  .wrapper {
    position: relative;
    height: 100vh;
  }

  .content-wrapper.login-page{
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
</style>

</head>
<body class="hold-transition sidebar-mini sidebar-collapse">

		<!-- Header start -->
	<div class="header">
	  <div class="container">
	    <div class="row">
	      <div class="col-md-2 col-sm-3 col-xs-12"> <a href="indexLogout" class="logo"><img src="resources/images/Logo.png" alt="Logo" /></a>
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
	        </div>
	        <div class="clearfix"></div>
	      </div>
	      <div class="col-md-10 col-sm-12 col-xs-12"> 
	        <!-- Nav start -->
	        <div class="navbar navbar-default" role="navigation">
	          <div class="navbar-collapse collapse" id="nav-main">
	            <ul class="nav navbar-nav">
	              <li><a href="indexLogout">Home </a></li>
	              <li><a href="aboutLogout">About us</a></li>
	              <li><a href="partnersLogout">Partners</a></li>
	              <li><a href="contactUsLogout">Contact</a></li>
	              <li class="postjob"><a href="loginLogout">Login</a></li>
	              <!-- <li class="jobseeker"><a href="candidate-listing.html">Job Seeker</a></li> -->
	              <div class="social"> 
	                  <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a> 
	                   <a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a> 
	                   <a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a> 
	                </div>
	            </ul>
	            <!-- Nav collapes end --> 
	          </div>
	          <div class="clearfix"></div>
	        </div>
	        <!-- Nav end --> 
	      </div>
	    </div>
	    <!-- row end --> 
	  </div>
	  <!-- Header container end --> 
	</div>
	
	
	  <!-- Process -->
  <div class="what_we_do">
    <div class="container">
      <div class="main-heading">Choose your login type</div>
      <ul class="row whatList">
        <li class="col-md-4 col-sm-6">
        	<a href="logAsStudent">
		          <div class="iconWrap">
		            <div class="icon"><i class="fa fa-user" aria-hidden="true"></i></div>
		          </div>
		          <h3>Login as Student</h3>
			</a>
        </li>
          
        <li class="col-md-4 col-sm-6">
        	<a href="logAsSchool">
		          <div class="iconWrap">
		            <div class="icon"><i class="fa fa-file-text"></i></div>
		          </div>
		          <h3>Login as School</h3>
			</a>
        </li>
        <li class="col-md-4 col-sm-6">
			<a href="logAsComp">        
	          <div class="iconWrap">
	            <div class="icon"><i class="fa fa-briefcase" aria-hidden="true"></i></div>
	          </div>
	          <h3>Login as Company</h3>
          	</a>
          
        </li>
      </ul>
    </div>
  </div>
<!-- Bootstrap's JavaScript --> 
<script src="resources/js/jquery-2.1.4.min.js"></script> 
<script src="resources/js/bootstrap.min.js"></script> 

<!-- Custom js --> 
<script src="resources/js/script.js"></script>
</body>
</html>
