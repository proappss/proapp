<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Homepage - PROPAPPS</title>
<!-- Fav Icon -->
<!-- <link rel="shortcut icon" href="/resources/images/favicon.ico"> -->

<!-- Slider -->
<link href="resources/js/revolution-slider/css/settings.css" rel="stylesheet">

<!-- Owl carousel -->
<link href="resources/css/owl.carousel.css" rel="stylesheet">

<!-- Bootstrap -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<!-- Font Awesome -->
<link href="resources/css/font-awesome.css" rel="stylesheet">

<!-- Custom Style -->
<link href="resources/css/main.css" rel="stylesheet">
<style>
  .click-block {
    position: absolute;
    z-index: 10;
    top:0px;
    left:0px;
    width:100%;
    height: 100%;
  }
</style>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="js/html5shiv.min.js"></script>
  <script src="js/respond.min.js"></script>
<![endif]-->
<style>
  .wrapper {
    position: relative;
    height: 100vh;
  }

  .content-wrapper.login-page{
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
</style>
</head>
<body>
<!-- Header start -->
<div class="header">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-3 col-xs-12"> <a href="indexLogout" class="logo"><img src="resources/images/Logo.png" alt="Logo" /></a>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="col-md-10 col-sm-12 col-xs-12"> 
        <!-- Nav start -->
        <div class="navbar navbar-default" role="navigation">
          <div class="navbar-collapse collapse" id="nav-main">
            <ul class="nav navbar-nav">
              <li><a href="indexLogout">Home </a></li>
              <li><a href="aboutLogout">About us</a></li>
              <li><a href="partnersLogout">Partners</a></li>
              <li><a href="contactUsLogout">Contact</a></li>
              <li class="postjob"><a href="loginLogout">Login</a></li>
              <!-- <li class="jobseeker"><a href="candidate-listing.html">Job Seeker</a></li> -->
              <div class="social"> 
                  <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a> 
                   <a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a> 
                   <a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a> 
                </div>
            </ul>
            <!-- Nav collapes end --> 
          </div>
          <div class="clearfix"></div>
        </div>
        <!-- Nav end --> 
      </div>
    </div>
    <!-- row end --> 
  </div>
  <!-- Header container end --> 
</div>

	<div class="content-wrapper login-page">
    <!-- Content Header (Page header) -->

		<form action="signInStudent" method="post">
		
		    <!-- Main content -->
		    <div class="container"> 
		      <div class="row">
		        <div class="col-md-6 col-md-offset-3">
		          <div class="userccount">
		            <div class="socialLogin">
		              <h5>Login Or Register with Social</h5>
		              <a href="#." class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a> 
		              <a href="#." class="gp"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
		              </div>
		            <h5 class="text-center">Student Login</h5>
		            <!-- login form -->
		            <div class="formpanel">
		              <div class="formrow">
		                <input type="text" name = "login" class="form-control" placeholder="Login">
		              </div>
		              <div class="formrow">
		                <input type="password" name = "pwd" class="form-control" placeholder="Password">
		              </div>
		              
		             
		              <input type="submit" style="margin-bottom:10px;" class="btn" value="Login as Student">
		            
		             
		            </div>
		            <!-- login form  end--> 
		            <!-- sign up form -->
            			<div class="newuser"><i class="fa fa-user" aria-hidden="true"></i> New Student? <a href="registerLogoutStudent">Sign Up Here</a></div>
            		<!-- sign up form end--> 
		          </div>
		        </div>
		       
		      </div>
		    </div>
	    
	    </form>
	</div>


</body>
</html>