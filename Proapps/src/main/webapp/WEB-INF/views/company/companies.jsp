<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Company - PROAPPS</title>
<!-- Fav Icon -->
<link rel="shortcut icon" href="/resources/images/favicon.ico">

<!-- Owl carousel -->
<link href="resources/css/owl.carousel.css" rel="stylesheet">

<!-- Bootstrap -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<!-- Font Awesome -->
<link href="resources/css/font-awesome.css" rel="stylesheet">

<!-- Custom Style -->
<link href="resources/css/main.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="js/html5shiv.min.js"></script>
  <script src="js/respond.min.js"></script>
<![endif]-->

</head>
<body>
<!-- Header start -->
<div class="header">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-3 col-xs-12"> 
      	<a href="indexLogout" class="logo"><img src="resources/images/Logo.png" alt="Logo" /></a>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="col-md-10 col-sm-12 col-xs-12">
        <div class="navbar navbar-default" role="navigation">
          <div class="navbar-collapse collapse" id="nav-main">
            <ul class="nav navbar-nav">
              <li><a href="companyHome?id=${ company.idCompany }">Home </a></li>
              <li><a href="aboutLogout">About us</a></li>
              <li><a href="partnersLogout">Partners</a></li>
              <li><a href="contactUsLogout">Contact</a></li>
              <li class="postjob"><a href="loginLogout">Login</a></li>
              <div class="social"> 
                <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a> 
                 <a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a> 
                 <a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a> 
              </div>
              </ul>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Header end --> 

<div class="about-wraper"> 
  <!-- About -->
  <div class="container">
    <div class="row" style="margin:30px 0px;">
        <div class="col-md-5">
            <div class="postimg"><img src="resources/images/about-us-img1.jpg" alt="your alt text" /></div>
          </div>
      <div class="col-md-7">
        <h2>About Companies</h2>
        <p>The apprenticeships can provide a unique opportunity for the company to extend their talent pool. The company, unlike other types of learning and training, provides a real life work experience under supervision and guidance of trained workers. The apprenticeships are structured learning programme based on practical learning driven by trained provider – a company.<br>
          <br>
          Companies’ gains<br>
          <br>
          Companies know best what it needs and the apprenticeship standards are guided to that end.<br></p>

		  <div class="col-md-12 col-sm-12"> 
		  <ul>
			  <li>* The apprenticeships enable high retention rates of employees for the companies through building loyalty at early stage of apprentice engagement. </li>
			  <li>* The companies gain a diverse pool of new employees. The apprenticeships offer specific skills for the new staff and provide quick solution to current workforce skills gaps. </li>
			  <li>* Higher productivity for companies. At early stage the company provides a structured approach to training’s goals and apprentices are motivated to achieve them.</li>
			  <li>* Continuing relations with educational institutions. The cooperation with educational institutions is crucial for companies to reduce cost of trainings.</li>
			</ul>
		  </div><br>
		   
		  <p><br>Companies provide:<br></p>
		  <div class="col-md-12 col-sm-12"> 
		  <ul>
			  <li>* Management of apprenticeships, set up the programme for each standard </li>
			  <li>* Diversified work and training under guidance of skilled staff. </li>
			  <li>* Maintain records of each apprentice related to learning and work conducted.</li>
			  <li>* Provide a recommendation for the apprentice for gaining a certificate upon completion of program.</li>
			  <li>* Provide support for the apprentice by issuing credentials and increasing employment opportunities in the specific trades based on apprentice needs.</li>
			</ul>
		  </div>
		</div>	
   
      
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12"> 
        <!-- Search List -->
        <ul class="searchList">
         
          <c:forEach items="${ listCompany }" var="Company">
          <!-- job start -->
          <li>
            <div class="row">
              <div class="col-md-8 col-sm-8">
                <div class="jobimg"><img src="resources/images/employers/emplogo3.jpg" alt="Job Name"></div>
                <div class="jobinfo">
                  <h3><a href="comingSoon">${ Company.type }</a></h3>
                  <div class="companyName"><a href="#.">${ Company.name }</a></div>
                  
                      <div class="location"><span>${ Company.addressCompany.city }</span></div>
                  
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="col-md-4 col-sm-4">
                <div class="listbtn"><a href="loginLogout">View Company</a></div>
              </div>
            </div>
            <p>${ Company.companyDesc }</p>
          </li>
          <!-- job end --> 
          </c:forEach>
                      
        </ul>
        
      
      </div>
    </div>
  </div>

</div>

<!--Footer-->
<div class="footerWrap">
  <div class="container">
    <div class="row"> 
      <!--About Us-->
      <div class="col-md-3 col-sm-12">
        <div class="ft-logo"><img src="resources/images/Logo.png" alt="Logo"></div>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et consequat elit. Proin molestie eros sed urna auctor lobortis. </p>
        
        <!-- Social Icons -->
        <div class="social"> <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a></div>
        <!-- Social Icons end --> 
      </div>
      <!--About us End--> 
      
      <!--Quick Links-->
      <div class="col-md-2 col-sm-6">
        <h5>Quick Links</h5>
        <!--Quick Links menu Start-->
        <ul class="quicklinks">
          <li><a href="#.">Career Services</a></li>
          <li><a href="#.">CV Writing</a></li>
          <li><a href="#.">Career Resources</a></li>
          <li><a href="#.">Company Listings</a></li>
          <li><a href="#.">Success Stories</a></li>
        </ul>
      </div>
      <!--Quick Links menu end--> 
      
      <!--Jobs By Industry-->
      <div class="col-md-3 col-sm-6">
        <h5>Jobs By Industry</h5>
        <!--Industry menu Start-->
        <ul class="quicklinks">
          <li><a href="#.">Information Technology Jobs</a></li>
          <li><a href="#.">Recruitment/Employment Firms Jobs</a></li>
          <li><a href="#.">Education/Training Jobs</a></li>
          <li><a href="#.">Manufacturing Jobs</a></li>
          <li><a href="#.">Call Center Jobs</a></li>
        </ul>
        <!--Industry menu End-->
        <div class="clear"></div>
      </div>
      
      <!--Latest Articles-->
      <div class="col-md-4 col-sm-12">
        <div class="ft-logo" style="width:80%;"><img src="resources/images/erasmusplus-logo-all-en-300dpi.jpg" /></div>
      </div>
    </div>
  </div>
</div>
<!--Footer end--> 

<!--Copyright-->
<div class="copyright">
    <div class="container">
      <div class="bttxt">Copyright &copy; 2020. All Rights Reserved. Design by: PROAPPS</a></div>
    </div>
  </div>

<!-- Bootstrap's JavaScript --> 
<script src="../resources/js/jquery-2.1.4.min.js"></script> 
<script src="../resources/js/bootstrap.min.js"></script> 

<!-- Owl carousel --> 
<script src="../resources/js/owl.carousel.js"></script> 

<!-- Custom js --> 
<script src="../resources/js/script.js"></script>
</body>
</html>