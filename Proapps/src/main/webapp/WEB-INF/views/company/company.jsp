<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Company - PROAPPS</title>
<!-- Fav Icon -->
<link rel="shortcut icon" href="/resources/images/favicon.ico">

<!-- Owl carousel -->
<link href="../resources/css/owl.carousel.css" rel="stylesheet">

<!-- Bootstrap -->
<link href="../resources/css/bootstrap.min.css" rel="stylesheet">

<!-- Font Awesome -->
<link href="../resources/css/font-awesome.css" rel="stylesheet">

<!-- Custom Style -->
<link href="../resources/css/main.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="js/html5shiv.min.js"></script>
  <script src="js/respond.min.js"></script>
<![endif]-->

</head>
<body>
<!-- Header start -->
<div class="header">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-3 col-xs-12"> <a href="index.html" class="logo"><img src="resources/images/Logo.png" alt="Logo" /></a>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="col-md-10 col-sm-12 col-xs-12">
        <div class="navbar navbar-default" role="navigation">
          <div class="navbar-collapse collapse" id="nav-main">
            <ul class="nav navbar-nav">
              <li><a href="companyHome?id=${ company.idCompany }">Home </a></li>
              <li><a href="../about.html">About us</a></li>
              <li><a href="../partners.html">Partners</a></li>
              <li><a href="../contact-us.html">Contact</a></li>
              <li class="postjob"><a href="../webapp/pages/forms/login.html">Login</a></li>
              <div class="social">
                <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                 <a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                 <a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
              </div>
              </ul>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Header end -->

<div class="about-wraper">
  <!-- About -->
  <div class="container">
    <div class="job-header">
      <div class="jobinfo">
        <div class="row">
          <div class="col-md-8 col-sm-8">
            <!-- Candidate Info -->
            <div class="candidateinfo">
              <div class="userPic"><img src="../webapp/pages/forms/companies/comp1.png" alt=""></div>
              <div class="title">Company Name</div>
              <div class="desi">Web Development Company</div>
              <div class="loctext"><i class="fa fa-history" aria-hidden="true"></i> Member Since, Aug 14, 2016</div>
              <div class="loctext"><i class="fa fa-map-marker" aria-hidden="true"></i> 123 Boscobel Street, NW8 8PS</div>
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="col-md-4 col-sm-4">
            <!-- Candidate Contact -->
            <div class="candidateinfo">
              <div class="loctext"><i class="fa fa-phone" aria-hidden="true"></i> (+1) 123.456.7890</div>
              <div class="loctext"><i class="fa fa-envelope" aria-hidden="true"></i> info@company.com</div>
              <div class="loctext"><i class="fa fa-globe" aria-hidden="true"></i> www.company.com</div>
              <div class="cadsocial">
                  <a href="http://www.twitter.com" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                  <a href="http://www.facebook.com" target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                  <a href="https://www.linkedin.com" target="_blank"><i class="fa fa-instagram-square"></i></a> </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Buttons -->
      <div class="jobButtons">
          <!-- <a href="#." class="btn apply"><i class="fa fa-paper-plane" aria-hidden="true"></i> Join Us</a>  -->
          <a href="#." class="btn"><i class="fa fa-envelope" aria-hidden="true"></i> Send Message</a>
          <!-- <a href="#." class="btn"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save This Company</a> -->
    </div>
    </div>

    <!-- Job Detail start -->
    <div class="row">
      <div class="col-md-8">
        <!-- About Employee start -->
        <div class="job-header">
          <div class="contentbox">
            <h3>About Company</h3>
            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pellentesque massa vel lorem fermentum fringilla. Pellentesque id est et neque blandit ornare malesuada a mauris. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sagittis, quam a fringilla congue, turpis turpis molestie ligula, ut laoreet elit arcu sed nulla. Sed at quam commodo, egestas turpis eget, fringilla dui. Etiam sit amet nulla metus. Etiam iaculis lobortis ultricies. <strong>Maecenas maximus magna a metus consectetur, vel fermentum nisl ultrices</strong>. Quisque eget ante id dui posuere ullamcorper ut molestie eros. Aliquam ultrices lacinia risus, at lacinia ante vehicula id. Nulla in lectus dignissim, egestas purus sit amet, mattis libero. Maecenas ullamcorper rutrum porta. </p>
            <ul>
              <li>In non augue eget purus placerat aliquet sit amet lobortis lacus.</li>
              <li>Curabitur interdum nisl quis placerat ornare.</li>
              <li>Curabitur ornare enim ac aliquam efficitur.</li>
              <li>Etiam volutpat leo et orci luctus, blandit accumsan arcu placerat.</li>
              <li>Proin egestas dui et pulvinar pellentesque.</li>
              <li>In ultricies nulla eget lacus tempor lobortis.</li>
            </ul>
          </div>
        </div>

        <!-- Opening Jobs start -->
        <div class="relatedJobs">
          <a href="./job-list.html" class="btn btn-default pull-right">View listings</a>
          <h3>Opening Apprenticeships</h3>
          <ul class="searchList">
            <!-- Job start -->
            <li>
            <div class="row">
              <a class="clickable" href="./job-description.html"></a>
              <div class="col-md-8 col-sm-8">
                <div class="jobimg"><img src="../webapp/pages/forms/companies/comp1.png" alt="Job Name"></div>
                <div class="jobinfo">
                  <h3><a href="#.">Technical Database Engineer</a></h3>
                  <div class="companyName"><a href="#.">Datebase Management Company</a></div>
                  <div class="location"><label class="fulltime">Full Time</label>   - <span>New York</span></div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="col-md-4 col-sm-4">
                <div class="listbtn"><a href="#.">Apply Now</a></div>
              </div>

            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis.</p>
          </li>
            <!-- Job end -->

            <!-- Job start -->
            <li>
            <div class="row">
              <a class="clickable" href="./job-description.html"></a>
              <div class="col-md-8 col-sm-8">
                <div class="jobimg"><img src="../webapp/pages/forms/companies/comp2.png" alt="Job Name"></div>
                <div class="jobinfo">
                  <h3><a href="#.">Technical Database Engineer</a></h3>
                  <div class="companyName"><a href="#.">Datebase Management Company</a></div>
                  <div class="location"><label class="partTime">Part Time</label>   - <span>New York</span></div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="col-md-4 col-sm-4">
                <div class="listbtn"><a href="#.">Apply Now</a></div>
              </div>
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis.</p>
          </li>
            <!-- Job end -->

            <!-- Job start -->
            <li>
            <div class="row">
              <a class="clickable" href="./job-description.html"></a>
              <div class="col-md-8 col-sm-8">
                <div class="jobimg"><img src="../webapp/pages/forms/companies/comp3.png" alt="Job Name"></div>
                <div class="jobinfo">
                  <h3><a href="#.">Technical Database Engineer</a></h3>
                  <div class="companyName"><a href="#.">Datebase Management Company</a></div>
                  <div class="location"><label class="freelance">Freelance</label>   - <span>New York</span></div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="col-md-4 col-sm-4">
                <div class="listbtn"><a href="#.">Apply Now</a></div>
              </div>
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis arcu est. Phasellus vel dignissim tellus. Aenean fermentum fermentum convallis.</p>
          </li>
            <!-- Job end -->
          </ul>
        </div>
      </div>
      <div class="col-md-4">
        <!-- Company Detail start -->
        <div class="job-header">
          <div class="jobdetail">
            <h3>Company Detail</h3>
            <ul class="jbdetail">
              <li class="row">
                <div class="col-md-6 col-xs-6">Total Employees</div>
                <div class="col-md-6 col-xs-6"><span>10-50</span></div>
              </li>
              <li class="row">
                <div class="col-md-6 col-xs-6">Established In</div>
                <div class="col-md-6 col-xs-6"><span>2008</span></div>
              </li>
              <li class="row">
                <div class="col-md-6 col-xs-6">Current jobs</div>
                <div class="col-md-6 col-xs-6"><span>10</span></div>
              </li>
            </ul>
          </div>
        </div>

        <!-- Google Map start -->
        <div class="job-header">
          <div class="jobdetail">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d193572.19492844533!2d-74.11808565615137!3d40.70556503857166!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1481975053066" allowfullscreen=""></iframe>
          </div>
        </div>

        <!-- Contact Company start -->
        <div class="job-header">
          <div class="jobdetail">
            <h3>Contact Company Name</h3>
            <div class="formpanel">
              <div class="formrow">
                <input type="text" class="form-control" placeholder="Your Name">
              </div>
              <div class="formrow">
                <input type="email" class="form-control" placeholder="Your Email">
              </div>
              <div class="formrow">
                <input type="tel" class="form-control" placeholder="Phone">
              </div>
              <div class="formrow">
                <input type="text" class="form-control" placeholder="Subject">
              </div>
              <div class="formrow">
                <textarea class="form-control" placeholder="Message"></textarea>
              </div>
              <input type="submit" class="btn" value="Submit">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

<!--Footer-->
<div class="footerWrap">
    <div class="container">
      <div class="row">
        <!--About Us-->
        <div class="col-md-3 col-sm-12">
          <div class="ft-logo"><img src="../resources/images/logo.png" alt="Logo"></div>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et consequat elit. Proin molestie eros sed urna auctor lobortis. </p>

          <!-- Social Icons -->
          <div class="social"> <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a> <a href="#." target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a> <a href="#." target="_blank"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a> <a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a> <a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a> </div>
          <!-- Social Icons end -->
        </div>
        <!--About us End-->

        <!--Quick Links-->
        <div class="col-md-2 col-sm-6">
          <h5>Quick Links</h5>
          <!--Quick Links menu Start-->
          <ul class="quicklinks">
            <li><a href="#.">Career Services</a></li>
            <li><a href="#.">CV Writing</a></li>
            <li><a href="#.">Career Resources</a></li>
            <li><a href="#.">Company Listings</a></li>
            <li><a href="#.">Success Stories</a></li>
          </ul>
        </div>
        <!--Quick Links menu end-->

        <!--Jobs By Industry-->
        <div class="col-md-3 col-sm-6">
          <h5>Jobs By Industry</h5>
          <!--Industry menu Start-->
          <ul class="quicklinks">
            <li><a href="#.">Information Technology Jobs</a></li>
            <li><a href="#.">Recruitment/Employment Firms Jobs</a></li>
            <li><a href="#.">Education/Training Jobs</a></li>
            <li><a href="#.">Manufacturing Jobs</a></li>
            <li><a href="#.">Call Center Jobs</a></li>
          </ul>
          <!--Industry menu End-->
          <div class="clear"></div>
        </div>

        <!--Latest Articles-->
        <div class="col-md-4 col-sm-12">
          <h5>Latest Articles</h5>
          <ul class="posts-list">
            <!--Article 1-->
            <li>
              <article class="post post-list">
                <div class="entry-content media">
                  <div class="media-left"> <a href="#." title="" class="entry-image"> <img width="80" height="80" src="../resources/images/news-1.jpg" alt="Your alt text here"> </a> </div>
                  <div class="media-body">
                    <h4 class="entry-title"> <a href="#.">Sed fermentum at lectus nec porta.</a> </h4>
                    <div class="entry-content-inner">
                      <div class="entry-meta"> <span class="entry-date">Jan 28, 2016</span> </div>
                    </div>
                  </div>
                </div>
              </article>
            </li>
            <!--Article end 1-->

            <!--Article 2-->
            <li>
              <article class="post post-list">
                <div class="entry-content media">
                  <div class="media-left"> <a href="#." title="" class="entry-image"> <img width="80" height="80" src="../resources/images/news-2.jpg" alt="Your alt text here"> </a> </div>
                  <div class="media-body">
                    <h4 class="entry-title"> <a href="#.">Sed fermentum at lectus nec porta.</a> </h4>
                    <div class="entry-content-inner">
                      <div class="entry-meta"> <span class="entry-date">Jan 28, 2016</span> </div>
                    </div>
                  </div>
                </div>
              </article>
            </li>
            <!--Article end 2-->
          </ul>
        </div>
      </div>
    </div>
  </div>
<!--Footer end-->

<!--Copyright-->
<div class="copyright">
    <div class="container">
      <div class="bttxt">Copyright &copy; 2020. All Rights Reserved. Design by: PROAPPS</a></div>
    </div>
  </div>

<!-- Bootstrap's JavaScript -->
<script src="../resources/js/jquery-2.1.4.min.js"></script>
<script src="../resources/js/bootstrap.min.js"></script>

<!-- Owl carousel -->
<script src="../resources/js/owl.carousel.js"></script>

<!-- Custom js -->
<script src="../resources/js/script.js"></script>
</body>
</html>
