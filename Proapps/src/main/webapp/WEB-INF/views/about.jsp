<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>About - PROAPPS</title>
<!-- Fav Icon -->
<link rel="shortcut icon" href="/resources/images/favicon.ico">

<!-- Owl carousel -->
<link href="resources/css/owl.carousel.css" rel="stylesheet">

<!-- Bootstrap -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<!-- Font Awesome -->
<link href="resources/css/font-awesome.css" rel="stylesheet">

<!-- Custom Style -->
<link href="resources/css/main.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="js/html5shiv.min.js"></script>
  <script src="js/respond.min.js"></script>
<![endif]-->

</head>
<body>
<!-- Header start -->
<div class="header">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-3 col-xs-12"> <a href="indexLogout" class="logo"><img src="resources/images/Logo.png" alt="Logo" /></a>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="col-md-10 col-sm-12 col-xs-12">
        <div class="navbar navbar-default" role="navigation">
          <div class="navbar-collapse collapse" id="nav-main">
            <ul class="nav navbar-nav">
              	<li><a href="indexLogout">Home </a></li>
				<li class="active"><a href="aboutLogout">About us</a></li>
				<li ><a href="partnersLogout">Partners</a></li>
				<li><a href="contactUsLogout">Contact</a></li>
				<li class="postjob"><a href="loginLogout">Login</a></li>
              <div class="social"> 
                <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a> 
                 <a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a> 
                 <a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a> 
              </div>
            </ul>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Header end --> 

<div class="about-wraper"> 
  <!-- About -->
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2>About PROAPPS</h2>
        <h3>The project:</h3>
        <h4 style="text-align: justify;">NEW APPRENTICESHIP STANDARDS: COMPANY DRIVEN APPRENTICESHIP STANDARDS is project co-financed by the Erasmus +
          cooperation for innovation and the exchange of good practices programme.
        </h4>
        <p>The overall objective is to facilitate the uptake of high quality jobs, apprenticeships and traineeships
          positions in SEE countries<br>
        </p>
      </div>
      <div class="col-md-6" style="text-align: center;">
        <div class="postimg"><img style="width:70%;" src="resources/images/about-us-img1.jpg" alt="your alt text" /></div>
      </div>
    </div>
  <div class="row">
      <div class="paragraph" style="margin-top:50px;">
        <h4>Specific Objective 1: </h4>
        <p style="text-align: justify;"> To facilitate exchanges of good practices, the development of networks and discussion
          platforms between education providers and the private sector to boost employability of learners.</p>
      </div>
      <div class="paragraph">
        <h4>Specific Objective 2: </h4>
        <p style="text-align: justify;">Support transnational cooperation and public-private partnerships to adapt the
          existing education provision in order to meet the emerging needs of employers and reflects new trends and
          developments in the labour market related to youth needs.</p>
      </div>
      <div class="paragraph">
        <h4>Specific Objective 3: </h4>
        <p style="text-align: justify;">Enable and encourage, through utilization of ICT tools and platforms, job seekers and
          businesses, particularly SMEs in order to find and offer apprenticeships and jobs in various industry.</p>
      </div>
      <div class="paragraph">
        <h4>Specific Objective 4:</h4>
        <p style="text-align: justify;">Enhance policy development related to apprenticeship through improving
          apprenticeships' standards and policies based on transnational cooperation and EU best practice.</p>
      </div>
      <div class="paragraph">
        <h4>Target group </h4>
        <p style="text-align: justify;">The project is foreseen to be implemented in close communication and coordination with
          involvement of various social partners and appropriate intermediary bodies such as chambers of commerce, association
          of employers, industry and crafts, professional organizations, sectoral organizations with established
          apprenticeship systems, public bodies (national employment agency, regional development centers etc.).</p> <br>
        <p style="text-align: justify;">However, as final beneficiaries and target groups the consortium sees the student
          support centers at universities, VET schools and the students (secondary and tertiary level) themselves. Common
          problems for hiring apprenticeships are the lack of training infrastructure and personnel to supervise apprentices,
          as well as insufficient expertise and capacity to manage complex rules, employment law and administrative
          requirements.</p>
      </div>
    </div>
      
      
    </div>
  </div>
  
  <!-- Process -->
  <div class="what_we_do">
    <div class="container">
      <div class="main-heading">OUTCOMES</div>
      <div class="whatText">Diam velit voluptatibus has te. Verear aliquid mentitum nam no</div>
      <ul class="row whatList">
        <li class="col-md-3 col-sm-6">
          <div class="iconWrap">
            <div class="icon"><i class="fa fa-user" aria-hidden="true"></i></div>
          </div>
          <h3>Create Account</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque porttitor congue enim non rhoncus. Sed ac lacus non elit malesuada blandit.</p>
        <a class="btn btn-primary" style="margin-top:20px;">Download report</a>
        </li>
        <li class="col-md-3 col-sm-6">
          <div class="iconWrap">
            <div class="icon"><i class="fa fa-file-text"></i></div>
          </div>
          <h3>Build CV</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque porttitor congue enim non rhoncus. Sed ac lacus non elit malesuada blandit.</p>
        <a class="btn btn-primary" style="margin-top:20px;">Download report</a>
        </li>
        <li class="col-md-3 col-sm-6">
          <div class="iconWrap">
            <div class="icon"><i class="fa fa-briefcase" aria-hidden="true"></i></div>
          </div>
          <h3>Get Job</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque porttitor congue enim non rhoncus. Sed ac lacus non elit malesuada blandit.</p>
        <a class="btn btn-primary" style="margin-top:20px;">Download report</a>
        </li>
        <li class="col-md-3 col-sm-6">
          <div class="iconWrap">
            <div class="icon"><i class="fa fa-briefcase" aria-hidden="true"></i></div>
          </div>
          <h3>Get Job</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque porttitor congue enim non rhoncus. Sed ac lacus non elit malesuada blandit.</p>
          <a class="btn btn-primary" style="margin-top:20px;">Download report</a>
        </li>
      </ul>
    </div>
  </div>
</div>

<!--Footer-->
<div class="footerWrap">
  <div class="container">
    <div class="row"> 
      <!--About Us-->
      <div class="col-md-3 col-sm-12">
        <div class="ft-logo"><img src="resources/images/Logo.png" alt="Logo"></div>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et consequat elit. Proin molestie eros sed urna auctor lobortis. </p>
        
        <!-- Social Icons -->
        <div class="social"> <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a></div>
        <!-- Social Icons end --> 
      </div>
      <!--About us End--> 
      
      <!--Quick Links-->
      <div class="col-md-2 col-sm-6">
        <h5>Quick Links</h5>
        <!--Quick Links menu Start-->
        <ul class="quicklinks">
          <li><a href="#.">Career Services</a></li>
          <li><a href="#.">CV Writing</a></li>
          <li><a href="#.">Career Resources</a></li>
          <li><a href="#.">Company Listings</a></li>
          <li><a href="#.">Success Stories</a></li>
        </ul>
      </div>
      <!--Quick Links menu end--> 
      
      <!--Jobs By Industry-->
      <div class="col-md-3 col-sm-6">
        <h5>Jobs By Industry</h5>
        <!--Industry menu Start-->
        <ul class="quicklinks">
          <li><a href="#.">Information Technology Jobs</a></li>
          <li><a href="#.">Recruitment/Employment Firms Jobs</a></li>
          <li><a href="#.">Education/Training Jobs</a></li>
          <li><a href="#.">Manufacturing Jobs</a></li>
          <li><a href="#.">Call Center Jobs</a></li>
        </ul>
        <!--Industry menu End-->
        <div class="clear"></div>
      </div>
      
      <!--Latest Articles-->
      <div class="col-md-4 col-sm-12">
        <div class="ft-logo" style="width:80%;"><img src="resources/images/erasmusplus-logo-all-en-300dpi.jpg" /></div>
      </div>
    </div>
  </div>
</div>
<!--Footer end--> 

<!--Copyright-->
<div class="copyright">
  <div class="container">
    <div class="bttxt">Copyright &copy; 2020. All Rights Reserved. Design by: PROAPPS</a></div>
  </div>
</div>

<!-- Bootstrap's JavaScript --> 
<script src="resources/js/jquery-2.1.4.min.js"></script> 
<script src="resources/js/bootstrap.min.js"></script> 

<!-- Owl carousel --> 
<script src="resources/js/owl.carousel.js"></script> 

<!-- Custom js --> 
<script src="resources/js/script.js"></script>
</body>
</html>