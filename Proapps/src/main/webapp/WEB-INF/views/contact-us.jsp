<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Contact - PROAPPS</title>
<!-- Fav Icon -->
<link rel="shortcut icon" href="resources/images/favicon.ico">

<!-- Owl carousel -->
<link href="resources/css/owl.carousel.css" rel="stylesheet">

<!-- Bootstrap -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

<!-- Font Awesome -->
<link href="resources/css/font-awesome.css" rel="stylesheet">

<!-- Custom Style -->
<link href="resources/css/main.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="js/html5shiv.min.js"></script>
  <script src="js/respond.min.js"></script>
<![endif]-->

</head>
<body>
<!-- Header start -->
<div class="header">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-3 col-xs-12"> <a href="indexLogout" class="logo"><img src="resources/images/Logo.png" alt="" /></a>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="col-md-10 col-sm-12 col-xs-12">
        <div class="navbar navbar-default" role="navigation">
          <div class="navbar-collapse collapse" id="nav-main">
            <ul class="nav navbar-nav">
                <li><a href="indexLogout">Home </a></li>
                <li><a href="aboutLogout">About us</a></li>
                <li><a href="partnersLogout">Partners</a></li>
                <li class="active"><a href="contactUsLogout">Contact</a></li>
                <li class="postjob"><a href="loginLogout">Login</a></li>
                <div class="social">
                  <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                   <a href="#." target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                   <a href="#." target="_blank"><i class="fa fa-youtube-square" aria-hidden="true"></i></a>
                </div>
              </ul>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Header end --> <!-- Page Title start -->

<!-- Contact us -->
<div class="inner-page">
  <div class="container">
    <div class="contact-wrap">
      <div class="row">
        <div class="col-md-12 column">
          <div class="title"> <span>We Are Here For Your Help</span>
            <h2>GET IN TOUCH FAST</h2>
            <p>Vestibulum at magna tellus. Vivamus sagittis nunc aliquet. Vivamin orci aliquam<br>
              eros vel saphicula. Donec eget ultricies ipsmconsequat.</p>
          </div>
        </div>

        <!-- Contact Info -->
        <div class="col-md-4 column">
          <div class="contact-now">
            <div class="contact"> <span><i class="fa fa-home"></i></span>
              <div class="information"> <strong>Address:</strong>
                <p>8500 lorem, New Ispum, Dolor amet sit 12301</p>
              </div>
            </div>
            <!-- Contact Info -->
            <div class="contact"> <span><i class="fa fa-envelope"></i></span>
              <div class="information"> <strong>Email Address:</strong>
                <p>investigate@your-site.com</p>
                <p>investigate@your-site.com</p>
              </div>
            </div>
            <!-- Contact Info -->
            <div class="contact"> <span><i class="fa fa-phone"></i></span>
              <div class="information"> <strong>Phone No:</strong>
                <p>+12 345 67 09</p>
                <p>+12 345 67 09</p>
              </div>
            </div>
            <!-- Contact Info -->
          </div>
          <!-- Contact Now -->
        </div>

        <!-- Contact form -->
        <div class="col-md-8 column">
          <div class="contact-form">
            <div id="message"></div>
              <div class="row">
                <div class="col-md-6">
                  <input name="name" type="text" id="name" placeholder="Full Name">
                </div>
                <div class="col-md-6">
                  <input type="tel" name="phone" placeholder="Phone Number">
                </div>
                <div class="col-md-12">
                  <input name="email" type="email" id="email" placeholder="Email">
                </div>
                <div class="col-md-12">
                  <textarea rows="4" name="comments" id="comments" placeholder="Details"></textarea>
                </div>
                <div class="col-md-12">
                  <button title="" class="button" type="button" id="submit">Submit Now</button>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Google Map -->
<!-- <div class="googlemap">
  <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d193572.19492844533!2d-74.11808565615137!3d40.70556503857166!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1481975053066" allowfullscreen></iframe>
</div> -->

<!--Footer-->
<div class="footerWrap">
  <div class="container">
    <div class="row">
      <!--About Us-->
      <div class="col-md-3 col-sm-12">
        <div class="ft-logo"><img src="resources/images/Logo.png" alt="Logo"></div>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et consequat elit. Proin molestie eros sed urna auctor lobortis. </p>

        <!-- Social Icons -->
        <div class="social"> <a href="#." target="_blank"> <i class="fa fa-facebook-square" aria-hidden="true"></i></a></div>
        <!-- Social Icons end -->
      </div>
      <!--About us End-->

      <!--Quick Links-->
      <div class="col-md-2 col-sm-6">
        <h5>Quick Links</h5>
        <!--Quick Links menu Start-->
        <ul class="quicklinks">
          <li><a href="#.">Career Services</a></li>
          <li><a href="#.">CV Writing</a></li>
          <li><a href="#.">Career Resources</a></li>
          <li><a href="#.">Company Listings</a></li>
          <li><a href="#.">Success Stories</a></li>
        </ul>
      </div>
      <!--Quick Links menu end-->

      <!--Jobs By Industry-->
      <div class="col-md-3 col-sm-6">
        <h5>Jobs By Industry</h5>
        <!--Industry menu Start-->
        <ul class="quicklinks">
          <li><a href="#.">Information Technology Jobs</a></li>
          <li><a href="#.">Recruitment/Employment Firms Jobs</a></li>
          <li><a href="#.">Education/Training Jobs</a></li>
          <li><a href="#.">Manufacturing Jobs</a></li>
          <li><a href="#.">Call Center Jobs</a></li>
        </ul>
        <!--Industry menu End-->
        <div class="clear"></div>
      </div>

      <!--Latest Articles-->
      <div class="col-md-4 col-sm-12">
        <div class="ft-logo" style="width:80%;"><img src="resources/images/erasmusplus-logo-all-en-300dpi.jpg" /></div>
      </div>
    </div>
  </div>
</div>
<!--Footer end-->

<!--Copyright-->
<div class="copyright">
    <div class="container">
      <div class="bttxt">Copyright &copy; 2020. All Rights Reserved. Design by: PROAPPS</a></div>
    </div>
  </div>

<!-- Bootstrap's JavaScript -->
<script src="resources/js/jquery-2.1.4.min.js"></script>
<script src="resources/js/bootstrap.min.js"></script>

<!-- Owl carousel -->
<script src="resources/js/owl.carousel.js"></script>

<!-- Custom js -->
<script src="resources/js/script.js"></script>
</body>
</html>
