package fr.eclee.metier;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import fr.eclee.beans.Company;
import fr.eclee.repositories.dao.CompanyRepository;


public class CompanyServices {
	
	@Autowired
	CompanyRepository cr;
	
	/**
	 * Return a single company depend on the idCompany in param
	 * @param idCompany
	 * @return
	 */
	public Company findbyID(int idCompany) {
		
		System.out.println(idCompany);
		
		System.out.println("bug2");
		Company comp = new Company();
		comp = cr.findById(idCompany).get();
		
		System.out.println("bug3");
		
		return comp;
	}
	

	/**
	 * Return all company in an  ArrayList<Company>
	 * @return
	 */
	public ArrayList<Company> listCompany(){
		
		ArrayList<Company> listComp = (ArrayList<Company>) cr.findAll();
		
		return listComp;
		
	}
	
}
