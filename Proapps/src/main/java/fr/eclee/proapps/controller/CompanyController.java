package fr.eclee.proapps.controller;

import java.time.LocalDate;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.eclee.beans.Account;
import fr.eclee.beans.Address;
import fr.eclee.beans.Apprenticeship;
import fr.eclee.beans.Company;
import fr.eclee.metier.CompanyServices;
import fr.eclee.repositories.dao.ApprenticeshipRepository;
import fr.eclee.repositories.dao.CompanyRepository;

@Controller

public class CompanyController {

	@Autowired
	CompanyRepository cr;
	@Autowired
	ApprenticeshipRepository ar;

	CompanyServices cs = new CompanyServices();
	
	/**
	 * function to create an company
	 * @param mv
	 * @param name
	 * @param regNo
	 * @param email
	 * @param tel
	 * @param website
	 * @param facebook
	 * @param linkedin
	 * @param companyDesc
	 * @param type
	 * @param totalEmployees
	 * @param yearEstablished
	 * @param nameStreet
	 * @param zipCode
	 * @param city
	 * @param country
	 * @param login
	 * @param pwd
	 * @return
	 */
	@PostMapping(value = "/signUp")
	public ModelAndView addCompany(ModelAndView mv, @RequestParam(value = "name") String name,
			@RequestParam(value = "regNo") String regNo, @RequestParam(value = "email") String email,
			@RequestParam(value = "tel") String tel, @RequestParam(value = "website") String website,
			@RequestParam(value = "facebook") String facebook, @RequestParam(value = "linkedin") String linkedin,
			@RequestParam(value = "companyDesc") String companyDesc, @RequestParam(value = "type") String type,
			@RequestParam(value = "totalEmployees") String totalEmployees,
			@RequestParam(value = "yearEstablished") String yearEstablished,
			@RequestParam(value = "nameStreet") String nameStreet, @RequestParam(value = "zipCode") String zipCode,
			@RequestParam(value = "city") String city, @RequestParam(value = "country") String country,
			@RequestParam(value = "login") String login, @RequestParam(value = "pwd") String pwd) {

		LocalDate dateJ = LocalDate.now();

		String pwdHash = BCrypt.hashpw(pwd, BCrypt.gensalt());

		Address address = new Address(nameStreet, zipCode, city, country);

		Account account = new Account(login, pwdHash);

		Company comp = new Company(name, regNo, email, tel, dateJ, website, facebook, linkedin, companyDesc, type,
				totalEmployees, yearEstablished);

		comp.setAddressCompany(address);
		comp.setAccount(account);

		cr.save(comp);

		cr.flush();

		mv.addObject("company", comp);

		mv.setViewName("notification/notification");

		return mv;
	}

	/**
	 * 
	 * @param mv
	 * @param idCompany
	 * @return
	 */
	@GetMapping(value = "/companyProfile")
	public ModelAndView companyProfile(ModelAndView mv, @RequestParam(value = "id") int idCompany) {

		Company comp = new Company();
		comp = cr.findById(idCompany).get();

		mv.addObject("company", comp);

		mv.setViewName("forms/company-profile-edit");

		return mv;
	}

	/**
	 * 
	 * @param mv
	 * @param idCompany
	 * @return
	 */
	@GetMapping(value = "/companyHome")
	public ModelAndView companyHome(ModelAndView mv, @RequestParam(value = "id") int idCompany) {
		
		

		ArrayList<Apprenticeship> listApprenticeship = (ArrayList<Apprenticeship>) ar.findAll();
		ArrayList<Apprenticeship> listApprenticeshipCompany = new ArrayList<Apprenticeship>();
		
		for (Apprenticeship apprenticeship : listApprenticeship) {
            if (apprenticeship.getCompany().getIdCompany() == idCompany) {

                listApprenticeshipCompany.add(apprenticeship);
            }

        }
		
		Company comp = new Company();
		comp = cr.findById(idCompany).get();
		
		mv.addObject("listeApprenticeship", listApprenticeshipCompany);
		mv.addObject("company", comp);

		mv.setViewName("forms/company-profile");

		return mv;
	}
	@GetMapping(value = "/companyAll")
	public ModelAndView companyAll(ModelAndView mv, @RequestParam(value = "id") int idCompany) {
		
		

		ArrayList<Apprenticeship> listApprenticeship = (ArrayList<Apprenticeship>) ar.findAll();
		ArrayList<Apprenticeship> listApprenticeshipCompany = new ArrayList<Apprenticeship>();
		
		for (Apprenticeship apprenticeship : listApprenticeship) {
            if (apprenticeship.getCompany().getIdCompany() == idCompany) {

                listApprenticeshipCompany.add(apprenticeship);
            }

        }
		
		Company comp = new Company();
		comp = cr.findById(idCompany).get();
		
		mv.addObject("listeApprenticeship", listApprenticeshipCompany);
		mv.addObject("company", comp);

		mv.setViewName("forms/companyAll");

		return mv;
	}
	@PostMapping(value = "/companyFilter" )
	public ModelAndView companyFilter(ModelAndView mv ,@RequestParam(value = "id") int idCompany,
			@RequestParam(value = "webdesigner", required = false)boolean webdesigner,
			@RequestParam(value = "3dgraphic",required = false)boolean dddgraphic,
			@RequestParam(value = "graphic",required = false)boolean graphic,
			@RequestParam( value = "electronicTech",required = false)boolean electronicTech,
			@RequestParam(value = "webgraphic",required = false)boolean webgraphic,
			@RequestParam(value = "brandmanager",required = false)boolean brandmanager,
			@RequestParam(value = "newyork",required = false)boolean newyork,
			@RequestParam(value =  "losangles",required = false)boolean losangles,
			@RequestParam(value = "chicago",required = false)boolean chicago,
			@RequestParam(value = "houston",required = false)boolean houston,
			@RequestParam(value = "sandiego",required = false)boolean sandiego,
			@RequestParam(value = "sanjose",required = false)boolean sanjose)  
	{ArrayList<Apprenticeship> listApprenticeship = (ArrayList<Apprenticeship>) ar.findAll();
	ArrayList<Apprenticeship> listApprenticeshipCompany = new ArrayList<Apprenticeship>();
	  if(newyork==true)
	  {System.out.println("GENIALE");
		  for (Apprenticeship apprenticeship : listApprenticeship) {
	            if (apprenticeship.getLocation().equals("New York")) {

	                listApprenticeshipCompany.add(apprenticeship);
	            }

	        }
	    
	  }  if(losangles==true)
	  {
	  for (Apprenticeship apprenticeship : listApprenticeship) {
            if (apprenticeship.getLocation().equals("Los Angeles")) {

                listApprenticeshipCompany.add(apprenticeship);
            }

        }
    
  }
	  if(chicago==true)
	  {
	  for (Apprenticeship apprenticeship : listApprenticeship) {
            if (apprenticeship.getLocation().equals("Chicago")) {

                listApprenticeshipCompany.add(apprenticeship);
            }

        }
    
  }
	  if(houston==true)
	  {System.out.println("GENIALE");
	  for (Apprenticeship apprenticeship : listApprenticeship) {
            if (apprenticeship.getLocation().equals("Houston")) {

                listApprenticeshipCompany.add(apprenticeship);
            }

        }
    
  }
	  if(sandiego==true)
	  {System.out.println("GENIALE");
	  for (Apprenticeship apprenticeship : listApprenticeship) {
            if (apprenticeship.getLocation().equals("San Diego")) {

                listApprenticeshipCompany.add(apprenticeship);
            }

        }
    
  }if(sanjose==true)
  {System.out.println("GENIALE");
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getLocation().equals("San Jose")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }
  }
  if(webdesigner==true)
  {
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getArea().equals("Web Designer")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }

}
  if(dddgraphic==true)
  {
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getArea().equals("3D Graphic Designer")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }

}
  if(graphic==true)
  {
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getArea().equals("Graphic Designer")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }

}
  if(electronicTech==true)
  {
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getArea().equals("Electronics Technician")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }

}
  if(webgraphic==true)
  {
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getArea().equals("Web / Graphic Designer")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }

}
  if(brandmanager==true)
  {
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getArea().equals("Brand Manager")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }

}


	  else
	  {
	    System.out.println("checkbox is not checked");
	  }
	  
	  Company comp = new Company();
		comp = cr.findById(idCompany).get();
		
		mv.addObject("listeApprenticeship", listApprenticeshipCompany);
		mv.addObject("company", comp);

		mv.setViewName("forms/companyAll");

		return mv;
	}

	/**
	 * 
	 * @param mv
	 * @param idCompany
	 * @return
	 */
	@GetMapping(value = "/suppressionCompany")
	 public ModelAndView deleteCompany(ModelAndView mv, @RequestParam(value = "id") int id) {
	        
		
		 		
		 		
		         cr.deleteById((id));
		         mv.setViewName("index");
		 		
		         return mv;
		     }
		 	
}
