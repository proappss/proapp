package fr.eclee.proapps.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;


import fr.eclee.beans.Apprenticeship;
import fr.eclee.beans.Company;
import fr.eclee.repositories.dao.ApprenticeshipRepository;
import fr.eclee.repositories.dao.CompanyRepository;

@Controller
@SessionAttributes("company")
public class AuthenticationCompany {

	@Autowired
	CompanyRepository cr;
	@Autowired
	ApprenticeshipRepository ar;
	
	/**
	 * Function to login
	 * @param model
	 * @param login
	 * @param pwd
	 * @return
	 */
	@PostMapping(value = "/signInCompany")
	public String singIn(Model model, @RequestParam(value = "login") String login,
			@RequestParam(value = "pwd") String pwd) {
		
		

		ArrayList<Company> listCompany = (ArrayList<Company>) cr.findAll();
		
		ArrayList<Apprenticeship> listApprenticeship = (ArrayList<Apprenticeship>) ar.findAll();
		
		ArrayList<Apprenticeship> listApprenticeshipCompany = new ArrayList<Apprenticeship>();
		
		
		
		

		if (listCompany != null) {
			for (Company company : listCompany) {
				if (company.getAccount().getLogin().equals(login) && BCrypt.checkpw(pwd, company.getAccount().getPwd())) {
					model.addAttribute("company", company);
					
					for (Apprenticeship apprenticeship : listApprenticeship) {
						if (apprenticeship.getCompany().getIdCompany()==company.getIdCompany()) {
							
							listApprenticeshipCompany.add(apprenticeship);
						}
					
					}
					model.addAttribute("listeApprenticeship", listApprenticeshipCompany);
					return "forms/company-profile";
				}
			}
		}
		System.out.println("not found");
		return "login/selectLog";
	}
	
	/**
	 * function to logout
	 * @param session
	 * @param status
	 * @return
	 */
	@GetMapping(value = "/signOutCompany")
	public String singOut(HttpSession session, SessionStatus status) {

		session.removeAttribute("company");
		status.setComplete();

		return "index";
	}
}
