package fr.eclee.proapps.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.eclee.beans.Apprenticeship;
import fr.eclee.beans.Company;
import fr.eclee.beans.School;
import fr.eclee.repositories.dao.ApprenticeshipRepository;
import fr.eclee.repositories.dao.CompanyRepository;
import fr.eclee.repositories.dao.SchoolRepository;

@Controller
public class RedirectController {
	
	@Autowired
	ApprenticeshipRepository ar;
	@Autowired
	CompanyRepository cr;
	@Autowired
	SchoolRepository sr;
	
	/**
	 * Partners redirection for an unconnected user
	 * @return
	 */
	@GetMapping(value = "/partnersLogout")
	public String redirectPartnersLogout() {
		
		return "partners";
	}
	
	/**
	 *  Home redirection for an unconnected user
	 * @return
	 */
	@GetMapping(value = "/indexLogout")
	public String redirectindexLogout() {
		
		return "index";
	}
	
	/**
	 * about redirection for an unconnected user
	 * @return
	 */
	@GetMapping(value = "/aboutLogout")
	public String redirectaboutLogout() {
		
		return "about";
	}
	
	/**
	 *  contact us redirection for an unconnected user
	 * @return
	 */
	@GetMapping(value = "/contactUsLogout")
	public String redirectcontactUsLogout() {
		
		return "contact-us";
	}
	
	/**
	 * Login redirection for an unconnected user
	 * @return
	 */
	@GetMapping(value = "/loginLogout")
	public String redirectloginLogout() {
		
		return "login/selectLog";
	}
	
	/**
	 * Register redirection for an unconnected user
	 * @return
	 */
	@GetMapping(value = "/registerLogout")
	public String redirectregisterLogout() {
		
		return "forms/register";
	}
	
	@GetMapping(value = "/registerLogoutSchool")
	public String redirectregisterLogoutSchool() {
		
		return "forms/registerSchool";
	}
	
	@GetMapping(value = "/registerLogoutStudent")
	public ModelAndView redirectregisterLogoutStudent(ModelAndView mv) {
		ArrayList<School> listSchool = (ArrayList<School>) sr.findAll();
		
		for(int i = 0 ; i < listSchool.size(); i++) {
			   System.out.println(listSchool.get(i).getName());}
		mv.addObject("listSchool", listSchool);
		
		  mv.setViewName("forms/registerStudent");
		return mv;
	}
	
	/**
	 * list company redirection
	 * @param mv
	 * @return
	 */
	@GetMapping(value = "/listCompany")
	public ModelAndView redirectListCompany(ModelAndView mv) {
		
		ArrayList<Company> listCompany = (ArrayList<Company>) cr.findAll();
		
		mv.addObject("listCompany", listCompany);
		mv.setViewName("company/companies");
		return mv;
	}
	/**
	 * COMING SOON REDIRECTION
	 * @return
	 */
	@GetMapping(value = "/comingSoon")
	public String redirectComingSoon() {
		
		return "comingSoon";
	}
	
	@GetMapping(value = "/logAsComp")
	public String redirectlogAsCompany() {
		
		return "login/loginCompany";
	}
	
	@GetMapping(value = "/logAsSchool")
	public String redirectlogAsSchool() {
		
		return "login/loginSchool";
	}
	
	@GetMapping(value = "/logAsStudent")
	public String redirectlogAsStudent() {
		
		return "login/loginStudent";
	}
	
	@PostMapping(value = "/signSecurityHome")
	public ModelAndView redirectSignSecurityHome(ModelAndView mv, @RequestParam(value = "pwd") String pwdSecure) {
		
		if (pwdSecure.equals("Ecleeprod!") || pwdSecure.equals("Ivanprod2021!")) {
			mv.setViewName("index");
		} else {
			mv.setViewName("securityHome");
		}
		
		return mv;
	}
}
