package fr.eclee.proapps.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import fr.eclee.beans.Apprenticeship;
import fr.eclee.beans.School;
import fr.eclee.beans.Student;
import fr.eclee.repositories.dao.ApprenticeshipRepository;
import fr.eclee.repositories.dao.SchoolRepository;
import fr.eclee.repositories.dao.StudentRepository;

@Controller
@SessionAttributes("student")
public class AuthentificationStudent {
	
	
	@Autowired
	SchoolRepository sr;
	@Autowired
	ApprenticeshipRepository ar;
	@Autowired
	StudentRepository st;
	

	@PostMapping(value = "/signInStudent")
	public String singInStudent(Model model, @RequestParam(value = "login") String login,
			@RequestParam(value = "pwd") String pwd) {
		
		StudentController sc = new StudentController();

		ArrayList<Student> listStudent = (ArrayList<Student>) st.findAll();
		
		ArrayList<Apprenticeship> listApprenticeship = (ArrayList<Apprenticeship>) ar.findAll();
		
		ArrayList<Apprenticeship> listApprenticeshipCompany = new ArrayList<Apprenticeship>();
		
		

		if (listStudent != null) {
			for (Student student : listStudent) {
				if (student.getAccount().getLogin().equals(login) && BCrypt.checkpw(pwd, student.getAccount().getPwd())) {
					model.addAttribute("student", student);
					System.out.println("ok");
					for (Apprenticeship apprenticeship : listApprenticeship) {
			          
			        	   	
			                listApprenticeshipCompany.add(apprenticeship);
			            

			        }
				
					model.addAttribute("listeApprenticeship", listApprenticeshipCompany);
					 sc.fieldsFilter(model);
					return "forms/student-profile";
				}
			}
		}
		return "login/selectLog";
	}
	@GetMapping(value = "/signOutStudent")
	public String singOut(HttpSession session, SessionStatus status) {

		session.removeAttribute("student");
		status.setComplete();

		return "index";
	}
}
