package fr.eclee.proapps.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import fr.eclee.beans.Apprenticeship;
import fr.eclee.beans.Company;
import fr.eclee.beans.Formation;
import fr.eclee.beans.School;
import fr.eclee.repositories.dao.FormationRepository;
import fr.eclee.repositories.dao.SchoolRepository;

@Controller
@SessionAttributes("school")
public class AuthentificationSchool {
	
	@Autowired
	SchoolRepository sr;
	@Autowired
	FormationRepository fr;
	
	/**
	 * 
	 * @param model
	 * @param login
	 * @param pwd
	 * @return
	 */
	@PostMapping(value = "/signInSchool")
	public String singIn(Model model, @RequestParam(value = "login") String login,
			@RequestParam(value = "pwd") String pwd) {
		
		

		ArrayList<School> listSchool = (ArrayList<School>) sr.findAll();
		
		
		
		
		
		

		if (listSchool != null) {
			for (School school : listSchool) {
				if (school.getAccount().getLogin().equals(login) && BCrypt.checkpw(pwd, school.getAccount().getPwd())) {
					model.addAttribute("school", school);
					
					ArrayList<Formation> listFormation= (ArrayList<Formation>) fr.findAll();
					ArrayList<Formation> listFormationSchool = new ArrayList<Formation>();
					
					for (Formation form : listFormation) {
			            if (form.getSchool().getIdSchool() == school.getIdSchool()) {
			            	

			            	listFormationSchool.add(form);
			            }

			        }
					
					model.addAttribute("listFormationSchool", listFormationSchool);
					
					
					return "forms/school-profile";
				}
			}
		}
		return "login/selectLog";
	}
	
	/**
	 * 
	 * @param session
	 * @param status
	 * @return
	 */
	@GetMapping(value = "/signOutSchool")
	public String singOut(HttpSession session, SessionStatus status) {

		session.removeAttribute("school");
		status.setComplete();

		return "index";
	}
	

}
