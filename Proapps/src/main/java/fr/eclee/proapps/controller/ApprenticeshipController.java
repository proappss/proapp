package fr.eclee.proapps.controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.eclee.beans.Apprenticeship;
import fr.eclee.beans.Company;
import fr.eclee.repositories.dao.ApprenticeshipRepository;
import fr.eclee.repositories.dao.CompanyRepository;


@Controller
public class ApprenticeshipController {
	
	@Autowired
	ApprenticeshipRepository ar;
	@Autowired
	CompanyRepository cr;
	

	@GetMapping(value = "/companyAddJob")
	public ModelAndView redirectForm(ModelAndView mv,@RequestParam(value = "id") int idCompany) {
		
		return fieldsApprencticeship(mv,idCompany);
	}
	
	@PostMapping(value = "/addApprenticeship")
	public ModelAndView addCompany(ModelAndView mv, @RequestParam(value = "id") Integer idC,@RequestParam(value = "occupation") String occupation,
			@RequestParam(value = "shortdesc") String shortdesc, @RequestParam(value = "entryreq") String entryreq,
			@RequestParam(value = "knowledge") String knowledge,
			@RequestParam(value = "skills") String skills, @RequestParam(value = "behaviours") String behaviours,
			@RequestParam(value = "duration") String duration,@RequestParam(value = "title") String title,@RequestParam(value = "apprenticeship") String app,@RequestParam(value = "progression") String progression,
			@RequestParam(value = "school") String school,@RequestParam(value = "edprog") String edprog,
			@RequestParam(value = "area") String area,@RequestParam(value = "msalary") String msalary,@RequestParam(value = "location") String location,
			@RequestParam(value = "empstatus") String empstatus,@RequestParam(value = "emptype") String emptype,@RequestParam(value = "positions") Integer positions,
			@RequestParam(value = "experience") String experience,@RequestParam(value = "gender") String gender,@RequestParam(value = "nationality") String nationality,
			@RequestParam(value = "jobdetail") String jobdetail
			) {
		//List<Apprenticeship> listeappskills = (List<Apprenticeship>) ar.findAll();
		Apprenticeship apprenticeship = new Apprenticeship(occupation,shortdesc,entryreq,knowledge,skills,behaviours,duration,title,app,progression,school,edprog,area,msalary,location,empstatus,emptype,positions,experience,gender,nationality,jobdetail);
		Company company = new Company();
		company = cr.findById(idC).get() ;
		apprenticeship.setCompany(company);	
		ar.saveAndFlush(apprenticeship);
		cr.saveAndFlush(company);
		
		ArrayList<Apprenticeship> listApprenticeship = (ArrayList<Apprenticeship>) ar.findAll();
		
		ArrayList<Apprenticeship> listApprenticeshipCompany = new ArrayList<Apprenticeship>();
		
		for (Apprenticeship apprenticeship2 : listApprenticeship) {
			if (apprenticeship2.getCompany().getIdCompany()==company.getIdCompany()) {
				
				listApprenticeshipCompany.add(apprenticeship2);
			}
		
		}
		mv.addObject("listeApprenticeship", listApprenticeshipCompany);

		mv.addObject("apprenticeship", apprenticeship);
		

		mv.addObject("company", company);
		
		
		
		//mv.addObject("listeAppskills",listeappskills);

		

		mv.setViewName("forms/company-profile");

		return mv;
	}
	public ModelAndView fieldsApprencticeship(ModelAndView mv, @RequestParam(value = "id") int idCompany) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader("C:\\env\\workspace\\proapp\\Proapps\\src\\main\\webapp\\resources\\csv\\tbsubindustry.csv"));
			System.out.println("fichier : "+br); 
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    String ligne = null;
	    String str = null;
	    ArrayList<String>  subindustry= new ArrayList <String>();
	    ArrayList<String> subindustry2= new ArrayList <String>();
	    try {
			while ((ligne = br.readLine()) != null)
			 {
			  
			  String[] data = ligne.split(",");
			
			  while ((ligne = br.readLine()) != null) {
				  
			      String[] temp = ligne.split(",");
			      String marque = temp[0];
			    
			      subindustry.add(marque);
				  for(int i = 0 ; i < subindustry.size(); i++) {
					  int coupure = 0;
					
					String str1 =  (String) subindustry.get(i);
					for(int y = 0 ; y <str1.length();y++) {
						
						char c = str1.charAt(y);
						
						 if((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')){
						     y =str1.length();
						     break;
						    }
					   else{
					      coupure++;
					    }
					}
					 str = str1.substring(coupure, str1.length()-1);
					
				 
				}
				  subindustry2.add(str);
			  }
			
			
  }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		BufferedReader br1 = null;
		try {
			br1 = new BufferedReader(new FileReader("C:\\env\\workspace\\proapp\\Proapps\\src\\main\\webapp\\resources\\csv\\tbindustry.csv"));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    String ligne1 = null;
	    String str2 = null;
	    ArrayList<String>  industry= new ArrayList <String>();
	    ArrayList<String> industry1= new ArrayList <String>();
	    try {
			while ((ligne1 = br1.readLine()) != null)
			 {
			  // Retourner la ligne dans un tableau
			  String[] data1 = ligne1.split(",");
			
			  // Afficher le contenu du tableau
   

			  while ((ligne1 = br1.readLine()) != null) {
				  
			      String[] temp = ligne1.split(",");
			      String marque = temp[0];
			    
			      industry.add(marque);
				  for(int i = 0 ; i < industry.size(); i++) {
					  int coupure = 0;
					
					String str3 =  (String) industry.get(i);
					for(int y = 0 ; y <str3.length();y++) {
						
						char c = str3.charAt(y);
						
						 if((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')){
						     y =str3.length();
						     break;
						    }
					   else{
					      coupure++;
					    }
					}
					 str2 = str3.substring(coupure, str3.length()-1);
					
				 
				}
				  industry1.add(str2);
			  }
			
			
  }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    BufferedReader br2 = null;
		try {
			br2 = new BufferedReader(new FileReader("C:\\env\\workspace\\proapp\\Proapps\\src\\main\\webapp\\resources\\csv\\tbapprenticeship.csv"));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    String ligne2 = null;
	    String str5 = null;
	    ArrayList<String>  apprenticeship= new ArrayList <String>();
	    ArrayList<String> apprenticeship1= new ArrayList <String>();
	    try {
			while ((ligne2 = br2.readLine()) != null)
			 {
			  // Retourner la ligne dans un tableau
			  String[] data2 = ligne2.split(",");
			
			  // Afficher le contenu du tableau
   

			  while ((ligne2 = br2.readLine()) != null) {
				  
			      String[] temp = ligne2.split(",");
			      String marque = temp[0];
			    
			      apprenticeship.add(marque);
				  for(int i = 0 ; i < apprenticeship.size(); i++) {
					  int coupure = 0;
					
					String str4 =  (String) apprenticeship.get(i);
					for(int y = 0 ; y <str4.length();y++) {
						
						char c = str4.charAt(y);
						
						 if((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')){
						     y =str4.length();
						     break;
						    }
					   else{
					      coupure++;
					    }
					}
					 str5 = str4.substring(coupure, str4.length()-1);
					
				 
				}
				  apprenticeship1.add(str5);
			  }
			
			
  }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    mv.addObject("industry", industry1);
	    mv.addObject("subindustry", subindustry2);
	    mv.addObject("apprenticeship", apprenticeship1);


		
	    Company comp = new Company();
		comp = cr.findById(idCompany).get();

		mv.addObject("company", comp);

		mv.setViewName("forms/job-edit-post");
		return mv;
	}
	
	@GetMapping(value = "/apprenticeshipDescription")
	public ModelAndView apprenticeshipDescription(ModelAndView mv, @RequestParam(value = "idCompany") int idCompany, @RequestParam(value = "idApprenticeship") int idApprenticeship) {
		
		

		Apprenticeship apprenticeship = new Apprenticeship();
		apprenticeship = ar.findById(idApprenticeship).get();
		
		Company comp = new Company();
		comp = cr.findById(idCompany).get();
		
		
		
		mv.addObject("apprenticeship", apprenticeship);
		mv.addObject("company", comp);

		mv.setViewName("forms/job-description");

		return mv;
	}
	
	@GetMapping(value = "/deleteApprenticeship")
    public ModelAndView deleteAprrenticeship(ModelAndView mv, @RequestParam(value = "id") int id) {
        
		
	        Apprenticeship apprenticeship = new Apprenticeship();
			apprenticeship = ar.findById(id).get();
			
			Company company = new Company();
			company = cr.findById(apprenticeship.getCompany().getIdCompany()).get();
        ar.deleteById((id));
        ArrayList<Apprenticeship> listApprenticeship = (ArrayList<Apprenticeship>) ar.findAll();
		ArrayList<Apprenticeship> listApprenticeshipCompany = new ArrayList<Apprenticeship>();
		
		for (Apprenticeship apprenticeship2 : listApprenticeship) {
			if (apprenticeship2.getCompany().getIdCompany()==company.getIdCompany()) {
				
				listApprenticeshipCompany.add(apprenticeship2);
			}
		
		}
		  mv.addObject("listeApprenticeship", listApprenticeshipCompany);
      
		mv.addObject("company", company);
		mv.addObject("apprenticeship", listApprenticeshipCompany);
        mv.setViewName("forms/company-profile");  
        
        
        return mv;
        		
        
    }

}

