package fr.eclee.proapps.controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.hibernate.mapping.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.eclee.beans.Account;
import fr.eclee.beans.Address;
import fr.eclee.beans.Apprenticeship;
import fr.eclee.beans.Company;
import fr.eclee.beans.School;
import fr.eclee.beans.Student;
import fr.eclee.repositories.dao.ApprenticeshipRepository;
import fr.eclee.repositories.dao.CompanyRepository;
import fr.eclee.repositories.dao.SchoolRepository;
import fr.eclee.repositories.dao.StudentRepository;

@Controller

public class StudentController {

	@Autowired
	ApprenticeshipRepository ar;

	@Autowired
	StudentRepository st;

	@Autowired
	SchoolRepository sr;
	
	@Autowired
	CompanyRepository cr;

	@PostMapping(value = "/signUpStudent")
	public ModelAndView addStudent(ModelAndView mv, @RequestParam(value = "name") String name,
			@RequestParam(value = "surname") String surname, @RequestParam(value = "email") String email,
			@RequestParam(value = "dateBirth") String dateBirth, @RequestParam(value = "phone") String phone,
			@RequestParam(value = "placeBirth") String placeBirth,

			@RequestParam(value = "gender") String gender,
			@RequestParam(value = "lastCertification") String lastCertification,

			@RequestParam(value = "currentSchool") String currentSchool,
			@RequestParam(value = "nameStreet") String nameStreet, @RequestParam(value = "zipCode") String zipCode,
			@RequestParam(value = "city") String city, @RequestParam(value = "country") String country,
			@RequestParam(value = "login") String login, @RequestParam(value = "pwd") String pwd,Map model) {
		System.out.println("/////////ok0");
		LocalDate registrationDate = LocalDate.now();

		String pwdHash = BCrypt.hashpw(pwd, BCrypt.gensalt());

		Address address = new Address(nameStreet, zipCode, city, country);

		Account account = new Account(login, pwdHash);
		System.out.println("/////////ok1");

		School school = new School();
		ArrayList<School> listSchool = (ArrayList<School>) sr.findAll();
		int idSchool = 0;
		for (int i = 0; i < listSchool.size(); i++) {
			if (listSchool.get(i).getName().equals(currentSchool)) {
				idSchool = listSchool.get(i).getIdSchool();
			}

		}
		school = sr.findById(idSchool).get();

		Student student = new Student(name, surname, dateBirth, placeBirth, phone, email, gender, registrationDate,
				lastCertification, currentSchool);
		student.setAccount(account);
		student.setSchool(school);
		student.setAddressStudent(address);
		

		st.save(student);

		st.flush();
		String test = "ok";
		mv.addObject("test", test);
		mv.addObject("student", student );

		mv.setViewName("notification/notification");

		return mv;
	}

	@GetMapping(value = "/studentProfile")
	public ModelAndView studentProfile(ModelAndView mv, @RequestParam(value = "id") int idStudent) {
		ArrayList<Apprenticeship> listApprenticeship = (ArrayList<Apprenticeship>) ar.findAll();
	
		ArrayList<Apprenticeship> listApprenticeshipCompany = new ArrayList<Apprenticeship>();
		mv.addObject("listeApprenticeship", listApprenticeshipCompany);
		Student student = new Student();
		student = st.findById(idStudent).get();

		mv.addObject("school", student);

		mv.setViewName("forms/student-profile3");

		return mv;
	}
	@GetMapping(value = "/studentProfileEdit")
	public ModelAndView studentProfileEdit(ModelAndView mv, @RequestParam(value = "id") int idStudent) {

		Student student = new Student();
		student = st.findById(idStudent).get();

		mv.addObject("school", student);

		mv.setViewName("forms/student-profile2");

		return mv;
	}
	@GetMapping(value = "/studentHome")
	public String studentHome(Model mv, @RequestParam(value = "id") int idS) {
		
		

		ArrayList<Apprenticeship> listApprenticeship = (ArrayList<Apprenticeship>) ar.findAll();
		ArrayList<Apprenticeship> listApprenticeshipCompany = new ArrayList<Apprenticeship>();
		
		for (Apprenticeship apprenticeship : listApprenticeship) {
          
                listApprenticeshipCompany.add(apprenticeship);
            

        }
		
		Student student = new Student();
		student = st.findById(idS).get();
		
		fieldsFilter(mv);
		
		mv.addAttribute("listeApprenticeship", listApprenticeshipCompany);
		mv.addAttribute("student", student);

		return "forms/student-profile";

	}
	
	@GetMapping(value = "/studentAll")
	public ModelAndView companyAll(ModelAndView mv, @RequestParam(value = "id") int idStudent) {
		
		

		ArrayList<Apprenticeship> listApprenticeship = (ArrayList<Apprenticeship>) ar.findAll();
		ArrayList<Apprenticeship> listApprenticeshipCompany = new ArrayList<Apprenticeship>();
		
		
		for (Apprenticeship apprenticeship : listApprenticeship) {
           
                listApprenticeshipCompany.add(apprenticeship);
            

        }
		Student student = new Student();
		student = st.findById(idStudent).get();
		
		
		
		mv.addObject("listeApprenticeship", listApprenticeshipCompany);
		mv.addObject("student", student);

		mv.setViewName("forms/studentAll");

		return mv;
	}
	
	@PostMapping(value = "/studentFilter" )
	public String studentFilter(Model mv ,@RequestParam(value = "id") int idStudent,
			@RequestParam(value = "webdesigner", required = false)boolean webdesigner,
			@RequestParam(value = "3dgraphic",required = false)boolean dddgraphic,
			@RequestParam(value = "graphic",required = false)boolean graphic,
			@RequestParam( value = "electronicTech",required = false)boolean electronicTech,
			@RequestParam(value = "webgraphic",required = false)boolean webgraphic,
			@RequestParam(value = "brandmanager",required = false)boolean brandmanager,
			@RequestParam(value = "newyork",required = false)boolean newyork,
			@RequestParam(value =  "losangles",required = false)boolean losangles,
			@RequestParam(value = "chicago",required = false)boolean chicago,
			@RequestParam(value = "houston",required = false)boolean houston,
			@RequestParam(value = "sandiego",required = false)boolean sandiego,
			@RequestParam(value = "sanjose",required = false)boolean sanjose,@RequestParam(value = "occupation")String occupation
			,@RequestParam(value = "region")String region)  
	{ArrayList<Apprenticeship> listApprenticeship = (ArrayList<Apprenticeship>) ar.findAll();
	
String arg0 ="Geolog";
String arg1 ="Civil Engineering and Geodes";
String arg2 ="Graphic";
String arg3 ="Economics, law and commerce";
String arg4 ="Electrical engineering";
String arg5 ="Health and social protection";
String arg6 ="Agriculture, fisheries and veterinary medicine";
String arg7 ="Personal services";
String arg8 ="Mechanical Engineering";
String arg9 ="Traffic, transport and storage";
String arg10 ="Textile, skin and similar  products";
String arg11="Hospitality and tourism";
String arg12="Chemistry and technology";
String arg13="Forestry and wood processing";
String arg14 ="Sport and Recreation";
String arg15="Other";



	ArrayList<Apprenticeship> listApprenticeshipCompany = new ArrayList<Apprenticeship>();
	  if(newyork==true)
	  {System.out.println("GENIALE");
		  for (Apprenticeship apprenticeship : listApprenticeship) {
	            if (apprenticeship.getLocation().equals("New York")) {

	                listApprenticeshipCompany.add(apprenticeship);
	            }

	        }
	    
	  }  if(losangles==true)
	  {
	  for (Apprenticeship apprenticeship : listApprenticeship) {
            if (apprenticeship.getLocation().equals("Los Angeles")) {

                listApprenticeshipCompany.add(apprenticeship);
            }

        }
    
  }
	  if(chicago==true)
	  {
	  for (Apprenticeship apprenticeship : listApprenticeship) {
            if (apprenticeship.getLocation().equals("Chicago")) {

                listApprenticeshipCompany.add(apprenticeship);
            }

        }
    
  }
	  if(houston==true)
	  {System.out.println("GENIALE");
	  for (Apprenticeship apprenticeship : listApprenticeship) {
            if (apprenticeship.getLocation().equals("Houston")) {

                listApprenticeshipCompany.add(apprenticeship);
            }

        }
    
  }
	  if(sandiego==true)
	  {System.out.println("GENIALE");
	  for (Apprenticeship apprenticeship : listApprenticeship) {
            if (apprenticeship.getLocation().equals("San Diego")) {

                listApprenticeshipCompany.add(apprenticeship);
            }

        }
    
  }if(sanjose==true)
  {System.out.println("GENIALE");
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getLocation().equals("San Jose")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }
  }
  if(webdesigner==true)
  {
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getArea().equals("Web Designer")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }

}
  if(dddgraphic==true)
  {
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getArea().equals("3D Graphic Designer")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }

}
  if(graphic==true)
  {
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getArea().equals("Graphic Designer")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }

}
  if(electronicTech==true)
  {
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getArea().equals("Electronics Technician")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }

}
  if(webgraphic==true)
  {
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getArea().equals("Web / Graphic Designer")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }

}
  if(brandmanager==true)
  {
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getArea().equals("Brand Manager")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }

}
  if(occupation.equals(arg0))
  {
	  System.out.println("IGNENIEUX");
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getOccupation().equals(arg0)) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }

}
  if(region.equals("Lille"))
  {
	  
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getLocation().equals("Lille")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }

}
  if(region.equals("Skopje"))
  {
	  
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getLocation().equals("Skopje")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }

}

  if(region.equals("Kratovo"))
  {
	  
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getLocation().equals("Kratovo")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }

}

  if(region.equals("Bitola"))
  {
	  
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getLocation().equals("Bitola")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }

}

  if(region.equals("Koumanovo"))
  {
	  
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getLocation().equals("Koumanovo")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }

}
  if(region.equals("Prilep"))
  {
	  
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getLocation().equals("Prilep")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }

}
  if(region.equals("Tetovo"))
  {
	  
  for (Apprenticeship apprenticeship : listApprenticeship) {
        if (apprenticeship.getLocation().equals("Tetovo")) {

            listApprenticeshipCompany.add(apprenticeship);
        }

    }

}


  


	  else
	  {
	    System.out.println("checkbox is not checked");
	  }
 
	Student student = new Student();
	student = st.findById(idStudent).get();
	
	fieldsFilter(mv);
	
	mv.addAttribute("listeApprenticeship", listApprenticeshipCompany);
	mv.addAttribute("student", student);

	return "forms/studentAll";

		
	}
	@PostMapping(value = "/mail" )
	public ModelAndView mail(ModelAndView mv ,@RequestParam(value = "idS") int idStudent,@RequestParam(value = "idA") int idApprenticeship) {
		Student student = new Student();
		student = st.findById(idStudent).get();
		Apprenticeship apprenticeship = new Apprenticeship();
		apprenticeship = ar.findById(idApprenticeship).get();
		Company company = new Company();
		company = cr.findById(apprenticeship.getCompany().getIdCompany()).get();
		
		final String username = "Ecleeprod@gmail.com";
	        final String password = "Salouhouprod!";
	        Properties props = new Properties();
	        props.put("mail.smtp.host", "smtp.gmail.com");
	        props.put("mail.smtp.port", "465");
	        props.put("mail.smtp.auth", "true");
	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.smtp.starttls.required", "true");
	        props.put("mail.smtp.ssl.protocols", "TLSv1.2");
	        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	       // props.put("mail.smtp.auth", "true");
	       // props.put("mail.smtp.starttls.enable", "true");
	      // props.put("mail.smtp.host", "smtp.gmail.com");
	      //  props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
	        props.put("mail.smtp.port", "587");
	        Session session = Session.getInstance(props, new Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	                return new PasswordAuthentication(username, password);
	            }
	        });
	        Message message = new MimeMessage(session);
	        try {
	            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(company.getEmail()));
	            message.setSubject(student.getName()+" "+student.getSurname()+" applied to you.");
	            message.setText( student.getName()+" "+student.getSurname()+" applied to your offer as "+ apprenticeship.getTitle()+"."+"\n"+"There is his cv in copy(on working)." + "\n"+"Grateful, Proapps Team");
	            
	            
	            Transport.send(message);
	        } catch (AddressException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        } catch (MessagingException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	        
	   mv.addObject("student", student);

	   mv.setViewName("forms/studentAll");
		return mv;
	}
	
	@GetMapping(value = "/studentApprenticeshipView")
	public ModelAndView studentApprenticeshipView(ModelAndView mv, @RequestParam(value = "idStudent") int idStudent,@RequestParam(value = "idApprenticeship") int idApprenticeship) {
		
		
		Apprenticeship apprenticeship = new Apprenticeship();
		apprenticeship = ar.findById(idApprenticeship).get();
		
		
		Student student= new Student();
		student = st.findById(idStudent).get();
		
		
		mv.addObject("apprenticeship", apprenticeship);
		mv.addObject("student", student);

		mv.setViewName("student/StudentApprenticeship");

		return mv;
	}
	
	@GetMapping(value = "/suppressionStudent")
	 public ModelAndView deleteStudent(ModelAndView mv, @RequestParam(value = "id") int id) {
	        
		
		         sr.deleteById((id));
		         mv.setViewName("index");
		         
		 		
		         return mv;
		     }
	public void fieldsFilter(Model mv) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader("D:\\env\\Stage\\proapp\\Proapps\\src\\main\\webapp\\resources\\csv\\tbsubindustry.csv"));
			System.out.println("fichier : "+br); 
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    String ligne = null;
	    String str = null;
	    ArrayList<String>  subindustry= new ArrayList <String>();
	    ArrayList<String> subindustry2= new ArrayList <String>();
	    try {
			while ((ligne = br.readLine()) != null)
			 {
			  
			  String[] data = ligne.split(",");
			
			  while ((ligne = br.readLine()) != null) {
				  
			      String[] temp = ligne.split(",");
			      String marque = temp[0];
			    
			      subindustry.add(marque);
				  for(int i = 0 ; i < subindustry.size(); i++) {
					  int coupure = 0;
					
					String str1 =  (String) subindustry.get(i);
					for(int y = 0 ; y <str1.length();y++) {
						
						char c = str1.charAt(y);
						
						 if((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')){
						     y =str1.length();
						     break;
						    }
					   else{
					      coupure++;
					    }
					}
					 str = str1.substring(coupure, str1.length()-1);
					
				 
				}
				  subindustry2.add(str);
			  }
			
			
  }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		BufferedReader br1 = null;
		try {
			br1 = new BufferedReader(new FileReader("D:\\env\\Stage\\proapp\\Proapps\\src\\main\\webapp\\resources\\csv\\tbindustry.csv"));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    String ligne1 = null;
	    String str2 = null;
	    ArrayList<String>  industry= new ArrayList <String>();
	    ArrayList<String> industry1= new ArrayList <String>();
	    try {
			while ((ligne1 = br1.readLine()) != null)
			 {
			  // Retourner la ligne dans un tableau
			  String[] data1 = ligne1.split(",");
			
			  // Afficher le contenu du tableau
   

			  while ((ligne1 = br1.readLine()) != null) {
				  
			      String[] temp = ligne1.split(",");
			      String marque = temp[0];
			    
			      industry.add(marque);
				  for(int i = 0 ; i < industry.size(); i++) {
					  int coupure = 0;
					
					String str3 =  (String) industry.get(i);
					for(int y = 0 ; y <str3.length();y++) {
						
						char c = str3.charAt(y);
						
						 if((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')){
						     y =str3.length();
						     break;
						    }
					   else{
					      coupure++;
					    }
					}
					 str2 = str3.substring(coupure, str3.length()-1);
					
				 
				}
				  industry1.add(str2);
			  }
			
			
  }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    BufferedReader br2 = null;
		try {
			br2 = new BufferedReader(new FileReader("D:\\env\\Stage\\proapp\\Proapps\\src\\main\\webapp\\resources\\csv\\tbapprenticeship.csv"));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    String ligne2 = null;
	    String str5 = null;
	    ArrayList<String>  apprenticeship= new ArrayList <String>();
	    ArrayList<String> apprenticeship1= new ArrayList <String>();
	    try {
			while ((ligne2 = br2.readLine()) != null)
			 {
			  // Retourner la ligne dans un tableau
			  String[] data2 = ligne2.split(",");
			
			  // Afficher le contenu du tableau
   

			  while ((ligne2 = br2.readLine()) != null) {
				  
			      String[] temp = ligne2.split(",");
			      String marque = temp[0];
			    
			      apprenticeship.add(marque);
				  for(int i = 0 ; i < apprenticeship.size(); i++) {
					  int coupure = 0;
					
					String str4 =  (String) apprenticeship.get(i);
					for(int y = 0 ; y <str4.length();y++) {
						
						char c = str4.charAt(y);
						
						 if((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')){
						     y =str4.length();
						     break;
						    }
					   else{
					      coupure++;
					    }
					}
					 str5 = str4.substring(coupure, str4.length()-1);
					
				 
				}
				  apprenticeship1.add(str5);
			  }
			
			
  }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    mv.addAttribute("industry", industry1);
	    mv.addAttribute("subindustry", subindustry2);
	    mv.addAttribute("apprenticeship", apprenticeship1);


		
	   

	
		
	}
}

