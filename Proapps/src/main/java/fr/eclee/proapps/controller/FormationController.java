package fr.eclee.proapps.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.eclee.beans.Apprenticeship;
import fr.eclee.beans.Company;
import fr.eclee.beans.Formation;
import fr.eclee.beans.School;
import fr.eclee.repositories.dao.FormationRepository;
import fr.eclee.repositories.dao.SchoolRepository;

@Controller
public class FormationController {
	
	@Autowired
	FormationRepository fr;
	@Autowired
	SchoolRepository sr;
	
	@PostMapping(value = "/addFormationForm")
	public ModelAndView addFormation(ModelAndView mv, @RequestParam(value = "idSchool") Integer idS,@RequestParam(value = "title") String title,
			@RequestParam(value = "formationDesc") String formationDesc,
			@RequestParam(value = "knwoledge") String knowledge,
			@RequestParam(value = "numberHours") String numberHours, @RequestParam(value = "location") String location,
			@RequestParam(value = "position") Integer position,@RequestParam(value = "type") String type,
			@RequestParam(value = "duration") String duration, @RequestParam(value = "formationDetail") String formationDetail
			) {
		//List<Apprenticeship> listeappskills = (List<Apprenticeship>) ar.findAll();
		Formation formation = new Formation(formationDesc, knowledge, duration, title, numberHours, location, position, type, formationDetail);
		School sch = new School();
		sch = sr.findById(idS).get();
		formation.setSchool(sch);	
		fr.saveAndFlush(formation);
		sr.saveAndFlush(sch);
		

		mv.addObject("formation", formation);
		

		mv.addObject("school", sch);
		
		
		ArrayList<Formation> listFormation= (ArrayList<Formation>) fr.findAll();
		ArrayList<Formation> listFormationSchool = new ArrayList<Formation>();
		
		for (Formation form : listFormation) {
            if (form.getSchool().getIdSchool() == idS) {
            	

            	listFormationSchool.add(form);
            }

        }
		
		mv.addObject("listFormationSchool", listFormationSchool);
		

		mv.setViewName("forms/school-profile");

		return mv;
	}

		 
}
