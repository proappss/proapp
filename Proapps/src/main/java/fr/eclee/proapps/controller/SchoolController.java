package fr.eclee.proapps.controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.eclee.beans.Account;
import fr.eclee.beans.Address;
import fr.eclee.beans.Apprenticeship;
import fr.eclee.beans.Company;
import fr.eclee.beans.Formation;
import fr.eclee.beans.School;
import fr.eclee.repositories.dao.FormationRepository;
import fr.eclee.repositories.dao.SchoolRepository;

@Controller
public class SchoolController {

		@Autowired
		SchoolRepository sr;
		@Autowired
		FormationRepository fr;
	
	@PostMapping(value = "/signUpSchool")
	public ModelAndView addSchool(ModelAndView mv, @RequestParam(value = "name") String name,@RequestParam(value = "email") String email,
			@RequestParam(value = "tel") String tel, @RequestParam(value = "website") String website,
			@RequestParam(value = "facebook") String facebook,
			@RequestParam(value = "schoolDesc") String schoolDesc, @RequestParam(value = "type") String type,
			@RequestParam(value = "yearEstablished") String yearEstablished,
			@RequestParam(value = "nameStreet") String nameStreet, @RequestParam(value = "zipCode") String zipCode,
			@RequestParam(value = "city") String city, @RequestParam(value = "country") String country,
			@RequestParam(value = "login") String login, @RequestParam(value = "pwd") String pwd) {

		LocalDate dateJ = LocalDate.now();

		String pwdHash = BCrypt.hashpw(pwd, BCrypt.gensalt());

		Address address = new Address(nameStreet, zipCode, city, country);

		Account account = new Account(login, pwdHash);
		

		School school = new School(name, email, tel, dateJ, website, schoolDesc, type, yearEstablished);
		
		school.setAddressSchool(address);
		school.setAccount(account);

		
		sr.save(school);

		sr.flush();

		mv.addObject("school", school);

		mv.setViewName("notification/notification");

		return mv;
	}
	
	@GetMapping(value = "/registerSchool")
	public String redirectregisterSchool() {
		
		return "forms/registerSchool";
	}
	
	@GetMapping(value = "/schoolProfile")
	public ModelAndView schoolProfile(ModelAndView mv, @RequestParam(value = "id") int idSchool) {

		School school = new School();
		school = sr.findById(idSchool).get();

		mv.addObject("school", school);
		ArrayList<Formation> listFormation= (ArrayList<Formation>) fr.findAll();
		ArrayList<Formation> listFormationSchool = new ArrayList<Formation>();
		
		for (Formation form : listFormation) {
            if (form.getSchool().getIdSchool() == idSchool) {
            	

            	listFormationSchool.add(form);
            }

        }
		
		mv.addObject("listFormationSchool", listFormationSchool);

		mv.setViewName("forms/school-profile-edit");

		return mv;
	}
	
	@GetMapping(value = "/schoolHome")
	public ModelAndView schoolHome(ModelAndView mv, @RequestParam(value = "id") int idSchool) {
		
		
		
		School school= new School();
		school = sr.findById(idSchool).get();
		
		mv.addObject("school", school);
		
		ArrayList<Formation> listFormation= (ArrayList<Formation>) fr.findAll();
		ArrayList<Formation> listFormationSchool = new ArrayList<Formation>();
		
		for (Formation form : listFormation) {
            if (form.getSchool().getIdSchool() == idSchool) {
            	

            	listFormationSchool.add(form);
            }

        }
		
		mv.addObject("listFormationSchool", listFormationSchool);
		
		mv.setViewName("forms/school-profile");
		

		return mv;
	}
	
	@GetMapping(value = "/schoolAddFormation")
	public ModelAndView schoolAddFormation(ModelAndView mv, @RequestParam(value = "id") int idSchool) {
		
		
		
		School school= new School();
		school = sr.findById(idSchool).get();
		
		mv.addObject("school", school);

		mv.setViewName("forms/addFormation");

		return mv;
	}
	
	@PostMapping(value = "/modifySchool")
	public ModelAndView modifySchool(ModelAndView mv, @RequestParam(value = "name") String name,@RequestParam(value = "email") String email,
			@RequestParam(value = "tel") String tel, @RequestParam(value = "website") String website,
			@RequestParam(value = "schoolDesc") String schoolDesc, @RequestParam(value = "type") String type,
			@RequestParam(value = "yearEstablished") String yearEstablished,
			@RequestParam(value = "nameStreet") String nameStreet, @RequestParam(value = "zipCode") String zipCode,
			@RequestParam(value = "city") String city, @RequestParam(value = "country") String country,
			@RequestParam(value = "login") String login, @RequestParam(value = "pwd") String pwd, @RequestParam(value = "id") int idSchool) {

		LocalDate dateJ = LocalDate.now();

		String pwdHash = BCrypt.hashpw(pwd, BCrypt.gensalt());

		Address address = new Address(nameStreet, zipCode, city, country);

		Account account = new Account(login, pwdHash);
		

		School school = new School(name, email, tel, dateJ, website, schoolDesc, type, yearEstablished);
		
		school.setAddressSchool(address);
		school.setAccount(account);

		
		sr.saveAndFlush(school);
		

		
		mv.addObject("school", school);
		ArrayList<Formation> listFormation= (ArrayList<Formation>) fr.findAll();
 		ArrayList<Formation> listFormationSchool = new ArrayList<Formation>();
 		
 		for (Formation form : listFormation) {
             if (form.getSchool().getIdSchool() == school.getIdSchool()) {
             	

             	listFormationSchool.add(form);
             }

         }
 		
 		mv.addObject("listFormationSchool", listFormationSchool);
 		
 		mv.setViewName("forms/school-profile");
 		
		

		return mv;
	}
	@GetMapping(value = "/deleteProgram")
	 public ModelAndView deleteProgram(ModelAndView mv, @RequestParam(value = "id") int id) {
		Formation formation = new Formation();
		formation = fr.findById(id).get();
	
		School school = new School();
		school = sr.findById(formation.getSchool().getIdSchool()).get();
		
		         fr.deleteById((id));
		         
		 mv.addObject("school", school);
		 		
		 		ArrayList<Formation> listFormation= (ArrayList<Formation>) fr.findAll();
		 		ArrayList<Formation> listFormationSchool = new ArrayList<Formation>();
		 		
		 		for (Formation form : listFormation) {
		             if (form.getSchool().getIdSchool() == school.getIdSchool()) {
		             	

		             	listFormationSchool.add(form);
		             }

		         }
		 		
		 		mv.addObject("listFormationSchool", listFormationSchool);
		 		
		 		mv.setViewName("forms/school-profile");
		 		

		     
		 		
		         return mv;
		     }

@PostMapping(value = "/csvSchool")
public String csvSchool(ModelAndView mv) {
	BufferedReader br = null;
	try {
		br = new BufferedReader(new FileReader("D:\\env\\Stage\\proapp\\Proapps\\src\\main\\webapp\\resources\\csv\\tbsubindustry.csv"));
		System.out.println("fichier : "+br); 
	} catch (FileNotFoundException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
    String ligne = null;
    String str = null;
    String name=null;
    String email=null;
    String tel=null;
    String dateJoin=null;
    String pwd ="";
    String website=null;
    String SchoolDesc=null;
    String type=null;
    String yearEstablished = null;
    String nameStreet=null;
    String zipcode=null;
    String country=null;
    String city=null;
    String login=null;
    String line = "";
    String test="";
    String nom = "";
   ArrayList<String> lines = new ArrayList<String>();
   ArrayList<String>  school= new ArrayList <String>();
    try {
		while((line = br.readLine()) != null) {
		    while ((line = br.readLine()) != null) {
		        lines.add(line);
		    }
		    
		    System.out.println(lines.get(0));
		    
		    int loop = 0;
		    String[] temp = ligne.split("\n");
		      String element = temp[0];
		    
		      school.add(element);
		      for(int i = 0 ; i < school.size(); i++) {
		    	  for (int j = 0; j < lines.get(i).length(); j++) {
				        if (lines.get(i).charAt(j)!= ',') {
				            test=test + lines.get(0).charAt(j);
				        }
				        if (lines.get(i).charAt(j) == ',') {
				            System.out.println(test);
				            if (loop==0) {
				                name = test;
				                pwd=test;
				                login=test;
				                loop++;
				            }
				            if (loop==1) {
				            	email=test;
				            	 loop++;
				            }
				            if (loop==2) {
				            	tel=test;
				            	 loop++;
				            }
				            if (loop==3) {
				            	dateJoin=test;
				            	 loop++;
				            }
				            if (loop==4) {
				            	website=test;
				            	 loop++;
				            }
				            if (loop==5) {
				            	SchoolDesc=test;
				            	loop++;
				            	}
				            if (loop==6) {
				            	type=test;
				            	 loop++;
				            }
				            if (loop==7) {
				            	yearEstablished=test;
				            	 loop++;
				            }
				            if (loop==8) {loop++;
				            nameStreet=test;
				            }
				            
				            if (loop==9) {
				            zipcode=test;
				            	 loop++;
				            }
				            if (loop==10) {
				            	country =test;
				            	 loop++;
				            }
				            if (loop==11) {
				            	city=test;
				            	 loop=0;
				            	 
				            }
				            
				            test="";
				            
				        }
				        LocalDate dateJ = LocalDate.now();

				        String pwdHash = BCrypt.hashpw(pwd, BCrypt.gensalt());

				        Address address = new Address(nameStreet, zipcode, city, country);

				        Account account = new Account(login, pwdHash);
				                

				        School school1 = new School(name, email, tel, dateJ, website, SchoolDesc, type, yearEstablished);
				                
				        school1.setAddressSchool(address);
				        school1.setAccount(account);

				                
				        sr.save(school1);

				        sr.flush();
				        
				    }
		          
		      }
		    
		}
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return "index";
    
		 

}}
