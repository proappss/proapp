package fr.eclee.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eclee.beans.Company;

public interface CompanyRepository extends JpaRepository<Company, Integer>{

	
}
