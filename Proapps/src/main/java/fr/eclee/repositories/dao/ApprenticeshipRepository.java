package fr.eclee.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eclee.beans.Apprenticeship;

public interface ApprenticeshipRepository extends JpaRepository<Apprenticeship, Integer>{

	
}