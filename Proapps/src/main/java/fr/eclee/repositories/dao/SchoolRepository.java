package fr.eclee.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eclee.beans.School;

public interface SchoolRepository extends JpaRepository<School, Integer>{

	
}
