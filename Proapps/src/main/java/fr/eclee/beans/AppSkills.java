package fr.eclee.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class AppSkills {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idAppSkills;

	@NonNull
	private String appSkillDesc;

	@ManyToOne
	@JoinColumn(name = "id_apprenticeship")
	private Apprenticeship apprenticeship;

}
