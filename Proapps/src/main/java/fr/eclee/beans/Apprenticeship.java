package fr.eclee.beans;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Apprenticeship {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idApp;

	@NonNull
	private String occupation;
	
	@NonNull
	private String shortDesc;
	
	@NonNull
	private String entry;
	
	@NonNull
	private String knwoledge;
	
	@NonNull
	private String skills;

	@NonNull
	private String behaviour;

	@NonNull
	private String duration;
	
	
	@NonNull
	private String title;
	
	@NonNull
	private String apprenticeship;
	
	
	@NonNull
	private String progression;
	
	@NonNull
	private String school;
	
	@NonNull
	private String edprog;
	

	@NonNull
	private String area;
	

	@NonNull
	private String salary;

	@NonNull
	private String location;
	

	@NonNull
	private String empstatus;
	
	@NonNull
	private String emptype;
	
	@NonNull
	private Integer position;
	
	@NonNull
	private String experience;
	
	@NonNull
	private String gender;
	
	@NonNull
	private String nationnality;
	
	
	@NonNull
	private String jobdetail;

	

	
	
	
	@ManyToOne
	@JoinColumn(name = "id_company")
	private Company company;
	
	@OneToMany(mappedBy = "apprenticeship" , cascade = CascadeType.ALL)
	private List<AppSkills> listAppSkills;

}
