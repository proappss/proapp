package fr.eclee.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Skills {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idSkills;

	@NonNull
	private String skillDesc;

	@ManyToOne
	@JoinColumn(name = "id_company")
	private Company company;

}
