package fr.eclee.beans;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class School {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idSchool;

	@NonNull
	private String name;

	@NonNull
	private String email;

	@NonNull
	private String tel;

	@NonNull
	private LocalDate dateJoin;
	
	@NonNull
	private String website;
	
	@NonNull
	private String SchoolDesc;
	
	@NonNull 
	private String type;
	
	@NonNull 
	private String yearEstablished;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_adresse")
	private Address addressSchool;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_compte")
	private Account account;
	
	@OneToMany(mappedBy = "school" , cascade = CascadeType.ALL)
	private List<Formation> listFormation;

}
