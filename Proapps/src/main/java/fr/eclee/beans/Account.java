package fr.eclee.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Account {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idAccount;

	@NonNull
	private String login;

	@NonNull
	private String pwd;

	@OneToOne(mappedBy = "account")
	private Company company;
	
	@OneToOne(mappedBy = "account")
	private School school;
	
	@OneToOne(mappedBy = "account")
	private Student student;

}
