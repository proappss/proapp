package fr.eclee.beans;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Company {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idCompany;

	@NonNull
	private String name;

	@NonNull
	private String regNo;

	@NonNull
	private String email;

	@NonNull
	private String tel;

	@NonNull
	private LocalDate dateJoin;
	
	@NonNull
	private String website;
	
	@NonNull
	private String facebook;
	
	@NonNull
	private String linkedin;
	
	@NonNull
	private String companyDesc;
	
	@NonNull 
	private String type;
	
	@NonNull 
	private String totalEmployees;
	
	@NonNull 
	private String yearEstablished;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_adresse")
	private Address addressCompany;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_compte")
	private Account account;
	
	@OneToMany(mappedBy = "company" , cascade = CascadeType.ALL)
	private List<Apprenticeship> listApprenticeship;
	
	@OneToMany(mappedBy = "company" , cascade = CascadeType.ALL)
	private List<Skills> listSkills;

}
