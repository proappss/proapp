package fr.eclee.beans;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity
public class CvStudent {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idCv;
	
	@NonNull
	private String name;

	@NonNull
	private String surname;
	
	@NonNull
	private String email;
	
	@OneToOne(mappedBy = "cv")
	private Student student;
}
