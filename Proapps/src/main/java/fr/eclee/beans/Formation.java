package fr.eclee.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Formation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idFormation;

	@NonNull
	private String formationDesc;
	
	@NonNull
	private String knwoledge;

	@NonNull
	private String duration;

	@NonNull
	private String title;
	
	@NonNull
	private String numberHours;

	@NonNull
	private String location;
	
	@NonNull
	private Integer position;
	
	@NonNull
	private String type;
	
	@NonNull
	private String formationDetail;
	
	@ManyToOne
	@JoinColumn(name = "id_school")
	private School school;

}
