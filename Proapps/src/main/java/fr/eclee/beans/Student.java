package fr.eclee.beans;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idStudent;

	@NonNull
	private String name;

	@NonNull
	private String surname;

	@NonNull
	private String dateBirth;
	
	@NonNull
	private String placeBirth;
	
	@NonNull
	private String phone;
	
	@NonNull
	private String email;
	
	@NonNull
	private String gender;
	
	@NonNull
	private LocalDate registrationDate;
	
	@NonNull
	private String lastCertification;
	
	@NonNull
	private String currentSchool;
	
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_adresse")
	private Address addressStudent;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_compte")
	private Account account;
	
	@ManyToOne()
	@JoinColumn(name = "fk_school")
	private School school;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_cv")
	private CvStudent cv;
	
}
